<?php

class Apidenuncia_model extends CI_Model
{

	public $tabela       = 'denuncia_api';
	public $chave   	 = 'denuncia_id';
	public $visivel      = 'denuncia_sicronizada';

	public function listarGo($id = null, $campo = null)
	{
		$this->db->select('denuncia_api.denuncia_id, empresa.empresa_nomeFantasia, tipodenuncia.nome as tp_denuncia, departamento.nome as dp_nome, denuncia_api.* ');
		$this->db->join('empresa', 'empresa_id = id_empresa');
		$this->db->join('tipodenuncia', 'tipodenuncia_id = id_tipodenuncia', 'left');
		$this->db->join('departamento', 'departamento_id = id_departamento', 'left');
		$this->db->where('denuncia_sicronizada', 0);
		if ($id) {
			$this->db->where('denuncia_api.' . $campo, $id);
		}
		return $this->db->get($this->tabela)->result();
	}

	public function listar($tabela)
	{
		$this->db->select('*');
		$this->db->where($tabela . '_visivel', '1');
		return $this->db->get($tabela)->result();
	}

	public function pegarPorId($id, $chave, $tabela)
	{
		$this->db->select('*');
		$this->db->where($chave, $id);
		return $this->db->get($tabela)->result();
	}

	public function todosDepartamentoEmpresa($id)
	{
		$this->db->select('*');
		$this->db->like('id_empresa', $id);
		return $this->db->get('departamento')->result();
	}

	public function addApi($dados, $tabela)
	{
		$this->db->insert($tabela, $dados);
		if ($this->db->affected_rows() == '1')
			return $this->db->insert_id();
		return false;
	}

	public function anexarApi($denuncia, $anexo, $url, $thumb, $path, $tamanho, $tipo)
	{
		$this->db->set('anexo_anexo', $anexo);
		$this->db->set('anexo_url', $url);
		$this->db->set('anexo_thumb', $thumb);
		$this->db->set('anexo_path', $path);
		$this->db->set('anexo_tamanho', $tamanho);
		$this->db->set('anexo_tipo', $tipo);
		$this->db->set('id_denuncia', $denuncia);
		return $this->db->insert('anexos');
	}

	public function updateAnexo($id, $id_denuncia)
	{
		$data = array(
			'id_denuncia' => $id_denuncia,
		);
	
		$this->db->where('anexo_id', $id); // Filtre pelo ID do anexo que deseja atualizar
		return $this->db->update('anexos', $data);
	}

	public function update($id)
	{
		$this->db->where($this->chave, $id);

		if ($this->db->update($this->tabela, [$this->visivel => 1])) {
			return true;
		}

		return false;
	}

	public function delete($id)
	{
		$this->db->where($this->chave, $id);

		if ($this->db->delete($this->tabela)) {
			return true;
		}

		return false;
	}
}

/* End of file Apidenuncia_model.php */
/* Location: ./application/models/Denuncia_model.php */