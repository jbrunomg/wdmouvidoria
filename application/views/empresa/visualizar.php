<!-- Form horizontal -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Visualizar Empresa</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<!-- <li><a data-action="close"></a></li> -->
        	</ul>
    	</div>
	</div>

	<div class="panel-body">
		<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/editarExe" method="post" enctype="multipart/form-data">
			<fieldset class="content-group">
				<legend class="text-bold">Dados Empresa:</legend>

				<!-- <input disabled  type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" /> -->

				<input disabled  type="hidden" name="empresa_id" value="<?php echo $dados[0]->empresa_id; ?>" />

				<div class="form-group">
					<label class="control-label col-lg-2">CNPJ:</label>
					<div class="col-lg-5">
						<input disabled  type="text" class="form-control" placeholder="Cnpj" data-mask="99.999.999/9999-99" data-mask-selectonfocus="true" name="empresa_cpfCnpj" id="cpfCnpj" value="<?php echo $dados[0]->empresa_cpfCnpj; ?>">
					<?php echo form_error('empresa_cpfCnpj'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Nome da Empresa:</label>
					<div class="col-lg-5">
						<input disabled  type="text" class="form-control" placeholder="Nome Empresa / Fantasia" name="empresa_nomeFantasia" id="nomeFantasia" value="<?php echo $dados[0]->empresa_nomeFantasia; ?>">
					<?php echo form_error('empresa_nomeFantasia'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Razão Social:</label>
					<div class="col-lg-5">
						<input disabled  type="text" class="form-control" placeholder="Nome Razão" name="empresa_nomeRazao" id="nomeRazao" value="<?php echo $dados[0]->empresa_nomeRazao; ?>">
					<?php echo form_error('empresa_nomeRazao'); ?>
					</div>										
				</div>	


				<legend class="text-bold">Dados Comerciais:</legend>

				<div class="form-group">
					<label class="control-label col-lg-2">CEP:</label>
					<div class="col-lg-5">
						<input disabled  type="text" class="form-control" name="empresa_cep" id="cep" data-mask="99999-999" data-mask-selectonfocus="true" value="<?php echo $dados[0]->empresa_cep; ?>">
					<?php echo form_error('empresa_cep'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Endereço:</label>
					<div class="col-lg-5">
						<input disabled  type="text" class="form-control" placeholder="Rua/Av/Trav" name="empresa_rua" id="rua" value="<?php echo $dados[0]->empresa_rua; ?>">
					<?php echo form_error('empresa_rua'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Numero:</label>
					<div class="col-lg-5">
						<input disabled  type="text" class="form-control" name="empresa_numero" id="numero" value="<?php echo $dados[0]->empresa_numero; ?>">
					<?php echo form_error('empresa_numero'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Complemento:</label>
					<div class="col-lg-5">
						<input disabled  type="text" class="form-control" name="empresa_complemento" id="complemento" value="<?php echo $dados[0]->empresa_complemento; ?>">
					<?php echo form_error('empresa_complemento'); ?>
					</div>										
				</div>


				<div class="form-group">
					<label class="control-label col-lg-2">Bairro:</label>
					<div class="col-lg-5">
						<input disabled  type="text" class="form-control" name="empresa_bairro" id="bairro" value="<?php echo $dados[0]->empresa_bairro;  ?>">
					<?php echo form_error('empresa_bairro'); ?>
					</div>										
				</div>

				<div class="form-group">
                	<label class="control-label col-lg-2">Estado:</label>
                	<div class="col-lg-5">
                        <select  class="form-control" name="empresa_estado" id="estado" >
                        	<option value="">Selecione</option>
                            <?php foreach ($estados as $valor) { ?>
                            	<?php $selected = ($valor->uf == $dados[0]->empresa_estado)?'SELECTED': ''; ?>
		                              <option value="<?php echo $valor->uf; ?>" <?php echo $selected; ?>><?php echo $valor->nome; ?></option>
		                        <?php } ?>
                        </select>
                        <?php echo form_error('empresa_estado'); ?>
                    </div>			                            
                </div>			                        
				<div class="form-group">
					<label class="control-label col-lg-2">Cidade:</label>
					<div class="col-lg-5">
                        <select class="form-control" name="empresa_cidade" id="cidade">
                            
                            <?php foreach ($cidades as $valor) { ?>
                            	<?php $selected = ($valor->nome == $dados[0]->empresa_cidade)?'SELECTED': ''; ?>
		                              <option value="<?php echo $valor->nome; ?>" <?php echo $selected; ?>><?php echo $valor->nome; ?></option>
		                        <?php } ?>
                        </select>
                    <?php echo form_error('empresa_cidade'); ?>	
                    </div>				                            	                            
				</div>	

				<legend class="text-bold">Dados Contato:</legend>

				<div class="form-group">
					<label class="control-label col-lg-2">Telefone Fixo:</label>
					<div class="col-lg-5">
						<input disabled  type="tel" class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="empresa_telefone" id="telefone" value="<?php echo $dados[0]->empresa_telefone; ?>">
					<?php echo form_error('empresa_telefone'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Telefone Celular I:</label>
					<div class="col-lg-5">
						<input disabled  type="tel"  class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="empresa_telefone2" id="telefone2" value="<?php echo $dados[0]->empresa_telefone2; ?>">
					<?php echo form_error('empresa_telefone2'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Telefone Celular II:</label>
					<div class="col-lg-5">
						<input disabled  type="tel" class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="empresa_telefone3" id="telefon3" value="<?php echo $dados[0]->empresa_telefone3; ?>">
					<?php echo form_error('empresa_telefone3'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Email - Mala Direta:</label>
					<div class="col-lg-5">
						<input disabled  type="text" placeholder="seu@email.com" class="form-control" name="empresa_email" id="email" value="<?php echo $dados[0]->empresa_email; ?>">
					<?php echo form_error('empresa_email'); ?>
					</div>										 
				</div>	

				<div class="form-group">
					<label class="control-label col-lg-2">Responsável:</label>
					<div class="col-lg-5">
						<input disabled  type="text" class="form-control" placeholder="Responsável"  data-mask-selectonfocus="true" name="empresa_responsavel" id="responsavel" value="<?php echo $dados[0]->empresa_responsavel; ?>">
					<?php echo form_error('empresa_responsavel'); ?>
					</div>										
				</div>

            </fieldset>  
		<!-- 		<div class="text-right">
					<button type="submit" class="btn bg-teal">Editar <i class="icon-arrow-right14 position-right"></i></button>
				</div> -->
		</form>
	</div>
</div>


<!-- Column selectors  FILIAIS -->
<div id='semScroll' class="panel panel-flat">
	<div class="panel-heading">
	<h5 class="panel-title">Visualizar filiais da empresa: <?php echo $dados[0]->empresa_nomeFantasia; ?></h5>	
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<li><a data-action="close"></a></li>
        	</ul>
    	</div>
	</div>

	<br>	
	<table class="table datatable-button-html5-columns">
		<thead>
			<tr>		    				
				<th>Nome Fantasia</th>	
				<th>Cnpj</th>					
				<th>Contato</th>
				<th>Email</th>
				<th>Responsável</th>											
				<th>Opções</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($filial as $valor) { ?>
				
				<tr> 					  								
					<td><?php echo $valor->filial_nomeFantasia; ?></td>
					<td><?php echo $valor->filial_cpfCnpj; ?></td> 					
					<td><?php echo $valor->filial_telefone; ?></td>
					<td><?php echo $valor->filial_email; ?></td>
					<td><?php echo $valor->filial_responsavel; ?></td>							
														
					<td>																				
						<ul class="icons-list">
							<?php if(checarPermissao('eEmpresa')){ ?>
							<li class="text-primary-600"><a href="<?php echo base_url()?>filial/editar/<?php echo $valor->filial_id; ?>" data-popup="tooltip" title="Editar"><i class="icon-pencil7"></i></a></li>
							<?php } ?>
							<?php if(checarPermissao('dEmpresa')){ ?>
							<li class="text-danger-600"><a href="#" data-popup="tooltip" title="Excluir" class="sweet_loader_id" url="filial/excluir" registro="<?php echo $valor->filial_id; ?>"><i class="icon-trash"></i></a></li>
							<?php } ?>
							<?php if(checarPermissao('vEmpresa')){ ?>
							<li class="text-teal-600"><a href="<?php echo base_url()?>filial/visualizar/<?php echo $valor->filial_id; ?>" data-popup="tooltip" title="Visualizar"><i class="icon-search4"></i></a></li>	
							<?php } ?>						
						</ul>																			
					</td>						
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>
<!-- /column selectors INATIVO -->


<!-- Column selectors  DENUNCIA -->
<div id='semScroll' class="panel panel-flat">
	<div class="panel-heading">
	<h5 class="panel-title">Visualizar denúncias da empresa: <?php echo $dados[0]->empresa_nomeFantasia; ?></h5>	
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<li><a data-action="close"></a></li>
        	</ul>
    	</div>
	</div>

	<br>	
	<table class="table datatable-button-html5-columns">
		<thead>
			<tr>				
				<th>Empresa</th>	
				<th>Tipo Denuncia</th>					
				<th>Departamento</th>
				<th>Data</th>
				<th>Status</th>											
				<th>Opções</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($denuncia as $valor) { 
			
			  switch ($valor->denuncia_status){
			    case 'aberto':
			      $status = '<span class="badge bg-blue">Aberto</span>';
			      break;
			    case 'analise':
			      $status = '<span class="badge bg-success-400">Em Análise</span>';
			      break;
			    case 'encerrado':
			      $status = '<span class="badge bg-danger">Encerrado</span>';
			      break;
			    default:
			      $status = '<span class="badge bg-grey-400">Não Informado</span>';
			      break;
			  }
			?>				
				<tr>     								
					<td><?php echo strtoupper($valor->empresa_nomeFantasia); ?></td>
					<td><?php echo $valor->tp_denuncia; ?></td> 					
					<td><?php echo $valor->dp_nome; ?></td>
					<td><?php echo date('d/m/Y', strtotime($valor->denuncia_data_cadastro)); ?></td>
					<td><?php echo $status; ?></td>							
														
					<td>																				
						<ul class="icons-list">							
								<?php if(checarPermissao('eDenuncia')){ ?>
								<li class="text-primary-600"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/editar/<?php echo $valor->denuncia_id; ?>" data-popup="tooltip" title="Editar"><i class="icon-pencil7"></i></a></li>
								<?php } ?>
								<?php if(checarPermissao('dDenuncia')){ ?>
								<li class="text-danger-600"><a href="#" data-popup="tooltip" title="Excluir" class="sweet_loader_id" url="<?php echo $this->uri->segment(1); ?>/excluir" registro="<?php echo $valor->denuncia_id; ?>"><i class="icon-trash"></i></a></li>
								<?php } ?>
								<?php if(checarPermissao('vDenuncia')){ ?>
								<li class="text-teal-600"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/visualizar/<?php echo $valor->denuncia_id; ?>" data-popup="tooltip" title="Visualizar"><i class="icon-search4"></i></a></li>	
								<?php } ?>																				
							
						</ul>																			
					</td>						
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>
<!-- /column selectors DENUNCIA -->


<!-- Grafico  1 -->
  <div class="panel panel-flat col-md-6 content" >

    <div class="panel-heading">
      <h5 class="panel-title">Denúncia Cadastradas</h5>
      <div class="heading-elements">
        <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li> 
                <li><a data-action="close"></a></li> 
              </ul>
          </div>
    </div>

    <div class="panel-body">

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Mês', '<?php echo date('Y') ?>', '<?php echo date('Y') -1 ?>', '<?php echo date('Y') -2 ?>'],

          <?php foreach ($dadosdenuncia as $valor ) { ?>

        
          [<?php echo "'".$valor->Ano."'"; ?> , <?php echo $valor->atual; ?>, <?php echo $valor->meio; ?>, <?php echo $valor->fim; ?>],


          <?php } ?>

        ]);

        var options = {
          chart: {
          //  title: 'Grafico',
          //  subtitle: 'Sales, Expenses, and Profit: 2014-2017',
          },
          bars: 'vertical', // Required for Material Bar Charts.
      vAxis: {format: 'decimal'}
        };

        var chart = new google.charts.Bar(document.getElementById('barchart_vaga'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
    </script>

    <div id="barchart_vaga" style="width: 100%; height: 500px;"></div>


      
    </div>
  </div> 
  <!-- Grafico  1 FIM -->


  <!-- Grafico  2 -->
	<div class="panel panel-flat col-md-6 content" >

		<div class="panel-heading">
			<h5 class="panel-title">Status da Denúncia</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<li><a data-action="reload"></a></li>
            		<li><a data-action="close"></a></li>
            	</ul>
        	</div>
		</div>


		<div class="panel-body">

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
        
          <?php foreach ($dadosstatus as $valor ) { ?>

        
          [<?php echo "'".$valor->denuncia_status."'"; ?> , <?php echo $valor->status; ?>],


          <?php } ?>

        ]);

        var options = {
          //title: 'My Daily Activities',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }
    </script>

    <div id="piechart_3d" style="width: 900px; height: 500px;"></div>
			
		</div>
	</div>
<!-- Grafico  2 FIM--> 
