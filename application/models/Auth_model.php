<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model {

	private $tabela = 'usuarios';

	public function processarLogin($dados)
	{
		$this->db->select('*');
		$this->db->join('permissoes','permissao_id = usuario_permissoes_id');		
		$this->db->where($dados);
		return $this->db->get($this->tabela)->result();		
	}	
}

/* End of file Auth_model.php */
/* Location: ./application/models/Auth_model.php */