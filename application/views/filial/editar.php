<!-- Form horizontal -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Edição de Filial</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>        	
        	</ul>
    	</div>
	</div>

	<div class="panel-body">
		<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/editarExe" method="post" enctype="multipart/form-data">
			<fieldset class="content-group">
				<legend class="text-bold">Dados Filial:</legend>

				<!-- <input  type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" /> -->

				<input  type="hidden" name="filial_id" value="<?php echo $dados[0]->filial_id; ?>" />

				<div class="form-group">
		           <label class="control-label col-lg-2">Nome da Matriz:</label>
		            <div class="col-lg-5">
		           		<input disabled type="text" class="form-control" placeholder="Nome Empresa" name="empresa_nomeFantasia" id="nomeFantasia" value="<?php echo $dados[0]->empresa_nomeFantasia; ?>">
		           		<?php echo form_error('empresa_nomeFantasia'); ?>
		           		<input  type="hidden" name="id_empresa" value="<?php echo $dados[0]->empresa_id; ?>" />
		            </div>                    
		        </div>

				<div class="form-group">
					<label class="control-label col-lg-2">CNPJ:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" placeholder="Cnpj" data-mask="99.999.999/9999-99" data-mask-selectonfocus="true" name="filial_cpfCnpj" id="cpfCnpj" value="<?php echo $dados[0]->filial_cpfCnpj; ?>">
					<?php echo form_error('filial_cpfCnpj'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Nome da Filial:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" placeholder="Nome Filial / Fantasia" name="filial_nomeFantasia" id="nomeFantasia" value="<?php echo $dados[0]->filial_nomeFantasia; ?>">
					<?php echo form_error('filial_nomeFantasia'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Razão Social:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" placeholder="Nome Razão" name="filial_nomeRazao" id="nomeRazao" value="<?php echo $dados[0]->filial_nomeRazao; ?>">
					<?php echo form_error('filial_nomeRazao'); ?>
					</div>										
				</div>	


				<legend class="text-bold">Dados Comerciais:</legend>

				<div class="form-group">
					<label class="control-label col-lg-2">CEP:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" name="filial_cep" id="cep" data-mask="99999-999" data-mask-selectonfocus="true" value="<?php echo $dados[0]->filial_cep; ?>">
					<?php echo form_error('filial_cep'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Endereço:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" placeholder="Rua/Av/Trav" name="filial_rua" id="rua" value="<?php echo $dados[0]->filial_rua; ?>">
					<?php echo form_error('filial_rua'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Numero:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" name="filial_numero" id="numero" value="<?php echo $dados[0]->filial_numero; ?>">
					<?php echo form_error('filial_numero'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Complemento:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" name="filial_complemento" id="complemento" value="<?php echo $dados[0]->filial_complemento; ?>">
					<?php echo form_error('filial_complemento'); ?>
					</div>										
				</div>


				<div class="form-group">
					<label class="control-label col-lg-2">Bairro:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" name="filial_bairro" id="bairro" value="<?php echo $dados[0]->filial_bairro;  ?>">
					<?php echo form_error('filial_bairro'); ?>
					</div>										
				</div>

				<div class="form-group">
                	<label class="control-label col-lg-2">Estado:</label>
                	<div class="col-lg-5">
                        <select  class="form-control" name="filial_estado" id="estado" >
                        	<option value="">Selecione</option>
                            <?php foreach ($estados as $valor) { ?>
                            	<?php $selected = ($valor->uf == $dados[0]->filial_estado)?'SELECTED': ''; ?>
		                              <option value="<?php echo $valor->uf; ?>" <?php echo $selected; ?>><?php echo $valor->nome; ?></option>
		                        <?php } ?>
                        </select>
                        <?php echo form_error('filial_estado'); ?>
                    </div>			                            
                </div>			                        
				<div class="form-group">
					<label class="control-label col-lg-2">Cidade:</label>
					<div class="col-lg-5">
                        <select class="form-control" name="filial_cidade" id="cidade">
                            
                            <?php foreach ($cidades as $valor) { ?>
                            	<?php $selected = ($valor->nome == $dados[0]->filial_cidade)?'SELECTED': ''; ?>
		                              <option value="<?php echo $valor->nome; ?>" <?php echo $selected; ?>><?php echo $valor->nome; ?></option>
		                        <?php } ?>
                        </select>
                    <?php echo form_error('filial_cidade'); ?>	
                    </div>				                            	                            
				</div>	

				<legend class="text-bold">Dados Contato:</legend>

				<div class="form-group">
					<label class="control-label col-lg-2">Telefone Fixo:</label>
					<div class="col-lg-5">
						<input  type="tel" class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="filial_telefone" id="telefone" value="<?php echo $dados[0]->filial_telefone; ?>">
					<?php echo form_error('filial_telefone'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Telefone Celular I:</label>
					<div class="col-lg-5">
						<input  type="tel"  class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="filial_telefone2" id="telefone2" value="<?php echo $dados[0]->filial_telefone2; ?>">
					<?php echo form_error('filial_telefone2'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Telefone Celular II:</label>
					<div class="col-lg-5">
						<input  type="tel" class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="filial_telefone3" id="telefon3" value="<?php echo $dados[0]->filial_telefone3; ?>">
					<?php echo form_error('filial_telefone3'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Email - Mala Direta:</label>
					<div class="col-lg-5">
						<input  type="text" placeholder="seu@email.com" class="form-control" name="filial_email" id="email" value="<?php echo $dados[0]->filial_email; ?>">
					<?php echo form_error('filial_email'); ?>
					</div>										 
				</div>	

				<div class="form-group">
					<label class="control-label col-lg-2">Responsável:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" placeholder="Responsável"  data-mask-selectonfocus="true" name="filial_responsavel" id="responsavel" value="<?php echo $dados[0]->filial_responsavel; ?>">
					<?php echo form_error('filial_responsavel'); ?>
					</div>										
				</div>


                </fieldset>  
				<div class="text-right">
					<button type="submit" class="btn bg-teal">Editar <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>
