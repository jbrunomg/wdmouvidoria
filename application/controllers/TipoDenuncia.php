<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipodenuncia extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('TipoDenuncia_model');
	}

	public function index()
	{
		$resultado = $this->TipoDenuncia_model->listar();

		$dadosView['dados'] = $resultado;
		$dadosView['meio']  = 'tipodenuncia/listar';
		$this->load->view('tema/tema',$dadosView);
	}


	public function adicionar()
	{		
		$dadosView['permissoes'] = $this->TipoDenuncia_model->todasPermissoes();
		$dadosView['meio']       = 'tipodenuncia/adicionar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionarExe()
	{			

		$dados = array(
		  'nome'             => $this->input->post('nome'),	
		  'data_cadastro'    => date('Y-m-d H:i:s')
		);
	
		$resultado = $this->TipoDenuncia_model->inserir($dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
		}

		redirect('Tipodenuncia', 'refresh');
	}

	public function editar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']      = $this->TipoDenuncia_model->pegarPorId($id);
		$dadosView['permissoes'] = $this->TipoDenuncia_model->todasPermissoes();
		$dadosView['meio']       = 'tipodenuncia/editar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function editarExe()
	{	
	
		$id = $this->input->post('tipodenuncia_id');

		$dados = $this->input->post();

		$resultado = $this->TipoDenuncia_model->editar($id,$dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro alterado com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para alterado o registro!');
		}

		redirect('Tipodenuncia', 'refresh');
	}

	public function excluir()
	{
		$id = $this->input->post('id');

		$resultado = $this->TipoDenuncia_model->excluir($id);

		if ($resultado) {			
			echo json_encode(array('status' => true));
		}else{
			echo json_encode(array('status' => false));
		}
	}
	
	public function visualizar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']      = $this->TipoDenuncia_model->pegarPorId($id);
		$dadosView['permissoes'] = $this->TipoDenuncia_model->todasPermissoes();
		$dadosView['meio']       = 'tipodenuncia/visualizar';
		$this->load->view('tema/tema',$dadosView);
	}

}

/* End of file Usuario.php */
/* Location: ./application/controllers/Usuario.php */