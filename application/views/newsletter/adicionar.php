<!-- Form horizontal -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Cadastro de Curso</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<!-- <li><a data-action="reload"></a></li> -->
            		<!-- <li><a data-action="close"></a></li> -->
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/adicionarExe" method="post" enctype="multipart/form-data">
				<fieldset class="content-group">
					<legend class="text-bold">Dados da vaga:</legend>

					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

					<div class="form-group">
          <label class="control-label col-lg-2">Nome da Curso:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Nome Curso" name="sempemfanta" id="sempemfanta" value="<?php echo set_value('sempemfanta'); ?>">
          <?php  echo form_error('sempemfanta'); ?>
          </div>                    
        </div>

        <div class="form-group">
          <label class="control-label col-lg-2">Razão Social:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Nome Razão" name="sempemrazao" id="sempemrazao" value="<?php echo set_value('sempemrazao'); ?>">
          <?php echo form_error('sempemrazao'); ?>
          </div>                    
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">CNPJ:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Cnpj" data-mask="99.999.999/9999-99" data-mask-selectonfocus="true" name="sempemcnpj" id="sempemcnpj" value="<?php echo set_value('sempemcnpj'); ?>">
          <?php echo form_error('sempemcnpj'); ?>
          </div>                    
        </div>


        <legend class="text-bold">Dados Comerciais:</legend>

        <div class="form-group">
          <label class="control-label col-lg-2">Endereço:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Rua/Av/Trav" name="sempemlogra" id="sempemlogra" value="<?php echo set_value('sempemlogra'); ?>">
          <?php echo form_error('sempemlogra'); ?>
          </div>                    
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">Numero:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="numero"   name="sempemnumer" id="sempemnumer" value="<?php echo set_value('sempemnumer'); ?>">
          <?php echo form_error('sempemnumer'); ?>
          </div>                    
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">Complemento:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Complemento"  name="sempemcompl" id="sempemcompl" value="<?php echo set_value('sempemcompl'); ?>">
          <?php echo form_error('sempemcompl'); ?>
          </div>                    
        </div>
<!--
        <div class="form-group">
                  <label class="control-label col-lg-2">UF:</label>
                  <div class="col-lg-5">
                        <select disabled  class="form-control" name="estado" id="estado" >
                          <option value="">Selecione</option>
                            <?php foreach ($estados as $valor) { ?>
                              <?php $selected = ($valor->id == $dados[0]->sempemuf)?'SELECTED': ''; ?>
                                  <option value="<?php echo $valor->id; ?>" <?php echo $selected; ?>><?php echo $valor->nome; ?></option>
                            <?php } ?>
                        </select>
                        <?php echo form_error('estado'); ?>
                    </div>                                  
                </div>                              
        <div class="form-group">
          <label class="control-label col-lg-2">Cidade:</label>
          <div class="col-lg-5">
                        <select disabled class="form-control" name="cidade" id="cidade">
                            
                            <?php foreach ($cidades as $valor) { ?>
                              <?php $selected = ($valor->id == $dados[0]->sestvacidad)?'SELECTED': ''; ?>
                                  <option value="<?php echo $valor->id; ?>" <?php echo $selected; ?>><?php echo $valor->nome; ?></option>
                            <?php } ?>
                        </select>
                    <?php echo form_error('cidade'); ?> 
                    </div>                                                                  
        </div> -->

        <div class="form-group">
          <label class="control-label col-lg-2">Bairro:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Bairro" name="sempembairr" id="sempembairr" value="<?php echo set_value('sempembairr');  ?>">
          <?php echo form_error('sempembairr'); ?>
          </div>                    
        </div>

        <div class="form-group">
          <label class="control-label col-lg-2">Cidade:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Cidade" name="sempemcidad" id="sempemcidad" value="<?php echo set_value('sempemcidad'); ?>">
          <?php echo form_error('sempemcidad'); ?>
          </div>                    
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">Estado:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Uf"  name="sempemuf" id="sempemuf" value="<?php echo set_value('sempemuf'); ?>">
          <?php echo form_error('sempemuf'); ?>
          </div>                    
        </div>

        <div class="form-group">
          <label class="control-label col-lg-2">CEP:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Cep"  name="sempemcep" id="sempemcep" data-mask="99999-999" data-mask-selectonfocus="true" value="<?php echo set_value('sempemcep'); ?>">
          <?php echo form_error('sempemcep'); ?>
          </div>                    
        </div>

        <legend class="text-bold">Dados Contato:</legend>

        <div class="form-group">
          <label class="control-label col-lg-2">Telefone Principal:</label>
          <div class="col-lg-5">
            <input  type="tel" class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="sempemtel01" id="sempemtel01" value="<?php echo set_value('sempemtel01'); ?>">
          <?php echo form_error('sempemtel01'); ?>
          </div>                    
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">Telefone Secundario:</label>
          <div class="col-lg-5">
            <input  type="tel"  class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="sempemtel02" id="sempemtel02" value="<?php echo set_value('sempemtel02'); ?>">
          <?php echo form_error('sempemtel02'); ?>
          </div>                    
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">Fax:</label>
          <div class="col-lg-5">
            <input  type="tel" class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="sempemtel03" id="sempemtel03" value="<?php echo set_value('sempemtel03'); ?>">
          <?php echo form_error('sempemtel03'); ?>
          </div>                    
        </div>

        <div class="form-group">
          <label class="control-label col-lg-2">Email:</label>
          <div class="col-lg-5">
            <input  type="email" placeholder="seu@email.com" class="form-control" name="sempememail" id="sempememail" value="<?php echo set_value('sempememail'); ?>">
          <?php echo form_error('sempememail'); ?>
          </div>                     
        </div>  

        <div class="form-group">
          <label class="control-label col-lg-2">Responsável:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Responsável"  data-mask-selectonfocus="true" name="sempemrespo" id="sempemrespo" value="<?php echo set_value('sempemrespo'); ?>">
          <?php echo form_error('sempemrespo'); ?>
          </div>                    
        </div>

        <div class="form-group">
          <label class="control-label col-lg-2">Cargo Responsável:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Cargo Responsável"  data-mask-selectonfocus="true" name="sempemcargo" id="sempemcargo" value="<?php echo set_value('sempemcargo'); ?>">
          <?php echo form_error('sempemcargo'); ?>
          </div>                    
        </div>


        <div class="form-group">
          <label class="control-label col-lg-2">Representante:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Representante"  data-mask-selectonfocus="true" name="sempemrepre" id="sempemrepre" value="<?php echo set_value('sempemrepre'); ?>">
          <?php echo form_error('sempemrepre'); ?>
          </div>                    
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">Cargo do Representante:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Cargo do Representante"  data-mask-selectonfocus="true" name="sempemrecar" id="sempemrecar" value="<?php echo set_value('sempemrecar'); ?>">
          <?php echo form_error('sempemrecar'); ?>
          </div>                    
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">Ramo de Atividade:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Ramo de Atividade"  data-mask-selectonfocus="true" name="sempemativi" id="sempemativi" value="<?php echo set_value('sempemativi'); ?>">
          <?php echo form_error('sempemativi'); ?>
          </div>                    
        </div>          
               		
				</fieldset>
				<div class="text-right">
					<button type="submit" class="btn bg-teal">Cadastrar <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>
