<div class="panel panel-flat col-md-3">
<div class="panel-heading">
	<h5 class="panel-title text-center">Relatórios Rápidos</h5>							
</div>
	<div class="category-content">
		<div class="row row-condensed">
			<div class="col-xs-12">
				<a href="<?php echo base_url();?>relatorios/patrimonioRapido" class="btn bg-teal-400 btn-block btn-float btn-float-lg" ><i class="icon-file-text2"></i> <span>Todos Patrimônios</span></a>
			</div>
		</div>
	</div>
</div>
<!-- Form horizontal -->

<div class="col-md-9">
<div class="panel panel-flat col-md-12">
<div class="panel-heading">
	<h5 class="panel-title text-center">Relatórios Personalizados</h5>							
</div>
	<div class="panel-body">
		<form class="form-horizontal" action="<?php echo base_url();?>relatorios/patrimonioPersonalizado/" method="post">
			<div class="form-group">
				<label class="control-label col-lg-2">Data Aquisição Início:</label>
				<div class="col-lg-4">																						
					<input type="text" class="form-control daterange-single" name="dataInicio" id="dataInicio" value="">
				</div>
				<label class="control-label col-lg-2">Data Aquisição Fim:</label>
				<div class="col-lg-4">																						
					<input type="text" class="form-control daterange-single" name="dataFim" id="dataFim" value="">
				</div>									
			</div>	
			<div class="form-group">
            	<label class="control-label col-lg-2">Estado Conservação:</label>
            	<div class="col-lg-4">
                    <select class="form-control" name="conservacao" id="conservacao">
                   		<option value="">Selecione</option>
                        <option value="1">Novo</option>
                        <option value="2">Semi-Novo</option>
                        <option value="3">Usado</option>
                    </select>
                </div>
            </div>										
			<div class="text-right">
				<button type="submit" class="btn bg-teal-400">Imprimir<i class="icon-arrow-right14 position-right"></i></button>
			</div>
		</form>
	</div>
</div>
</div>