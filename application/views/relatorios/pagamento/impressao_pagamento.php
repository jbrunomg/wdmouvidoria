<?php $totalProdutos = 0; ?>

    <!-- Bordered striped table -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Pagamento do Sistema</h5>

        </div>

        <div class="panel-body">
            <!-- Pagamentos -->
        </div>

        <div class="table-responsive">


            <div style="margin-top: 0; padding-top: 0">             

                <table class="table table-bordered table-condensed" id="tblProdutos">
                    <thead>
                        <tr><th colspan="4"><h1>Recibo de Pagamento #<?php echo $results->pagamento_sistema_id ?></h1></th></tr>
                        <tr>
                            <th style="font-size: 15px">Descrição</th>
                            <th style="font-size: 15px">Data de vencimento</th>
                            <th style="font-size: 15px">Data de pagamanto</th>                                         
                            <th style="font-size: 15px">Sub-total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php                                                                
                        
                            echo '<tr>';
                            echo '<td>'.$results->pagamento_sistema_descricao.'</td>';
                            echo '<td>'.date('d/m/Y', strtotime($results->pagamento_sistema_data_vencimento)).'</td>';
                            echo '<td>'.date('d/m/Y', strtotime($results->pagamento_sistema_data_pagamento)).'</td>';
                            echo '<td>'.$results->pagamento_sistema_valor.'</td>';                                                                                     
                            echo '</tr>';
                        ?>
                        <tr>
                            <td colspan="3" style="text-align: right"><strong>Total:</strong></td>
                            <td><strong> <?php echo $results->pagamento_sistema_valor;?></strong></td>
                        </tr>       

                    </tbody>
                </table>     

                <table class="table table-bordered table-condensed" id="tblProdutos">                  

                    <tbody>
                        <tr>
                            <td colspan="5" style="text-align: left"><strong>Observações:</strong><br><?php echo $results->pagamento_sistema_observacoes; ?></td>
                        </tr>
                    </tbody>
                </table>
                <hr size="1" style="border:1px dashed #C0C0C0;">
                <table class="table table-bordered table-condensed" id="canhoto">
                    <tbody>
                        <tr>
                            <td colspan="3" style="text-align: center"><strong>RECEBEMOS DA Carvalhal Auditoria Interna e Compliance O VALOR REFERENTE AO PAGAMENTO DE NÚMERO AO LADO</strong></td>
                            <td colspan="2" rowspan="2" style="text-align: center"><strong>PAGAMENTO: </strong><br>#<?php echo $results->pagamento_sistema_id?><br><strong>VALOR: </strong><br><?php echo  $results->pagamento_sistema_valor;?></td>
                        </tr>
                        <tr>
                            <td colspan="1" style="text-align: center;"><br><strong>Data <?php echo date('d/m/Y', strtotime($results->pagamento_sistema_data_pagamento))?></strong></td>
                            <td colspan="2" style="text-align: left"><br><strong>Assinatura: <img src="<?php echo base_url(); ?>public/assets/images/signature.png"> </strong></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /bordered striped table -->