<!-- Column selectors -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<?php if(checarPermissao('aEmpresa')){ ?>
		<div class="col-md-3 col-sm-6 row">
			<a class="btn bg-teal btn-block" href="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/adicionar">Adicionar <i class="icon-plus2 position-right"></i></a>
		</div>
		<?php } ?>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<li><a data-action="close"></a></li>
        	</ul>
    	</div>
	</div>

	<br>
	<table class="table datatable-button-html5-columns">
		<thead>
			<tr>				
				<th>Nome Fantasia</th>	
				<th>Cnpj</th>					
				<th>Contato</th>
				<th>Email</th>
				<th>Responsável</th>											
				<th>Opções</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($dados as $valor) { ?>
				
				<tr>     								
					<td><?php echo $valor->empresa_nomeFantasia; ?></td>
					<td><?php echo $valor->empresa_cpfCnpj; ?></td> 					
					<td><?php echo $valor->empresa_telefone; ?></td>
					<td><?php echo $valor->empresa_email; ?></td>
					<td><?php echo $valor->empresa_responsavel; ?></td>							
														
					<td>																				
						<ul class="icons-list">						
								<li class="text-grey"><a href="#" data-popup="tooltip" title="Gerar Botão" data-toggle="modal" data-target="#modal_iconified" empresa="<?php echo $valor->empresa_id; ?>"><i class="icon-file-xml"></i></a></li>								
								<?php if(checarPermissao('aEmpresa')){ ?>
								<li class="text-grey"><a href="<?php echo base_url()?>filial/adicionar/<?php echo $valor->empresa_id; ?>" data-popup="tooltip" title="Filial"><i class="icon-reading"></i></a></li>	
								<?php } ?>	
								<?php if(checarPermissao('eEmpresa')){ ?>
								<li class="text-primary-600"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/editar/<?php echo $valor->empresa_id; ?>" data-popup="tooltip" title="Editar"><i class="icon-pencil7"></i></a></li>
								<?php } ?>
								<?php if(checarPermissao('dEmpresa')){ ?>
								<li class="text-danger-600"><a href="#" data-popup="tooltip" title="Excluir" class="sweet_loader_id" url="<?php echo $this->uri->segment(1); ?>/excluir" registro="<?php echo $valor->empresa_id; ?>"><i class="icon-trash"></i></a></li>
								<?php } ?>
								<?php if(checarPermissao('vEmpresa')){ ?>
								<li class="text-teal-600"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/visualizar/<?php echo $valor->empresa_id; ?>" data-popup="tooltip" title="Visualizar"><i class="icon-search4"></i></a></li>	
								<?php } ?>
						</ul>																			
					</td>						
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>
<!-- /column selectors -->

<!-- Iconified modal -->
<div id="modal_iconified" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title"><i class="icon-menu7"></i> &nbsp;Gerador botão denuncia</h5>
			</div>

			<div class="modal-body">
				<div class="alert alert-info alert-styled-left text-blue-800 content-group">
	                <span class="text-semibold">Instruções!</span> Siga o exemplo abaixo.
	                <button type="button" class="close" data-dismiss="alert">×</button>
	            </div>	            
						     
				<h6 class="text-semibold"><i class="icon-embed2 position-left"></i> Ex 01: como adicionar o botão "Ouvidoria" no site do cliente</h6>
				
				<pre class="language-markup code-toolbar">
					<code class=" language-markup" id="iframe">														
						<!-- Cód javaScript -->
					</code>				
				</pre>
				<hr>

				<h6 class="text-semibold"><i class="icon-embed2 position-left"></i> Ex 02: como adicionar o botão "Canal de denuncia" no site do cliente</h6>
				
				<pre class="language-markup code-toolbar">
					<code class=" language-markup" id="iframe2">														
						<!-- Cód javaScript -->
					</code>				
				</pre>
				<hr>

				<p><i class="icon-mention position-left"></i> Copie o cód acima e cole dentro do elemento &lt;body&gt; do seu site.</p>
			</div>

			<div class="modal-footer">
				<button class="btn btn-link" data-dismiss="modal"><i class="icon-cross"></i> Close</button>
				<!-- <button class="btn btn-primary"><i class="icon-check"></i> Save</button> -->
			</div>
		</div>
	</div>
</div>
<!-- /iconified modal -->

<script type="text/javascript">
$(document).ready(function(){


   $(document).on('click', 'a', function(event) {
        
        var empresa = $(this).attr('empresa');
        $('#idEmpresa').val(empresa);

        // var span = '<span> <?php echo base_url(); ?>cotacaofornecedor/carregarcotacao/'+fornecedor+'</span>';

        // var span = '<span class="token attr-name"> &lt;a href="http://ouvidoria.carvalhal.com.br/denunciaiframe/carregarIframe/'+empresa+'" target="_blank" title="Faça seu relatos de forma anônima!" &gt; &lt;img src="http://ouvidoria.carvalhal.com.br/public/assets/images/button_ouvidoria.png" alt="Faça seu relatos de forma anônima!" /&gt; &lt;/a&gt; </span>';
         var span = '<span class="token attr-name"> &lt;a href="<?php echo base_url(); ?>apidenuncia/carregarapi/'+empresa+'" target="_blank" title="Faça seu relatos de forma anônima!" &gt; &lt;img src="<?php echo base_url(); ?>public/assets/images/button_ouvidoria.png" alt="Faça seu relatos de forma anônima!" /&gt; &lt;/a&gt; </span>';

        
		document.getElementById("iframe").innerHTML = span;

		// var span2 = '<span class="token attr-name"> &lt;a href="http://ouvidoria.carvalhal.com.br/denunciaiframe/carregarIframe/'+empresa+'" target="_blank" title="Faça seu relatos de forma anônima!" &gt; &lt;img src="http://ouvidoria.carvalhal.com.br/public/assets/images/button_denuncia.png" alt="Faça seu relatos de forma anônima!" /&gt; &lt;/a&gt; </span>';
		var span2 = '<span class="token attr-name"> &lt;a href="<?php echo base_url(); ?>apidenuncia/carregarapi/'+empresa+'" target="_blank" title="Faça seu relatos de forma anônima!" &gt; &lt;img src="<?php echo base_url(); ?>public/assets/images/button_denuncia.png" alt="Faça seu relatos de forma anônima!" /&gt; &lt;/a&gt; </span>';

		document.getElementById("iframe2").innerHTML = span2;

    });

});

</script>