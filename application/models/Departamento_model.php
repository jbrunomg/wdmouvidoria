<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// class Usuario_model extends CI_Model
class Departamento_model extends MY_Model {

	public $tabela  = 'departamento';
	public $chave   = 'departamento_id';
	public $visivel = 'departamento_visivel';

	// CRUD NA PASTA application/core/MY_Model.php //

}

/* End of file Usuario_model.php */
/* Location: ./application/models/Usuario_model.php */