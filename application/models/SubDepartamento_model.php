<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// class Usuario_model extends CI_Model
class SubDepartamento_model extends MY_Model {

	public $tabela  = 'sub_departamento';
	public $chave   = 'sub_departamento_id';
	public $visivel = 'sub_departamento_visivel';

	// CRUD NA PASTA application/core/MY_Model.php //

	public function listarGo()
	{
		$this->db->select('departamento.nome AS nome_depart, sub_departamento.*');
		$this->db->join('departamento','departamento_id = id_departamento');	
		$this->db->where($this->visivel, 1);	
		return $this->db->get($this->tabela)->result();	
	}



}

/* End of file Usuario_model.php */
/* Location: ./application/models/Usuario_model.php */