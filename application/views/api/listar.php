<!-- Column selectors -->
<div class="panel panel-flat">
<!-- 	<div class="panel-heading">
		<?php if(checarPermissao('aDenuncia')){ ?>
		<div class="col-md-3 col-sm-6 row">
			<a class="btn bg-teal btn-block" href="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/adicionar">Adicionar <i class="icon-plus2 position-right"></i></a>
		</div>
		<?php } ?>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<li><a data-action="reload"></a></li>
        		<li><a data-action="close"></a></li>
        	</ul>
    	</div>
	</div> -->

	<br>
	<table class="table datatable-button-html5-columns">
		<thead>
			<tr>				
				<th>Empresa</th>	
				<th>Tipo Denuncia</th>					
				<th>Departamento</th>
				<th>Data</th>
				<th>Status</th>											
				<th>Opções</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($dados as $valor) { 
			
			  switch ($valor->denuncia_status){
			    case 'aberto':
			      $status = '<span class="badge bg-blue">Aberto</span>';
			      break;
			    case 'analise':
			      $status = '<span class="badge bg-success-400">Em Análise</span>';
			      break;
			    case 'encerrado':
			      $status = '<span class="badge bg-danger">Encerrado</span>';
			      break;
			    default:
			      $status = '<span class="badge bg-grey-400">Não Informado</span>';
			      break;
			  }
			?>				
				<tr>     								
					<td><?php echo strtoupper($valor->empresa_nomeFantasia); ?></td>
					<td><?php echo $valor->tp_denuncia; ?></td> 					
					<td><?php echo $valor->dp_nome; ?></td>
					<td><?php echo date('d/m/Y', strtotime($valor->denuncia_data_cadastro)); ?></td>
					<td><?php echo $status; ?></td>							
														
					<td>																				
						<ul class="icons-list">						
							
							<?php if(checarPermissao('vDenuncia')){ ?>
							<li class="text-teal-600"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/visualizar/<?php echo $valor->denuncia_id; ?>" data-popup="tooltip" title="Visualizar"><i class="icon-search4"></i></a></li>	
							<?php } ?>							
							
						</ul>																			
					</td>						
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>
<!-- /column selectors -->