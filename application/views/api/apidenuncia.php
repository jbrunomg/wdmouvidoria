<?php //var_dump($empresas);die(); 
$tipos_de_arquivo_permitidos = array(
  '.txt',
  '.jpg',
  '.jpeg',
  '.gif',
  '.png',
  '.pdf',
  '.xls',
  '.xlsx',
  '.xlsm',
  '.ods',
  '.zip',
  '.rar',
  '.mp4',
  '.mp3'
);
$valores_aceitos = implode(', ', $tipos_de_arquivo_permitidos);
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>WDM - Ouvidoria</title>

  <!-- Global stylesheets -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url(); ?>public/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url(); ?>public/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url(); ?>public/assets/css/core.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url(); ?>public/assets/css/components.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url(); ?>public/assets/css/colors.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url(); ?>public/assets/css/timepicki.css" rel="stylesheet" type="text/css">
  <!-- /global stylesheets -->

  <!-- Estilização local -->
  <link href="<?= base_url(); ?>public/assets/css/fileButton.css" rel="stylesheet" type="text/css">
  <!-- /estilização local -->

  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/core/libraries/jquery.min.js"></script>

  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/maps/google/basic/basic.js"></script>



  <script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';
    var token = '';
  </script>


</head>

<body>
  <!-- Main content -->
  <div class="content-wrapper">

    <!-- Main navbar -->
    <div class="navbar navbar-inverse">
      <div class="navbar-header">
        <!-- <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>public/assets/images/logo_light.png" alt=""></a> -->
        <a class="navbar-brand" href="#"><i class="icon-headset"></i><span><strong>&nbsp;&nbsp;&nbsp; WDM-Ouvidoria </strong></span></a>


        <!--    <ul class="nav navbar-nav pull-right visible-xs-block">
        <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
        <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
      </ul> -->
      </div>

    </div>
    <!-- /main navbar -->


    <!-- Page container -->
    <div class="page-container">

      <!-- Page content -->
      <div class="page-content">



        <!-- Page header -->
        <div class="page-header">
          <!--           <div class="page-header-content">
            <div class="page-title">
              <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Denuncia</span>  Adicionar</h4>
            </div>

           <div class="heading-elements">
              <div class="heading-btn-group">
                <a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Estatísticas</span></a>               
              </div>
            </div>
          </div> -->

          <div class="breadcrumb-line">
            <ul class="breadcrumb">
              <li><a href="#"><i class="icon-home2 position-left"></i> Denuncia</a></li>
              <li class="active">Adicionar</li>
            </ul>

            <ul class="breadcrumb-elements">
              <li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
            </ul>
          </div>
        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">


          <!-- Form horizontal -->
          <div class="panel panel-flat">


            <form class="form-horizontal" action="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/adicionarApi" method="post" enctype="multipart/form-data">


              <div class="panel-heading">
                <h5 class="panel-title">Cadastro</h5>
                <!--           <div class="heading-elements">
                  <ul class="icons-list">
                     <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                  </ul>
                </div> -->
              </div>

              <div class="panel-body">
                <div class="row">
                  <div class="col-md-12">
                    <fieldset>
                      <legend class="text-semibold"><i class="icon-reading position-left"></i>Detalhar Denuncia</legend>

                      <div class="form-group">
                        <label class="col-lg-3 control-label">Empresa:</label>
                        <div class="col-lg-9">
                          <input disabled type="text" class="form-control" placeholder="Empresa" name="empresa" id="id_empresa" value="<?php echo $empresas[0]->empresa_nomeFantasia; ?>">
                          <input type="hidden" class="form-control" name="id_empresa" value="<?php echo $empresas[0]->empresa_id; ?>">
                          <?php echo form_error('id_empresa'); ?>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-lg-3 control-label">Unidade/ Localidade:</label>
                        <div class="col-lg-9">
                          <input type="text" class="form-control" placeholder="Und/Loc" name="denuncia_und_localidade">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-lg-3 control-label">Data do fato:</label>
                        <div class="col-lg-9">
                          <input type="date" class="form-control" placeholder="Data do Fato" name="denuncia_data_fato">
                        </div>
                      </div>

                      <!--               <div class="form-group">
                        <label class="col-lg-3 control-label">Usuário:</label>
                        <div class="col-lg-9">
                          <input disabled type="text" class="form-control" placeholder="Usuário" value="<?php echo $this->session->userdata('usuario_nome'); ?>">
                          <input type="hidden" class="form-control"  name="id_usuario"  value="<?php echo $this->session->userdata('usuario_id'); ?>">
                        </div>
                      </div> -->

                      <div class="form-group">
                        <label class="col-lg-3 control-label">Departamento:</label>
                        <div class="col-lg-9">
                          <!-- <select name="id_departamento" id="departemento" class="js-example-basic-single js-states form-control" required></select>            -->
                          <select class="form-control" name="id_departamento" id="departamento">
                            <option value="">Selecione</option>
                            <?php foreach ($departamento as $d) { ?>
                              <option value="<?php echo $d->departamento_id; ?>"><?php echo $d->nome; ?></option>
                            <?php } ?>
                          </select>
                          <?php echo form_error('id_departamento'); ?>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-lg-3 control-label">Tipo de Denuncia:</label>
                        <div class="col-lg-9">
                          <select class="form-control" name="id_tipodenuncia" id="id_tipodenuncia">
                            <option value="">Selecione</option>
                            <?php foreach ($tipodenuncia as $t) { ?>
                              <option value="<?php echo $t->tipodenuncia_id; ?>"><?php echo $t->nome; ?></option>
                            <?php } ?>
                          </select>
                          <?php echo form_error('id_tipodenuncia'); ?>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-lg-3 control-label">Status:</label>
                        <div class="col-lg-9">
                          <select class="form-control" name="denuncia_status" id="denuncia_status">
                            <option value="aberto">Arbertura</option>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-lg-3 control-label">Relato:</label>
                        <div class="col-lg-9">
                          <textarea class="form-control rounded-0" name="denuncia_relato" id="denuncia_relato" rows="3"></textarea>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-lg-3 control-label">Anexo:</label>
                        <p class="col-lg-9" style="color: #999999;">&lpar;Tamanho máximo: 16 MB&rpar;</p>
                        <div class="col-lg-9 file-input">
                          <input class="form-control" name="denuncia_anexo[]" id="denuncia_anexo" type="file" accept="<?= $valores_aceitos; ?>" multiple>
                          <button id="apagar_anexo" type="button" class="btn btn-ex" onclick="apagarAnexo()">Excluir</button>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-lg-3 control-label">Denunciante:</label>
                        <div class="col-lg-9">
                          <input type="text" class="form-control" placeholder="Responsável" data-mask-selectonfocus="true" name="denuncia_nome" id="denuncia_nome" value="<?php echo set_value('denuncia_nome'); ?>">
                          <?php echo form_error('denuncia_nome'); ?>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-lg-3 control-label">Telefone:</label>
                        <div class="col-lg-9">
                          <input type="tel" class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="denuncia_contato" id="contato" value="<?php echo set_value('denuncia_contato'); ?>">
                          <?php echo form_error('denuncia_contato'); ?>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-lg-3 control-label">Email:</label>
                        <div class="col-lg-9">
                          <input type="text" placeholder="seu@email.com" class="form-control" name="denuncia_email" id="email" value="<?php echo set_value('denuncia_email'); ?>">
                          <?php echo form_error('denuncia_email'); ?>
                        </div>
                      </div>


                    </fieldset>
                  </div>

                  <!-- <div class="col-md-6">
                    <fieldset>
                      <legend class="text-semibold"><i class="icon-drawer-in position-left"></i> Anexo(s)</legend>
                              
                    </fieldset>
                  </div>
                </div> -->

                  <div class="text-right">
                    <button type="submit" class="btn bg-teal">Cadastrar <i class="icon-arrow-right14 position-right"></i></button>
                  </div>
                </div>

            </form>

          </div>

        </div>
        <!-- /main content -->

      </div>
      <!-- /page content -->

    </div>
    <!-- /page content -->
  </div>
  <!-- /page container -->


  <!-- Core JS files -->
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/loaders/pace.min.js"></script>

  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/core/libraries/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/loaders/blockui.min.js"></script>
  <!-- /core JS files -->

  <!-- Theme JS files -->
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/core/libraries/jquery_ui/core.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/styling/uniform.min.js"></script>
  <!-- <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/styling/switchery.min.js"></script> -->
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/selects/select2.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/selects/selectboxit.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/selects/bootstrap_select.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/notifications/pnotify.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/notifications/noty.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/notifications/jgrowl.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/core/app.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/visualization/d3/d3.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/ui/moment/moment.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/pickers/daterangepicker.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/extensions/session_timeout.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/ui/prism.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/tables/datatables/datatables.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/notifications/bootbox.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/notifications/sweet_alert.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/tables/footable/footable.min.js"></script>


  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/core/libraries/jasny_bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/inputs/autosize.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/inputs/formatter.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/inputs/typeahead/handlebars.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/inputs/passy.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/inputs/maxlength.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/editors/summernote/summernote.min.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/datatables_extension_buttons_html5.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/components_modals.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/table_responsive.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/components_popups.js"></script>

  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/form_inputs.js"></script>

  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/form_controls_extended.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/editor_summernote.js"></script>

  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/timepicki.js"></script>



  <!-- Theme JS files -->
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/visualization/echarts/echarts.js"></script>

  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/charts/echarts/combinations.js"></script>

  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/media/fancybox.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/gallery_library.js"></script>

  <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/action.js"></script>

  <!-- /theme JS files -->

  <?php if (!(NULL === $this->session->flashdata('sucesso'))) { ?>
    <script type="text/javascript">
      swal({
        title: "Ok!",
        text: "<?php echo $this->session->flashdata('sucesso'); ?>",
        confirmButtonColor: "#66BB6A",
        type: "success"
      });
    </script>
  <?php } ?>

  <?php if (!(NULL === $this->session->flashdata('erro'))) { ?>
    <script type="text/javascript">
      swal({
        title: "Oops...",
        text: "<?php echo $this->session->flashdata('erro'); ?>",
        confirmButtonColor: "#EF5350",
        type: "error"
      });
    </script>
  <?php } ?>

  <?php
  $this->session->unset_userdata('sucesso');
  $this->session->unset_userdata('erro');
  ?>


  <script type="text/javascript">
    // AJAX loader
    //  $('.sweet_loader_id').on('click', function() {  
    $(document).on('click', '.sweet_loader_id', function() {

      var id = $(this).attr('registro');
      var url = $(this).attr('url');

      console.log(url);

      swal({
          title: "Deseja realmente excluir o registro?",
          text: "",
          type: "info",
          showCancelButton: true,
          closeOnConfirm: false,
          confirmButtonColor: "#2196F3",
          showLoaderOnConfirm: true
        },
        function() {

          $.ajax({
              url: '<?php echo base_url(); ?>' + url,
              type: 'POST',
              dataType: 'json',
              data: {
                id: id,
                token: token
              },
            })
            .done(function(resultado) {

              if (resultado.status == true) {
                swal({
                  title: "Registro excluido com sucesso!",
                  confirmButtonColor: "#2196F3"
                });
              } else {
                swal({
                  title: "Tivemos problema para excluir o registro!",
                  confirmButtonColor: "#2196F3"
                });
              }

              window.location.reload();
            });


        });
    });


    const inputAnexo = document.getElementById('denuncia_anexo');
    const apagarAnexoBtn = document.getElementById('apagar_anexo');
    const extensoesPermitidas = [
      '.jpg', '.jpeg', '.gif', '.png', '.pdf', '.xls', '.xlsx', '.xlsm', '.ods', '.zip', '.rar', '.mp4', '.mp3', '.txt'
    ];

    inputAnexo.addEventListener('change', () => {
      let anexosValidos = true;
      for (const anexo of inputAnexo.files) {
        if (anexo.name.length > 53) {
          swal({
            title: "Oops...",
            text: "O nome do arquivo é muito longo. O tamanho máximo é de 53 caracteres. Nome do arquivo: " + anexo.name,
            confirmButtonColor: "#EF5350",
            type: "error",
          });
          anexosValidos = false;
        }
        if (anexo.size > 16 * 1024 * 1024) {
          swal({
            title: "Oops...",
            text: "Arquivo muito grande: " + anexo.name,
            confirmButtonColor: "#EF5350",
            type: "error",
          });
          anexosValidos = false;
        }

        const extensao = anexo.name.slice(((anexo.name.lastIndexOf(".") - 1) >>> 0) + 2);
        if (!extensoesPermitidas.includes('.' + extensao.toLowerCase())) {
          swal({
            title: "Oops...",
            text: "Tipo de arquivo não permitido. Tipos permitidos: " + extensoesPermitidas.join(', '),
            confirmButtonColor: "#EF5350",
            type: "error",
          });
          anexosValidos = false;
        }
      }
      if (anexosValidos) {
        apagarAnexoBtn.style.display = 'block';
      } else {
        inputAnexo.value = null;
      }
    });

    function apagarAnexo() {
      inputAnexo.value = null;
      apagarAnexoBtn.style.display = 'none';
    }
  </script>

</body>

</html>