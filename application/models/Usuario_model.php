<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// class Usuario_model extends CI_Model
class Usuario_model extends MY_Model {

	public $tabela  = 'usuarios';
	public $chave   = 'usuario_id';
	public $visivel = 'usuario_visivel';

	// CRUD NA PASTA application/core/MY_Model.php //

	public function listarGo()
	{
		$this->db->select('*');
		$this->db->join('permissoes','permissao_id = usuario_permissoes_id');	
		$this->db->where($this->visivel, 1);	
		return $this->db->get($this->tabela)->result();	
	}


}

/* End of file Usuario_model.php */
/* Location: ./application/models/Usuario_model.php */