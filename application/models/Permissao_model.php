<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//class Permissao_model extends CI_Model
class Permissao_model extends MY_Model {

	public $tabela  = 'permissoes';
	public $chave   = 'permissao_id';
	public $visivel = 'permissao_visivel';

	// CRUD NA PASTA application/core/MY_Model.php //


}

/* End of file Permissao_model.php */
/* Location: ./application/models/Permissao_model.php */