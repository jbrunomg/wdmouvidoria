<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Empresa extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Empresa_model');
	}

	public function index()
	{
		$resultado = $this->Empresa_model->listar();
		$dadosView['dados'] = $resultado;		
		$dadosView['meio']  = 'empresa/listar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionar()
	{
		$dadosView['estados']    = $this->Empresa_model->todosEstados();		
		$dadosView['permissoes'] = $this->Empresa_model->todasPermissoes();
		$dadosView['meio']       = 'empresa/adicionar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionarExe()
	{	
		$Cnpj =  $this->input->post('empresa_cpfCnpj'); 
		
		$resultadoCnpj = $this->Empresa_model->pegarPorCnpj($Cnpj);

		if ($resultadoCnpj) {	

			$this->session->set_flashdata('erro', "O Empresa: ".'\n' .$resultadoCnpj[0]->nomeRazao.'\n'. " já está cadastrado em nosso sistema!");

		} else {

			$dados1 = $this->input->post(); 	

			$dados2 = array( 		  	  
			    'empresa_data_cadastro' => date('Y-m-d') // Data do 1º dia de cadastro				
			);
			
			$dados  = array_merge($dados1,$dados2);

			$resultado = $this->Empresa_model->inserir($dados);

			if ($resultado) {			
				$this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');
			} else {
				$this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
			}			
		}	

		redirect('Empresa', 'refresh');
	}


	public function editar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']      = $this->Empresa_model->pegarPorId($id);
		$dadosView['cidades']    = $this->Empresa_model->todasCidadesIdEstado($dadosView['dados'][0]->empresa_estado);
		$dadosView['estados']    = $this->Empresa_model->todosEstados();		
		$dadosView['permissoes'] = $this->Empresa_model->todasPermissoes();
		$dadosView['meio']       = 'empresa/editar';


		$this->load->view('tema/tema',$dadosView);
	}

	public function editarExe()
	{	

		$id = $this->input->post('empresa_id');

		$dados = $this->input->post();

		$resultado = $this->Empresa_model->editar($id,$dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro alterado com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para alterado o registro!');
		}

		redirect('Empresa', 'refresh');
	}

	public function excluir()
	{
		$id = $this->input->post('id');

		$dados = array(
						'empresa_visivel' => 0
						
					);

		$resultado = $this->Empresa_model->excluir($id,$dados);

		if ($resultado) {			
			echo json_encode(array('status' => true));
		}else{
			echo json_encode(array('status' => false));
		}
	}
	
	public function visualizar()
	{
		$this->load->model('Filial_model');
		$this->load->model('Denuncia_model');
		$this->load->model('Dashboard_model');
		
		$id = $this->uri->segment(3);
		$campo = 'id_empresa';

		$dadosView['filial']      = $this->Filial_model->listarPorId($id,$campo);
		$dadosView['denuncia']    = $this->Denuncia_model->listarGo($id,$campo);

		$dadosView['dadosdenuncia'] = $this->Dashboard_model->graficoDenuncia($id);		
		$dadosView['dadosstatus']   = $this->Dashboard_model->graficoStatus($id);

		$dadosView['dados']       = $this->Empresa_model->pegarPorId($id);
		$dadosView['cidades']     = $this->Empresa_model->todasCidadesIdEstado($dadosView['dados'][0]->empresa_estado);
		$dadosView['estados']     = $this->Empresa_model->todosEstados();		
		$dadosView['permissoes']  = $this->Empresa_model->todasPermissoes();
		$dadosView['meio']       = 'empresa/visualizar';

		$this->load->view('tema/tema',$dadosView);

	}

	public function cidadesPorId()
	{
		$uf = $this->input->post('empresa_estado'); 	

        $cidades = $this->Empresa_model->todasCidadesIdEstado($uf);
        
        echo  "<option value=''>Selecionar uma cidade</option>";
        foreach ($cidades as $cidade) {
           echo "<option value='".$cidade->id."'>".$cidade->nome."</option>";
        }
	}

	public function selecionarCidade(){
		
		$estado  = $this->input->post('empresa_estado');
		$cidades = $this->Empresa_model->selecionarCidades($estado);

		echo  "<option value=''>Selecionar uma cidade</option>";
		foreach ($cidades as $cidade) {
			echo "<option value='".$cidade->id_cidade."'>".$cidade->nome."</option>";
		}

	}

	public function selecionarEmpresas()
  	{   
    	$termo  = $this->input->get('term');
        $ret['results'] = $this->Empresa_model->selecionarEmpresas($termo['term']);
    	echo json_encode($ret);
  	}



	public function wscnpj()
    {
        $caracteres = array(".","/","-");
        $cpfCnpj  = $this->input->post('documento');        
        $cpfCnpj = str_replace($caracteres, "", $cpfCnpj);       
        $json = file_get_contents('https://receitaws.com.br/v1/cnpj/'.$cpfCnpj);
        
        echo $json;
    }

    public function wscep()
    {
        $cep =  explode("-",$this->input->post('cep'));
        $cep = $cep[0].$cep[1];
        $xml = file_get_contents('http://webservices.profissionaisdaweb.com.br/cep/'.$cep.'.xml');
        $xml = simplexml_load_string($xml);
        echo json_encode($xml);
    }


	public function Mask($mask,$str){

	    $str = str_replace(" ","",$str);

	    for($i=0;$i<strlen($str);$i++){
	        $mask[strpos($mask,"#")] = $str[$i];
	    }

	    return $mask;
	}

	
}

/* End of file Empresa.php */
/* Location: ./application/controllers/Empresa.php */