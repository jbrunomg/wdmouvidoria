				</div>
				<!-- /main content -->

			</div>
			<!-- /page content -->

		</div>
		<!-- /page content -->
	</div>
	<!-- /page container -->

	
	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/loaders/pace.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/core/libraries/jquery_ui/core.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/styling/uniform.min.js"></script>
	<!-- <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/styling/switchery.min.js"></script> -->
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/selects/selectboxit.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/selects/bootstrap_select.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/notifications/pnotify.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/notifications/noty.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/notifications/jgrowl.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/core/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>public/assets/js/plugins/visualization/d3/d3.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>public/assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>public/assets/js/plugins/pickers/daterangepicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>public/assets/js/plugins/extensions/session_timeout.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>public/assets/js/plugins/ui/prism.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>public/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>public/assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>public/assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>public/assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>public/assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>public/assets/js/plugins/notifications/bootbox.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>public/assets/js/plugins/notifications/sweet_alert.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>public/assets/js/plugins/tables/footable/footable.min.js"></script>

	<!-- <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/assets/js/plugins/pickers/anytime.min.js"></script> -->
	<!-- <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/assets/js/plugins/pickers/pickadate/picker.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/assets/js/plugins/pickers/pickadate/picker.date.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/assets/js/plugins/pickers/pickadate/picker.time.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/assets/js/plugins/pickers/pickadate/legacy.js"></script> -->


	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/core/libraries/jasny_bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/inputs/autosize.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/inputs/formatter.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/inputs/typeahead/handlebars.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/inputs/passy.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/inputs/maxlength.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/editors/summernote/summernote.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	



	<!-- <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/colors_teal.js"></script> 	
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/dashboard.js"></script> 
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/extra_session_timeout.js"></script> -->
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/datatables_extension_buttons_html5.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/components_modals.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/table_responsive.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/components_popups.js"></script>
	<!-- <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/form_checkboxes_radios.js"></script> -->	
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/form_inputs.js"></script>
	<!-- <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/picker_date.js"></script> -->
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/form_controls_extended.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/editor_summernote.js"></script>

	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/timepicki.js"></script>

	
	<!-- <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/jquery.maskMoney.min.js"></script> --> 


	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/visualization/echarts/echarts.js"></script>

	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/charts/echarts/combinations.js"></script>

	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/media/fancybox.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/gallery_library.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/action.js"></script>

	<!-- /theme JS files -->	


	<?php if(!(NULL === $this->session->flashdata('sucesso'))){ ?>
		<script type="text/javascript">			   
		        swal({
		            title: "Ok!",
		            text: "<?php echo $this->session->flashdata('sucesso'); ?>",
		            confirmButtonColor: "#66BB6A",
		            type: "success"
		        });			  
		</script>
	<?php } ?>
	
	<?php if(!(NULL === $this->session->flashdata('erro'))){ ?>
		<script type="text/javascript">
			        swal({
			            title: "Oops...",
			            text: "<?php echo $this->session->flashdata('erro'); ?>",
			            confirmButtonColor: "#EF5350",
			            type: "error"
			        });
		</script>
	<?php } ?>

	<?php 
		$this->session->unset_userdata('sucesso');
		$this->session->unset_userdata('erro');
	?>



	<script type="text/javascript">

		// AJAX loader
	  //  $('.sweet_loader_id').on('click', function() {  
	    $(document).on('click','.sweet_loader_id', function() {   

	    	var id = $(this).attr('registro');
	    	var url = $(this).attr('url');

	    	console.log(url);

	        swal({
	            title: "Deseja realmente excluir o registro?",
	            text: "",
	            type: "info",
	            showCancelButton: true,
	            closeOnConfirm: false,
	            confirmButtonColor: "#2196F3",
	            showLoaderOnConfirm: true
	        },
	        function() {

	        	$.ajax({
	        		url: '<?php echo base_url() ?>'+url,
	        		type: 'POST',
	        		dataType: 'json',
	        		data: {id: id,token:token},
	        	})
	        	.done(function(resultado) {
	        		
	        		if(resultado.status == true){
	        			swal({
		                    title: "Registro excluido com sucesso!",
		                    confirmButtonColor: "#2196F3"
	                	});
	        		}else{
	        			swal({
		                    title: "Tivemos problema para excluir o registro!",
		                    confirmButtonColor: "#2196F3"
	               		});
	        		}

	        		 window.location.reload();
	        	});
	        	
	           
	        });
	    });
	</script>
	
</body>
</html>