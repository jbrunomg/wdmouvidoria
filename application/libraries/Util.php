<?php

class Util
{

    function removerCaracteresEspeciais($string)
    {
        //      Remove acentos de letras minúsculas.     //
        $string = preg_replace('/[áàâãä]/u', 'a', $string);
        $string = preg_replace('/[éèêë]/u', 'e', $string);
        $string = preg_replace('/[íìîï]/u', 'i', $string);
        $string = preg_replace('/[óòôõö]/u', 'o', $string);
        $string = preg_replace('/[úùûü]/u', 'u', $string);
        $string = preg_replace('/[ç]/u', 'c', $string);
        //      /remove acentos de letras minúsculas.     //

        //      Remove acentos de letras maiúsculas.     //
        $string = preg_replace('/[ÁÀÂÃÄ]/u', 'A', $string);
        $string = preg_replace('/[ÉÈÊË]/u', 'E', $string);
        $string = preg_replace('/[ÍÌÎÏ]/u', 'I', $string);
        $string = preg_replace('/[ÓÒÔÕÖ]/u', 'O', $string);
        $string = preg_replace('/[ÚÙÛÜ]/u', 'U', $string);
        $string = preg_replace('/[Ç]/u', 'C', $string);
        //      /remove acentos de letras maiúsculas.     //

        //      Remove outros caracteres especiais, exceto pontos e espaços.        //
        $string = preg_replace('/[^a-zA-Z0-9\s\.\(\)-]/', '', $string);
        //      /remove outros caracteres especiais, exceto pontos e espaços.        //
        return $string;
    }
}
