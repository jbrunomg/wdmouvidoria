<!-- Column selectors -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<?php if(checarPermissao('aEmpresa')){ ?>
		<div class="col-md-3 col-sm-6 row">
			<a class="btn bg-teal btn-block" href="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/adicionar/<?php echo $dados[0]->id_empresa; ?>">Adicionar <i class="icon-plus2 position-right"></i></a>
		</div>
		<?php } ?>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<li><a data-action="close"></a></li>
        	</ul>
    	</div>
	</div>

	<br>
	<table class="table datatable-button-html5-columns">
		<thead>
			<tr>
			    <th>Nome Matrix</th>				
				<th>Nome Fantasia</th>	
				<th>Cnpj</th>					
				<th>Contato</th>
				<th>Email</th>
				<th>Responsável</th>											
				<th>Opções</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($dados as $valor) { ?>
				
				<tr> 
					<td><?php echo $valor->empresa_nomeFantasia; ?></td>    								
					<td><?php echo $valor->filial_nomeFantasia; ?></td>
					<td><?php echo $valor->filial_cpfCnpj; ?></td> 					
					<td><?php echo $valor->filial_telefone; ?></td>
					<td><?php echo $valor->filial_email; ?></td>
					<td><?php echo $valor->filial_responsavel; ?></td>							
														
					<td>																				
						<ul class="icons-list">
								<?php if(checarPermissao('eEmpresa')){ ?>
								<li class="text-primary-600"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/editar/<?php echo $valor->filial_id; ?>" data-popup="tooltip" title="Editar"><i class="icon-pencil7"></i></a></li>
								<?php } ?>
								<?php if(checarPermissao('dEmpresa')){ ?>
								<li class="text-danger-600"><a href="#" data-popup="tooltip" title="Excluir" class="sweet_loader_id" url="<?php echo $this->uri->segment(1); ?>/excluir" registro="<?php echo $valor->filial_id; ?>"><i class="icon-trash"></i></a></li>
								<?php } ?>
								<?php if(checarPermissao('vEmpresa')){ ?>
								<li class="text-teal-600"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/visualizar/<?php echo $valor->filial_id; ?>" data-popup="tooltip" title="Visualizar"><i class="icon-search4"></i></a></li>	
								<?php } ?>																				
							
						</ul>																			
					</td>						
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>
<!-- /column selectors -->