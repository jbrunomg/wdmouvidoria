<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class MY_Model extends CI_Model {

    function __construct() {
       parent::__construct();
    }


    /* CRUD PADRÃO */
    /* Bruno Magnata 2019 */

    public function listar()
	{
		$this->db->select('*');
		$this->db->where($this->visivel,'1');
		return $this->db->get($this->tabela)->result();
	}

	public function inserir($dados)
	{
        $this->db->insert($this->tabela, $dados);
        if ($this->db->affected_rows() == '1')
            return $this->db->insert_id();
        return false;
	}

	public function editar($id,$dados)
	{
		$this->db->where($this->chave,$id);		
		
		if($this->db->update($this->tabela,$dados))
		{
			return true;
		}

		return false;
	}

	public function excluir($id)
	{
		$this->db->where($this->chave,$id);
		$this->db->update($this->tabela,[$this->visivel => 0]);
		
		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}


	/* SQL GENERICO - PADRÃO */
	/* Bruno Magnata 2019 */

	public function todasPermissoes()
	{
		$this->db->select('*');		
		return $this->db->get('permissoes')->result();		
	}

	public function todosEstados()
	{
		$this->db->select('*');				
		return $this->db->get('tb_estados')->result();
	}
	
	public function todasCidadesIdEstado($estado)
	{
		$this->db->select('*');		
		$this->db->where('uf',$estado);		
		return $this->db->get('tb_cidades')->result();
	}

	public function getAllByTable($tabela,$visivel = false)
	{
		$this->db->select('*');
		if($visivel)
		$this->db->where("visivel",'1');
		return $this->db->get($tabela)->result();
	}
 
	public function pegarPorId($id)
	{
		$this->db->select('*');		
		$this->db->where($this->visivel,'1');
		$this->db->where($this->chave,$id);
		return $this->db->get($this->tabela)->result();
	}	

   

}