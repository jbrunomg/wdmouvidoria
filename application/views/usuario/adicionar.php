<!-- Form horizontal -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Cadastro de Usuário</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<!-- <li><a data-action="reload"></a></li> -->
			                		<!-- <li><a data-action="close"></a></li> -->
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/adicionarExe" method="post" enctype="multipart/form-data">
								<fieldset class="content-group">
									<legend class="text-bold">Dados Pessoais:</legend>

									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />
 
									<div class="form-group">
										<label class="control-label col-lg-2">Nome:</label>
										<div class="col-lg-5">
											<input type="text" class="form-control" placeholder="Nome do Usuário" name="nome" id="nome" value="<?php echo set_value('nome'); ?>">
										<?php echo form_error('nome'); ?>
										</div>										
									</div>

									<div class="form-group">
										<label class="control-label col-lg-2">CPF:</label>
										<div class="col-lg-5">
											<input type="text" class="form-control" placeholder="" data-mask="999.999.999-99" data-mask-selectonfocus="true" name="cpf" id="cpf" value="<?php echo set_value('cpf'); ?>">
										<?php echo form_error('cpf'); ?>
										</div>										
									</div>
									<div class="form-group">
										<label class="control-label col-lg-2">RG:</label>
										<div class="col-lg-5">
											<input type="text" class="form-control" placeholder="" data-mask="9.999.999" data-mask-selectonfocus="true" name="rg" id="rg" value="<?php echo set_value('rg'); ?>">
										<?php echo form_error('rg'); ?>
										</div>										
									</div>									
									
									<div class="form-group">
			                        	<label class="control-label col-lg-2">Sexo:</label>
			                        	<div class="col-lg-5">
				                            <select class="form-control" name="sexo" id="sexo">
				                            	<option value="">Selecione</option>
				                                <option value="1">Masculino</option>
				                                <option value="2">Feminino</option>
				                            </select>
			                            </div>
			                        </div>	
			                        
									<div class="form-group">
										<label class="control-label col-lg-2">Telefone:</label>
										<div class="col-lg-5">
											<input type="tel" name="telefone" class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="telefone" id="telefone" value="<?php echo set_value('telefone'); ?>">
										<?php echo form_error('telefone'); ?>
										</div>										
									</div>
									<div class="form-group">
										<label class="control-label col-lg-2">Email:</label>
										<div class="col-lg-5">
											<input type="email" name="email"  placeholder="seu@email.com" class="form-control" name="email" id="email" value="<?php echo set_value('email'); ?>">
										<?php echo form_error('email'); ?>
										</div>										 
									</div>
								<!-- Possível Botão de Form dinamico 
									<div class="text-center">
									<button type="button" class="btn btn-primary">Próximo<i class="icon-arrow-right14 position-right"></i></button>
								</div>
								-->
								<legend class="text-bold">Dados Residênciais:</legend>
									<div class="form-group">
										<label class="control-label col-lg-2">CEP:</label>
										<div class="col-lg-5">
											<input type="text" class="form-control" name="cep" id="cep" data-mask="99999-999" data-mask-selectonfocus="true" value="<?php echo set_value('cep'); ?>">
										<?php echo form_error('cep'); ?>
										</div>										
									</div>

									<div class="form-group">
										<label class="control-label col-lg-2">Endereço:</label>
										<div class="col-lg-5">
											<input type="text" class="form-control" name="endereco" id="endereco" value="<?php echo set_value('endereco'); ?>">
										<?php echo form_error('endereco'); ?>
										</div>										
									</div>
									<div class="form-group">
										<label class="control-label col-lg-2">Complemento:</label>
										<div class="col-lg-5">
											<input type="text" class="form-control" name="complemento" id="complemento" value="<?php echo set_value('complemento'); ?>">
										<?php echo form_error('complemento'); ?>
										</div>										
									</div>
									<div class="form-group">
			                        	<label class="control-label col-lg-2">UF:</label>
			                        	<div class="col-lg-5">
				                            <select  class="form-control" name="estado" id="estado" >
				                            	<option value="">Selecione</option>
				                                <?php foreach ($estados as $estado) { ?>
				  		                              <option value="<?php echo $estado->uf; ?>"><?php echo $estado->nome; ?></option>
				  		                        <?php } ?>
				                            </select>
				                            <?php echo form_error('estado'); ?>
			                            </div>			                            
			                        </div>			                        
									<div class="form-group">
										<label class="control-label col-lg-2">Cidade:</label>
										<div class="col-lg-5">
				                            <select class="form-control" name="cidade" id="cidade">
				                                <option value="">Selecione</option>
				                            </select>
			                            <?php echo form_error('cidade'); ?>	
			                            </div>				                            	                            
									</div>
									<div class="form-group">
										<label class="control-label col-lg-2">Bairro:</label>
										<div class="col-lg-5">
											<input type="text" class="form-control" name="bairro" id="bairro" value="<?php echo set_value('bairro'); ?>">
										<?php echo form_error('bairro'); ?>
										</div>										
									</div>
									<div class="form-group">
										<label class="control-label col-lg-2">Numero:</label>
										<div class="col-lg-5">
											<input type="text" class="form-control" name="numero" id="numero" value="<?php echo set_value('numero'); ?>">
										<?php echo form_error('numero'); ?>
										</div>										
									</div>									
									<legend class="text-bold">Dados Acesso:</legend>
									<div class="form-group">
										<label class="control-label col-lg-2">Login:</label>
										<div class="col-lg-5">
											<input type="email" class="form-control" name="login" id="login" value="<?php echo set_value('login'); ?>" placeholder="E-mail para acesso">
										<?php echo form_error('login'); ?>
										</div>										
									</div>
									<div class="form-group">
										<label class="control-label col-lg-2">Senha:</label>
										<div class="col-lg-5">
											<input type="password" class="form-control" name="senha" id="senha" value="<?php echo set_value('senha'); ?>">
										<?php echo form_error('senha'); ?>
										</div>										
									</div>

									<div class="form-group">
			                        	<label class="control-label col-lg-2">Perfil:</label>
			                        	<div class="col-lg-5">
				                            <select class="form-control" name="permissao" id="permissao">
				                           		<option value="">Selecione</option>
				                                <?php foreach ($permissoes as $valor) { ?>
				  		                              <option value="<?php echo $valor->permissao_id; ?>"><?php echo $valor->permissao_nome; ?></option>
				  		                        <?php } ?>
				                               
				                            </select>
			                            </div>
			                        </div>																		
									<div class="form-group">
			                        	<label class="control-label col-lg-2">Situação:</label>
			                        	<div class="col-lg-5">
				                            <select class="form-control" name="situacao" id="situacao">
				                                <option value="1">Ativo</option>
				                                <option value="0">Inativo</option>
				                            </select>
			                            </div>
			                        </div>		

			                        <div class="form-group">
										<label class="col-lg-2 control-label">Imagem:</label>
										<div class="col-lg-5">
											<div class="uploader bg-teal">
												<input class="file-styled" type="file" name="arquivo">
												<span class="filename" style="-moz-user-select: none;">Nenhum arquivo selecionado</span>
												<span class="action" style="-moz-user-select: none;">
												<i class="icon-cloud-upload2"></i>
												</span>
											</div>
										</div>
									</div>
			                      		
								</fieldset>
								<div class="text-right">
									<button type="submit" class="btn bg-teal">Cadastrar <i class="icon-arrow-right14 position-right"></i></button>
								</div>
							</form>
						</div>
					</div>
