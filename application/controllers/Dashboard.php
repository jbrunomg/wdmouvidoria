<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Dashboard_model');
	}

	public function index()
	{

		$noticias = $this->carregarnoticias();		
		
		$resultadoDen  = $this->Dashboard_model->graficoDenuncia();
		$resultadoEmpr = $this->Dashboard_model->graficoEmpresa();
		$resultadoStatus = $this->Dashboard_model->graficoStatus();
		

		$data['noticias']   = $noticias;
		
		$data['dadosdenuncia'] = $resultadoDen;
		$data['dadosempresa']  = $resultadoEmpr;
		$data['dadosstatus']   = $resultadoStatus;

		// echo "<pre>";
		// var_dump($data['dadosstatus']);die();
		

		
		$data['meio'] = 'dashboard/listar';
		$this->load->view('tema/tema',$data);
	 	

	}

	public function notificacoes()
	{	
		$dados['resultadoApi']  = $this->Dashboard_model->apiDenuncia();

		$resposta['api'] = $dados['resultadoApi'][0]->total;

		// $dados['resultadoTce']  = $this->Dashboard_model->tcePendente();
		// $dados['resultadoTa'] = $this->Dashboard_model->taPendente();


		// $resposta['ta'] = $dados['resultadoTa'][0]->total;
		// $resposta['tce'] = $dados['resultadoTce'][0]->total;

		echo json_encode($resposta);
	}


	public function carregarnoticias()
	{


		//$xml = simplexml_load_file("http://g1.globo.com/dynamo/pernambuco/educacao/rss2.xml"); // Educação
		$xml = simplexml_load_file("http://g1.globo.com/dynamo/economia/rss2.xml");

		$quant = 4;
		$noticias = array();

		for($i=0;$i<$quant;$i++) 
		{
			$hora                       = explode(" ",$xml->channel->item[$i]->pubDate);
		
			$noticias[$i]['titulo']     = $xml->channel->item[$i]->title;
			$noticias[$i]['link']       = $xml->channel->item[$i]->link;
			$noticias[$i]['dia']       = $hora[1];
			$noticias[$i]['mes']       = $hora[2];			
			$fimimg                    = strpos($xml->channel->item[$i]->description, "<br />");			
			$semimg                    = substr($xml->channel->item[$i]->description, 6 + $fimimg);
			//var_dump($novo);die();
			$noticias[$i]['descricao']  = $semimg;
				
		}

	 
	    return $noticias;


	}



}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */