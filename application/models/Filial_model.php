<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//class Filial_model extends CI_Model {
class Filial_model extends MY_Model {


	public $tabela  = 'filial';
	public $chave   = 'filial_id';
	public $visivel = 'filial_visivel';


	public function pegarPorCnpj($cnpj)
	{
		$this->db->select('filial_nomeRazao');		
		$this->db->where('filial_cpfCnpj',$cnpj);		
		return $this->db->get($this->tabela)->result();
	}	

	public function listarPorId($id,$campo)
	{
		$this->db->select('*');		
		$this->db->join('empresa','empresa_id = id_empresa');
		$this->db->where($this->visivel,'1');
		if ($id) {
		$this->db->where($campo,$id);	
		}
		
		return $this->db->get($this->tabela)->result();
	}	


}

/* End of file Filial_model.php */
/* Location: ./application/models/Filial_model.php */