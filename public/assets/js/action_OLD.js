

$.ajax({
    url: base_url+'dashboard/notificacoes/',
    dataType: 'json',
})
.done(function(response) {

    var s1 = parseInt(response.ta);
    var s2 = parseInt(response.tce);
    
    var s3 = s1 + s2;

    $("#somaNotificacao").text(s3);
    $("#tceNotificacao").text(response.tce);
    $("#taNotificacao").text(response.ta);
});


$('#estado').change(function(event) {

  var estado = $(this).val();  

  $.ajax({
    url: base_url+'usuario/cidadesPorId',
    type: 'POST',
    dataType: 'html',
    data: {estado: estado },
  })
  .done(function(resultado) {
    $('#cidade').html(resultado);
  });
});

$('#id_empresa').change(function(event) {

  var id_empresa = $(this).val();

  $.ajax({
    url: base_url+'denuncia/departamentoEmpresa',
    type: 'POST',
    dataType: 'html',
    data: {id_empresa: id_empresa },
  })
  .done(function(resultado) {
    $('#departemento').html(resultado);
  });
});


$('.classData').daterangepicker({
	singleDatePicker: true,
	locale: {
      format: 'DD-MM-YYYY'
    }
});

$('#empresa').select2({ 

        placeholder: 'Empresa',
        ajax: {
            url: base_url+'empresa/selecionarEmpresas/',
            dataType: 'json',
            data: function (term, page) {
                return {
                    term: term, //search term
                    page_limit: 10 // page size
                };
            },
            results: function (data, page) {
                return { results: data.results };
            }

        },
        tags:true,
        multiple: true,
        tokenSeparators: [',']

  });


$('#cep').change(function(event) {
      
      var cep = $(this).val();

      $.ajax({
        url: 'http://viacep.com.br/ws/'+cep+'/json/',
        type: 'get',
        dataType: 'json',
      })
      .done(function(result) {
          if(result.erro != true){
              $('#endereco').val(result.logradouro);
              $('#bairro').val(result.bairro);             
              $('#estado').val(result.uf);
              $('#cidade').html('<option value="'+result.localidade+'">'+result.localidade+'</option>');
          }
      });
      
});

$('#cpfCnpj').blur(function(event) {
  
       var documento = $(this).val();

       if(documento.length == 18){

        $.ajax({
           url: base_url+'empresa/wscnpj/', //'<?php echo base_url();?>empresa/wscnpj/',
           type: 'POST',
           dataType: 'json',
           data: {documento: documento},
           beforeSend: function() {
                 $('#carregando').css('display', 'block');
           }
            
       })
       .always(function(result) {

          $('#carregando').css('display', 'none');

          if(result.status == 'OK'){

            var cep = result.cep.replace('.','');
            var telefone = result.telefone.replace(' ','');

            // console.log(result);
             $('#nomeRazao').val(result.nome);
             $('#telefone').val(telefone);
             $('#email').val(result.email);
             $('#cep').val(cep);
             $('#endereco').val(result.logradouro);
             $('#numero').val(result.numero);
             $('#bairro').val(result.bairro);
             $('#estado').val(result.uf);
             //$('#cidade').val(result.municipio);
             $('#cidade').html('<option value="'+result.municipio+'">'+result.municipio+'</option>');


          }else{
            //alert(result.message);
          }
        
       });

     }
     
  });
