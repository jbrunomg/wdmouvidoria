<?php
$tipos_de_arquivo_permitidos = array(
  '.txt',
  '.jpg',
  '.jpeg',
  '.gif',
  '.png',
  '.pdf',
  '.xls',
  '.xlsx',
  '.xlsm',
  '.ods',
  '.zip',
  '.rar',
  '.mp4',
  '.mp3'
);
$valores_aceitos = implode(', ', $tipos_de_arquivo_permitidos);
?>
<!-- Estilização local -->
<link href="<?= base_url(); ?>public/assets/css/fileButton.css" rel="stylesheet" type="text/css">
<!-- /estilização local -->

<!-- Form horizontal -->
<div class="panel panel-flat">

  <?php if ($this->session->flashdata('error') != null) { ?>
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert">&times;</button>
      <?php echo $this->session->flashdata('error'); ?>
    </div></br>
  <?php } ?>

  <?php if ($this->session->flashdata('success') != null) { ?>
    <div class="alert alert-success">
      <button type="button" class="close" data-dismiss="alert">&times;</button>
      <?php echo $this->session->flashdata('success'); ?>
    </div></br>
  <?php } ?>


  <form class="form-horizontal" action="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/editarExe" method="post" enctype="multipart/form-data">

    <!-- <input  type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" /> -->

    <div class="panel-heading">
      <h5 class="panel-title">Cadastro</h5>
      <div class="heading-elements">
        <ul class="icons-list">
          <li><a data-action="collapse"></a></li>
          <!--  <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li> -->
        </ul>
      </div>
    </div>

    <div class="panel-body">
      <div class="row">
        <div class="col-md-6">
          <fieldset>
            <legend class="text-semibold"><i class="icon-reading position-left"></i>Detalhar Denuncia</legend>

            <input type="hidden" name="denuncia_id" value="<?php echo $dados[0]->denuncia_id; ?>" />
            <div class="form-group">
              <label class="col-lg-3 control-label">Empresa:</label>
              <div class="col-lg-9">
                <select class="form-control" name="id_empresa" id="id_empresa">
                  <option value="">Selecione</option>
                  <?php foreach ($empresas as $e) { ?>
                    <?php $selected = ($e->empresa_id == $dados[0]->id_empresa) ? 'SELECTED' : ''; ?>
                    <option value="<?php echo $e->empresa_id; ?>" <?php echo $selected; ?>><?php echo $e->empresa_nomeFantasia; ?></option>
                  <?php } ?>
                </select>
                <?php echo form_error('id_empresa'); ?>
              </div>
            </div>

            <div class="form-group">
              <label class="col-lg-3 control-label">Unidade/ Localidade:</label>
              <div class="col-lg-9">
                <input  type="text" class="form-control" placeholder="Und/Loc" name="denuncia_und_localidade" id="denuncia_und_localidade"  value="<?php echo $dados[0]->denuncia_und_localidade; ?>">                
              </div>
            </div>

            <div class="form-group">
              <label class="col-lg-3 control-label">Data do fato:</label>
              <div class="col-lg-9">
                <input type="date" class="form-control" placeholder="Data do Fato" name="denuncia_data_fato" value="<?php echo $dados[0]->denuncia_data_fato; ?>">
              </div>
            </div>

            <div class="form-group">
              <label class="col-lg-3 control-label">Usuário:</label>
              <div class="col-lg-9">
                <input disabled type="text" class="form-control" placeholder="Usuário" value="<?php echo $this->session->userdata('usuario_nome'); ?>">
                <input type="hidden" class="form-control" name="id_usuario" value="<?php echo $this->session->userdata('usuario_id'); ?>">
              </div>
            </div>

            <div class="form-group">
              <label class="col-lg-3 control-label">Departamento:</label>
              <div class="col-lg-9">
                <!-- <select name="id_departamento" id="departemento" class="js-example-basic-single js-states form-control" required></select> -->
                <select class="form-control" name="id_tipodenuncia" id="departemento">
                  <option value="">Selecione</option>
                  <?php foreach ($departamento as $d) { ?>
                    <?php $selected = ($d->departamento_id == $dados[0]->id_departamento) ? 'SELECTED' : ''; ?>
                    <option value="<?php echo $d->departamento_id; ?>" <?php echo $selected; ?>><?php echo $d->nome; ?></option>
                  <?php } ?>
                </select>
                <?php echo form_error('id_tipodenuncia'); ?>
              </div>
            </div>

            <div class="form-group">
              <label class="col-lg-3 control-label">Tipo de Denuncia:</label>
              <div class="col-lg-9">
                <select class="form-control" name="id_tipodenuncia" id="id_tipodenuncia">
                  <option value="">Selecione</option>
                  <?php foreach ($tipodenuncia as $t) { ?>
                    <?php $selected = ($t->tipodenuncia_id == $dados[0]->id_tipodenuncia) ? 'SELECTED' : ''; ?>
                    <option value="<?php echo $t->tipodenuncia_id; ?>" <?php echo $selected; ?>><?php echo $t->nome; ?></option>
                  <?php } ?>
                </select>
                <?php echo form_error('id_tipodenuncia'); ?>
              </div>
            </div>

            <div class="form-group">
              <label class="col-lg-3 control-label">Status:</label>
              <div class="col-lg-9">
                <select class="form-control" name="denuncia_status" id="denuncia_status">
                  <option <?php if ($dados[0]->denuncia_status == 'aberto') {
                            echo 'selected';
                          } ?> value="aberto">Em aberto</option>
                  <option <?php if ($dados[0]->denuncia_status == 'analise') {
                            echo 'selected';
                          } ?> value="analise">Em análise</option>
                  <option <?php if ($dados[0]->denuncia_status == 'encerrado') {
                            echo 'selected';
                          } ?> value="encerrado">Encerrado</option>
                </select>

              </div>
            </div>

            <div class="form-group">
              <label class="col-lg-3 control-label">Relato:</label>
              <div class="col-lg-9">
                <textarea class="form-control rounded-0" name="denuncia_relato" id="denuncia_relato" rows="3"><?php echo $dados[0]->denuncia_relato; ?></textarea>
              </div>
            </div>

            <div class="form-group">
              <label class="col-lg-3 control-label">Denunciante:</label>
              <div class="col-lg-9">
                <input type="text" class="form-control" placeholder="Responsável" data-mask-selectonfocus="true" name="denuncia_nome" id="denuncia_nome" value="<?php echo $dados[0]->denuncia_nome; ?>">
                <?php echo form_error('denuncia_nome'); ?>
              </div>
            </div>

            <div class="form-group">
              <label class="col-lg-3 control-label">Telefone:</label>
              <div class="col-lg-9">
                <input type="tel" class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="denuncia_contato" id="contato" value="<?php echo $dados[0]->denuncia_contato; ?>">
                <?php echo form_error('denuncia_contato'); ?>
              </div>
            </div>

            <div class="form-group">
              <label class="col-lg-3 control-label">Email:</label>
              <div class="col-lg-9">
                <input type="text" placeholder="seu@email.com" class="form-control" name="denuncia_email" id="email" value="<?php echo $dados[0]->denuncia_email; ?>">
                <?php echo form_error('denuncia_email'); ?>
              </div>
            </div>


          </fieldset>
        </div>

        <div class="col-md-6">
          <fieldset>
            <legend class="text-semibold"><i class="icon-drawer-in position-left"></i> Anexo(s)</legend>
            <!-- <form id="formAnexos" enctype="multipart/form-data" action="javascript:;" accept-charset="utf-8" method="post">  -->
            <!-- <form class="form-horizontal" action="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/anexar" method="post" enctype="multipart/form-data">  -->
            <div class="form-group" style="padding: 0%; margin-left: 0; margin-right: 0;">
              <p class="col-lg-9" style="color: #999999;">&lpar;Tamanho máximo: 16 MB&rpar;</p>
              <div class="col-lg-10 file-input">
                <input class="form-control" name="denuncia_anexo[]" id="denuncia_anexo" type="file" accept="<?= $valores_aceitos; ?>" multiple>
                <button id="apagar_anexo" type="button" class="btn btn-ex" onclick="apagarAnexo()">Excluir</button>
              </div>
              <!-- <div class="col-md-10">
                <label>Anexar arquivo</label>
                <input type="file" class="span12" name="denuncia_anexo[]" multiple/>
              </div> -->
              <div class="col-md-2">
                <!-- <button id="formAnexos" class="btn btn-primary">Anexar <i class="icon-arrow-right14 position-right"></i></button> -->
                <button formaction="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/anexar" class="btn btn-primary">Anexar <i class="icon-arrow-right14 position-right"></i></button>
              </div>
            </div>
  </form>
  </br></br>

  <!-- Double thead border -->
  <div class="panel panel-flat">
    <div class="panel-heading">
      <h5 class="panel-title">Lista Anexos</h5>
      <div class="heading-elements">
        <ul class="icons-list">
          <li><a data-action="collapse"></a></li>
          <!--  <li><a data-action="reload"></a></li>
                              <li><a data-action="close"></a></li> -->
        </ul>
      </div>
    </div>

    <!--  <div class="panel-body">
                          Example of a <code>double</code> border inside table head. This border has <code>3px</code> width, <code>double</code> style and inherits all styling options from the default border style. Visually it displays table <code>head</code> and <code>body</code> as 2 separated table areas. To use this border add <code>.border-double</code> to the table head row.
                        </div> -->

    <div class="table-responsive">
      <table class="table">
        <thead>
          <tr class="border-double">
            <th>#</th>
            <th>Nome</th>
            <th>Tamanho</th>
            <th>Tipo</th>
            <th>Opções</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($anexos as $a) { ?>

            <tr>
              <?php if ($a->anexo_tipo == '.mp3') { ?>
                <td>
                  <audio src="<?php echo $a->anexo_url . $a->anexo_anexo; ?>" preload="auto" controls>
                    <p>Seu nevegador não suporta o elemento audio.</p>
                  </audio>
                </td>
              <?php } elseif ($a->anexo_tipo == '.mp4') { ?>
                <td>
                  <video src="<?php echo $a->anexo_url . $a->anexo_anexo; ?>" controls width="280" height="200">
                    Seu navegador não suporta o elemento <code>video</code>.
                  </video>
                </td>
              <?php } else { ?>
                <td>
                  <div class="media-left media-middle">
                    <a href="<?php echo $a->anexo_url . 'thumbs/' . $a->anexo_thumb; ?>" data-popup="lightbox">
                      <img src="<?php echo $a->anexo_url . 'thumbs/' . $a->anexo_thumb; ?>" alt="" class="img-rounded img-preview">
                    </a>
                  </div>
                </td>
              <?php } ?>

              <td><?php echo $a->anexo_anexo; ?></td>
              <td><?php echo $a->anexo_tamanho; ?></td>
              <td><?php echo $a->anexo_tipo; ?></td>

              <td>
                <ul class="icons-list">
                  <?php if (checarPermissao('vDenuncia')) { ?>
                    <li class="text-teal-600"><a target="_blank" href="<?= $a->anexo_url ?><?= $a->anexo_anexo ?>" data-popup="tooltip" title="Visualizar"><i class="icon-search4"></i></a></li>
                  <?php } ?>
                  <?php if (checarPermissao('vDenuncia')) { ?>
                    <li class="text-teal-600"><a href="<?php echo base_url() ?><?php echo $this->uri->segment(1); ?>/download/<?php echo $a->anexo_id; ?>" data-popup="tooltip" title="download"><i class="icon-file-download"></i></a></li>
                  <?php } ?>
                  <?php if (checarPermissao('dDenuncia')) { ?>
                    <li class="text-danger-600"><a href="#" data-popup="tooltip" title="Excluir" class="sweet_loader_id" url="<?php echo $this->uri->segment(1); ?>/excluirAnexo" registro="<?php echo $a->anexo_id; ?>"><i class="icon-trash"></i></a></li>
                  <?php } ?>
                </ul>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>

  </div>
  <!-- /double thead border -->
  </fieldset>
</div>
</div>

<div class="text-right">
  <button type="submit" class="btn bg-teal">Editar <i class="icon-arrow-right14 position-right"></i></button>
</div>
</div>
</form>
</div>

<script>
  const inputAnexo = document.getElementById('denuncia_anexo');
  const apagarAnexoBtn = document.getElementById('apagar_anexo');
  const extensoesPermitidas = [
    '.jpg', '.jpeg', '.gif', '.png', '.pdf', '.xls', '.xlsx', '.xlsm', '.ods', '.zip', '.rar', '.mp4', '.mp3', '.txt'
  ];

  inputAnexo.addEventListener('change', () => {
    let anexosValidos = true;
    for (const anexo of inputAnexo.files) {
      if (anexo.name.length > 53) {
        swal({
          title: "Oops...",
          text: "O nome do arquivo é muito longo. O tamanho máximo é de 53 caracteres. Nome do arquivo: " + anexo.name,
          confirmButtonColor: "#EF5350",
          type: "error",
        });
        anexosValidos = false;
      }
      if (anexo.size > 16 * 1024 * 1024) {
        swal({
          title: "Oops...",
          text: "Arquivo muito grande: " + anexo.name,
          confirmButtonColor: "#EF5350",
          type: "error",
        });
        anexosValidos = false;
      }

      const extensao = anexo.name.slice(((anexo.name.lastIndexOf(".") - 1) >>> 0) + 2);
      if (!extensoesPermitidas.includes('.' + extensao.toLowerCase())) {
        swal({
          title: "Oops...",
          text: "Tipo de arquivo não permitido. Tipos permitidos: " + extensoesPermitidas.join(', '),
          confirmButtonColor: "#EF5350",
          type: "error",
        });
        anexosValidos = false;
      }
    }
    if (anexosValidos) {
      apagarAnexoBtn.style.display = 'block';
    } else {
      inputAnexo.value = null;
    }
  });

  function apagarAnexo() {
    inputAnexo.value = null;
    apagarAnexoBtn.style.display = 'none';
  }
</script>