<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Filial extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Filial_model');		
	}

	public function index($id=null)
	{
		$campo = 'id_empresa';
		$resultado = $this->Filial_model->listarPorId($id,$campo);
		$dadosView['dados'] = $resultado;		
		$dadosView['meio']  = 'filial/listar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionar()
	{
		$this->load->model('Empresa_model');

		$id = $this->uri->segment(3);

		$dadosView['dados']      = $this->Empresa_model->pegarPorId($id);

		$dadosView['estados']    = $this->Filial_model->todosEstados();		
		$dadosView['permissoes'] = $this->Filial_model->todasPermissoes();
		$dadosView['meio']       = 'filial/adicionar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionarExe()
	{	
		$Cnpj =  $this->input->post('filial_cpfCnpj'); 
		$id   =  $this->input->post('id_empresa');		

		$resultadoCnpj = $this->Filial_model->pegarPorCnpj($Cnpj);

		if ($resultadoCnpj) {	

			$this->session->set_flashdata('erro', "O Filial: ".'\n' .$resultadoCnpj[0]->filial_nomeRazao.'\n'. " já está cadastrado em nosso sistema!");

		} else {

			$dados1 = $this->input->post(); 	

			$dados2 = array( 		  	  
			    'filial_data_cadastro' => date('Y-m-d') // Data do 1º dia de cadastro				
			);
			
			$dados  = array_merge($dados1,$dados2);

			$resultado = $this->Filial_model->inserir($dados);

			if ($resultado) {			
				$this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');
			} else {
				$this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
			}			
		}	
			
		redirect('/filial/index/'.$id, 'refresh');
	}


	public function editar()
	{
		$id = $this->uri->segment(3);
		$campo = 'filial_id';

		$dadosView['dados']      = $this->Filial_model->listarPorId($id,$campo);
		$dadosView['cidades']    = $this->Filial_model->todasCidadesIdEstado($dadosView['dados'][0]->filial_estado);
		$dadosView['estados']    = $this->Filial_model->todosEstados();		
		$dadosView['permissoes'] = $this->Filial_model->todasPermissoes();
		$dadosView['meio']       = 'filial/editar';


		$this->load->view('tema/tema',$dadosView);
	}

	public function editarExe()
	{	

		$id = $this->input->post('filial_id');
		$idEmpresa =  $this->input->post('id_empresa');

		$dados = $this->input->post();

		$resultado = $this->Filial_model->editar($id,$dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro alterado com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para alterado o registro!');
		}
		
		redirect('/filial/index/'.$idEmpresa, 'refresh');
	}

	public function excluir()
	{
		$id = $this->input->post('id');

		$dados = array(
						'filial_visivel' => 0
						
					);

		$resultado = $this->Filial_model->excluir($id,$dados);

		if ($resultado) {			
			echo json_encode(array('status' => true));
		}else{
			echo json_encode(array('status' => false));
		}
	}
	
	public function visualizar()
	{
		$id = $this->uri->segment(3);
		$campo = 'filial_id';

		$dadosView['dados']      = $this->Filial_model->listarPorId($id,$campo);
		$dadosView['cidades']    = $this->Filial_model->todasCidadesIdEstado($dadosView['dados'][0]->filial_estado);
		$dadosView['estados']    = $this->Filial_model->todosEstados();		
		$dadosView['permissoes'] = $this->Filial_model->todasPermissoes();
		$dadosView['meio']       = 'filial/visualizar';

		$this->load->view('tema/tema',$dadosView);

	}

	public function cidadesPorId()
	{
		$uf = $this->input->post('filial_estado'); 	

        $cidades = $this->Filial_model->todasCidadesIdEstado($uf);
        
        echo  "<option value=''>Selecionar uma cidade</option>";
        foreach ($cidades as $cidade) {
           echo "<option value='".$cidade->id."'>".$cidade->nome."</option>";
        }
	}

	public function selecionarCidade(){
		
		$estado  = $this->input->post('filial_estado');
		$cidades = $this->Filial_model->selecionarCidades($estado);

		echo  "<option value=''>Selecionar uma cidade</option>";
		foreach ($cidades as $cidade) {
			echo "<option value='".$cidade->id_cidade."'>".$cidade->nome."</option>";
		}

	}



	public function wscnpj()
    {
        $caracteres = array(".","/","-");
        $cpfCnpj  = $this->input->post('documento');        
        $cpfCnpj = str_replace($caracteres, "", $cpfCnpj);       
        $json = file_get_contents('https://receitaws.com.br/v1/cnpj/'.$cpfCnpj);
        
        echo $json;
    }

    public function wscep()
    {
        $cep =  explode("-",$this->input->post('cep'));
        $cep = $cep[0].$cep[1];
        $xml = file_get_contents('http://webservices.profissionaisdaweb.com.br/cep/'.$cep.'.xml');
        $xml = simplexml_load_string($xml);
        echo json_encode($xml);
    }


	public function Mask($mask,$str){

	    $str = str_replace(" ","",$str);

	    for($i=0;$i<strlen($str);$i++){
	        $mask[strpos($mask,"#")] = $str[$i];
	    }

	    return $mask;

	}

	
}

/* End of file Filial.php */
/* Location: ./application/controllers/Filial.php */