<!-- Form horizontal -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Edição de Sub Departamento</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<!-- <li><a data-action="close"></a></li> -->
        	</ul>
    	</div>
	</div>

	<div class="panel-body">
		<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/editarExe" method="post" enctype="multipart/form-data">
			<fieldset class="content-group">
				<legend class="text-bold">Dados Sub Departamento:</legend>

				<!-- <input  type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" /> -->

				<input  type="hidden" name="sub_departamento_id" value="<?php echo $dados[0]->sub_departamento_id; ?>" />

				<div class="form-group">
					<label class="control-label col-lg-2">Nome da Sub Departamento:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" placeholder="Nome Departamento" name="nome" id="nome" value="<?php echo $dados[0]->nome; ?>">
					<?php echo form_error('nome'); ?>
					</div>										
				</div>

				<div class="form-group">
                	<label class="control-label col-lg-2">Departamento:</label>
                	<div class="col-lg-5">
                        <select class="form-control" name="id_departamento" id="id_departamento">
                       		<option  value="">Selecione</option>                       		
                            <?php foreach ($departamento as $valor) { ?>
                            	  <?php $selected = ($valor->departamento_id == $dados[0]->id_departamento)?'SELECTED': ''; ?>
		                              <option  value="<?php echo $valor->departamento_id; ?>" <?php  echo $selected; ?>><?php echo $valor->nome; ?></option>
		                        <?php } ?>
                           
                        </select>
                    </div>
                </div>	

                </fieldset>  
				<div class="text-right">
					<button type="submit" class="btn bg-teal">Editar <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>
