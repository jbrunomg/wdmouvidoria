<!-- Form horizontal -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Visualizar Departamento</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<!-- <li><a data-action="close"></a></li> -->
        	</ul>
    	</div>
	</div>

	<div class="panel-body">
		<form class="form-horizontal" action="<?php echo base_url();?>curso/editarExe" method="post" enctype="multipart/form-data">
			<fieldset class="content-group">
				<legend class="text-bold">Dados Departamento:</legend>

				<!-- <input disabled type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" /> -->

				<input disabled type="hidden" name="departamento_id" value="<?php echo $dados[0]->departamento_id; ?>" />

				<div class="form-group">
					<label class="control-label col-lg-2">Nome da Departamento:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" placeholder="Nome Departamento" name="nome" id="nome" value="<?php echo $dados[0]->nome; ?>">
					<?php echo form_error('nome'); ?>
					</div>										
				</div>									
		
            </fieldset>  
		</form>
	</div>
</div>
