<?php
defined('BASEPATH') or exit('No direct script access allowed');
ini_set('memory_limit', '3048M'); // para geração arquivo remessa
ini_set('max_execution_time', 0); // para geração arquivo remessa


class Denuncia extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Denuncia_model');
        $this->load->library('Util');
    }

    public function index()
    {
        $resultado = $this->Denuncia_model->listarGo();

        $dadosView['dados'] = $resultado;
        $dadosView['meio']  = 'denuncia/listar';
        $this->load->view('tema/tema', $dadosView);
    }

    public function adicionar()
    {
        $this->load->model('Empresa_model');
        $this->load->model('TipoDenuncia_model');

        $dadosView['empresas']   = $this->Empresa_model->listar();
        $dadosView['tipodenuncia']  = $this->TipoDenuncia_model->listar();
        $dadosView['permissoes'] = $this->Denuncia_model->todasPermissoes();
        //$dadosView['meio']       = 'denuncia/table_elements';
        $dadosView['meio']       = 'denuncia/adicionar';
        $this->load->view('tema/tema', $dadosView);
    }

    public function adicionarExe()
    {

        $dados1 = $this->input->post();

        $dados2 = array(
            'denuncia_data_cadastro' => date('Y-m-d') // Data do 1º dia de cadastro				
        );

        $dados  = array_merge($dados1, $dados2);

        if (is_numeric($id = $this->Denuncia_model->add($dados, true))) {
            $this->session->set_flashdata('success', 'Denuncia adicionada com sucesso, você pode adicionar os "Anexos"!');
            redirect('Denuncia/editar/' . $id);
        } else {

            $this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
        }

        // $resultado = $this->Denuncia_model->inserir($dados);		

        // if ($resultado) {			
        // 	$this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');
        // } else {
        // 	$this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
        // }	

        redirect('Denuncia', 'refresh');
    }


    public function editar($idAnexo = false)
    {
        if ($idAnexo) {
            $id = $idAnexo;
            //die('op_1='.$id);
        } else {
            $id = $this->uri->segment(3);
            //die('op_2='.$id);
        }

        $this->load->model('Empresa_model');
        $this->load->model('TipoDenuncia_model');
        $this->load->model('Departamento_model');

        $dadosView['dados']         = $this->Denuncia_model->pegarPorId2($id);
        $dadosView['anexos']        = $this->Denuncia_model->getAnexosLista($id);

        // var_dump($dadosView['anexos']);die();

        $dadosView['empresas']      = $this->Empresa_model->listar();
        $dadosView['tipodenuncia']  = $this->TipoDenuncia_model->listar();
        $dadosView['departamento']  = $this->Departamento_model->listar();
        $dadosView['permissoes'] = $this->Denuncia_model->todasPermissoes();
        $dadosView['meio']       = 'denuncia/editar';



        $this->load->view('tema/tema', $dadosView);
    }

    public function editarExe()
    {

        $id = $this->input->post('denuncia_id');



        $dados = $this->input->post();

        $resultado = $this->Denuncia_model->editar($id, $dados);

        if ($resultado) {
            $this->session->set_flashdata('sucesso', 'Registro alterado com sucesso!');
        } else {
            $this->session->set_flashdata('erro', 'Tivemos problema para alterado o registro!');
        }

        redirect('Denuncia', 'refresh');
    }

    public function excluir()
    {
        $id = $this->input->post('id');

        $dados = array(
            'filial_visivel' => 0
        );

        $resultado = $this->Denuncia_model->excluir($id, $dados);

        if ($resultado) {
            echo json_encode(array('status' => true));
        } else {
            echo json_encode(array('status' => false));
        }
    }

    public function visualizar()
    {
        $id = $this->uri->segment(3);

        $this->load->model('Empresa_model');
        $this->load->model('TipoDenuncia_model');
        $this->load->model('Departamento_model');

        $dadosView['dados']         = $this->Denuncia_model->pegarPorId2($id);
        $dadosView['anexos']        = $this->Denuncia_model->getAnexosLista($id);

        $dadosView['empresas']      = $this->Empresa_model->listar();
        $dadosView['tipodenuncia']  = $this->TipoDenuncia_model->listar();
        $dadosView['departamento']  = $this->Departamento_model->listar();
        $dadosView['permissoes'] = $this->Denuncia_model->todasPermissoes();
        $dadosView['meio']       = 'denuncia/visualizar';

        $this->load->view('tema/tema', $dadosView);
    }

    public function departamentoEmpresa()
    {
        $id = $this->input->post('id_empresa');

        $departamento = $this->Denuncia_model->todosDepartamentoEmpresa($id);

        echo  "<option value=''>Selecionar um departamento</option>";
        foreach ($departamento as $d) {
            echo "<option value='" . $d->departamento_id . "'>" . $d->nome . "</option>";
        }
    }


    public function anexar()
    {
        $this->load->model('Apidenuncia_model');
        $denuncia_id = $this->input->post('denuncia_id');

        $error = array();
        $success = array();

        //      Removendo caracteres especiais do nome do arquivo.       //
        foreach ($_FILES['denuncia_anexo']['name'] as $chave => $valor) {
            $novoNome = $this->util->removerCaracteresEspeciais($valor);
            $_FILES['denuncia_anexo']['name'][$chave] = $novoNome;
        }
        //      /removendo caracteres especiais do nome do arquivo.       //

        //      Carregando bibliotecas e configurações dos anexos.      //
        $this->load->library('upload');
        $this->load->library('image_lib');

        $upload_conf = array(
            'upload_path'   => realpath('./public/assets/anexos'),
            'allowed_types' => 'txt|jpg|jpeg|gif|png|pdf|PDF|JPG|JPEG|GIF|PNG|xls|xlsx|xlsm|ods|zip|ZIP|rar|RAR|mp4|MP4|mp3|MP3', // formatos permitidos para anexos de os
            'max_size'      => 16 * 1024 * 1024
        );

        $this->upload->initialize($upload_conf);
        //      /carregando bibliotecas e configurações dos anexos.      //

        //      Alterando $_FILES para novas variáveis.     // 
        foreach ($_FILES['denuncia_anexo'] as $chave => $valor) {
            $i = 1;
            foreach ($valor as $v) {
                $field_name = "file_" . $i;
                $_FILES[$field_name][$chave] = $v;
                $i++;
            }
        }
        unset($_FILES['denuncia_anexo']);
        //      /alterando $_FILES para novas variáveis.    //

        //      Fazendo o uploado dos arquivos.     //
        foreach ($_FILES as $field_name => $file) {
            if (!$this->upload->do_upload($field_name)) {
                $error['upload'][] = $this->upload->display_errors();
            } else {
                $upload_data = $this->upload->data();
                $tamanho = $upload_data['file_size'];
                $tipo = $upload_data['file_ext'];

                if ($upload_data['is_image'] == 1) {

                    $resize_conf = array(
                        'source_image'  => $upload_data['full_path'],
                        'new_image'     => $upload_data['file_path'] . 'thumbs/thumb_' . $upload_data['file_name'],
                        'width'         => 200,
                        'height'        => 125
                    );

                    $this->image_lib->initialize($resize_conf);

                    if (!$this->image_lib->resize()) {
                        $error['resize'][] = $this->image_lib->display_errors();
                    } else {
                        $success[] = $upload_data;

                        $this->Apidenuncia_model->anexarApi($denuncia_id, $upload_data['file_name'], base_url() . 'public/assets/anexos/', 'thumb_' . $upload_data['file_name'], realpath('./public/assets/anexos/'), $tamanho, $tipo);
                    }
                } else {
                    $success[] = $upload_data;
                    $this->Apidenuncia_model->anexarApi($denuncia_id, $upload_data['file_name'], base_url() . 'public/assets/anexos/', '', realpath('./public/assets/anexos/'), $tamanho, $tipo);
                }
            }
        }

        // see what we get
        if (count($error) > 0) {
            // echo json_encode(array('result'=> false, 'mensagem' => 'Nenhum arquivo foi anexado.')); // Via Ajax
            $this->session->set_flashdata('error', 'Tivemos problema para inserir o registro!');
            redirect('Denuncia/editar/' . $this->input->post('denuncia_id'));
        } else {
            // echo json_encode(array('result'=> true, 'mensagem' => 'Arquivo(s) anexado(s) com sucesso .')); // Via Ajax
            $this->session->set_flashdata('success', 'Anexo adicionada com sucesso, você pode adicionar mais "Anexos"!');
            redirect('Denuncia/editar/' . $this->input->post('denuncia_id'));
        }
    }


    public function excluirAnexo()
    {
        $idDenuncia = $this->uri->segment(3);
        $id = $this->input->post('id');

        if ($id == null || !is_numeric($id)) {
            // echo json_encode(array('result'=> false, 'mensagem' => 'Erro ao tentar excluir anexo.'));
            //$this->session->set_flashdata('error', 'Erro ao tentar excluir anexo!');
            echo json_encode(array('status' => false));
        } else {

            $this->db->where('anexo_id', $id);
            $file = $this->db->get('anexos', 1)->row();

            unlink($file->anexo_path . '/' . $file->anexo_anexo);

            if ($file->anexo_thumb != null) {
                unlink($file->anexo_path . '/thumbs/' . $file->anexo_thumb);
            }

            if ($this->Denuncia_model->deleteAnexo('anexos', 'anexo_id', $id) == true) {

                //echo json_encode(array('result'=> true, 'mensagem' => 'Anexo excluído com sucesso.'));
                echo json_encode(array('status' => true));
                //$this->session->set_flashdata('success','Anexo excluído com sucesso!');
            } else {
                // echo json_encode(array('result'=> false, 'mensagem' => 'Erro ao tentar excluir anexo.'));
                echo json_encode(array('status' => false));
                //$this->session->set_flashdata('error', 'Erro ao tentar excluir anexo!');
            }
        }

        //redirect('Denuncia/editar/'.$idDenuncia);
    }

    public function download($id = null)
    {

        if ($id == null || !is_numeric($id)) {
            $this->session->set_flashdata('error', 'Erro! O arquivo não pode ser localizado.');
            redirect(base_url() . 'Denuncia/');
        }

        $file = $this->Denuncia_model->getAnexos($id);

        $this->load->library('zip');

        $path = $file->anexo_path . "\\" . $file->anexo_anexo;

        $this->zip->read_file($path);

        $this->zip->download('file' . date('d-m-Y-H.i.s') . '.zip');
    }
}

/* End of file Denuncia.php */
/* Location: ./application/controllers/Denuncia.php */