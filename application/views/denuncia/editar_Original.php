<!-- Form horizontal -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Edição de Empresa</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<!-- <li><a data-action="close"></a></li> -->
        	</ul>
    	</div>
	</div>

	<div class="panel-body">
		<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/editarExe" method="post" enctype="multipart/form-data">
			<fieldset class="content-group">
				<legend class="text-bold">Dados Empresa:</legend>

				<!-- <input  type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" /> -->

				<input  type="hidden" name="denuncia_id" value="<?php echo $dados[0]->denuncia_id; ?>" />

				<div class="form-group">
          
		<!--         <div class="col-md-4">
		          <div class="panel panel-body border-top-teal text-center">
		            <h6 class="no-margin text-semibold">Empresa</h6>
		              <br/> 
		              <select name="empresa[]" id="empresa" class="js-example-basic-single js-states form-control" required></select>           
		          </div>
		        </div> -->

		        <div class="col-md-4">
		          <div class="panel panel-body border-top-teal text-center">
		            <h6 class="no-margin text-semibold">Empresa</h6>
		              <br/> 
		                <select  class="form-control" name="id_empresa" id="id_empresa" >
                        	<option value="">Selecione</option>
                            <?php foreach ($empresas as $e) { ?>
                            	<?php $selected = ($e->empresa_id == $dados[0]->id_empresa)?'SELECTED': ''; ?>
		                              <option value="<?php echo $e->empresa_id; ?>" <?php echo $selected; ?>><?php echo $e->empresa_nomeFantasia; ?></option>
		                        <?php } ?>
                        </select>
                        <?php echo form_error('id_empresa'); ?>                     
		          </div>
		        </div>  

		        <div class="col-md-4">
		          <div class="panel panel-body border-top-teal text-center">
		            <h6 class="no-margin text-semibold">Data do Fato</h6>
		              <br/>               
		              <div class="col-lg-12">
		                <input type="date" class="form-control" placeholder="Data do Fato" name="denuncia_data_fato" value="<?php echo $dados[0]->denuncia_data_fato; ?>">
		              </div>                        
		          </div>
		        </div>

		        <div class="col-md-4">
		          <div class="panel panel-body border-top-teal text-center">
		            <h6 class="no-margin text-semibold">Usuário</h6>
		              <br/>               
		              <div class="col-lg-12">
		                <input disabled type="text" class="form-control" placeholder="Usuário" value="<?php echo $dados[0]->usuario_nome; ?>">
		                <input type="hidden" class="form-control"  name="id_usuario"  value="<?php echo $dados[0]->usuario_id; ?>">
		              </div>                        
		          </div>
		        </div>

		      </div>

				
		    <div class="form-group">
          
		        <div class="col-md-6">
		          <div class="panel panel-body border-top-teal text-center">
		            <h6 class="no-margin text-semibold">Departemanto</h6>
		              <br/> 
		              <!-- <select name="id_departamento" id="departemento" class="js-example-basic-single js-states form-control" required></select> -->
		                <select  class="form-control" name="id_tipodenuncia" id="departemento" >
                        	<option value="">Selecione</option>
                            <?php foreach ($departamento as $d) { ?>
                            	<?php $selected = ($d->departamento_id == $dados[0]->id_departamento)?'SELECTED': ''; ?>
		                              <option value="<?php echo $d->departamento_id; ?>" <?php echo $selected; ?>><?php echo $d->nome; ?></option>
		                        <?php } ?>
                        </select>
                        <?php echo form_error('id_tipodenuncia'); ?>             
		          </div>
		        </div>

		        <div class="col-md-6">
		          <div class="panel panel-body border-top-teal text-center">
		            <h6 class="no-margin text-semibold">Tipo de Denuncia</h6>
		              <br/> 
		                <select  class="form-control" name="id_tipodenuncia" id="id_tipodenuncia" >
                        	<option value="">Selecione</option>
                            <?php foreach ($tipodenuncia as $t) { ?>
                            	<?php $selected = ($t->tipodenuncia_id == $dados[0]->id_tipodenuncia)?'SELECTED': ''; ?>
		                              <option value="<?php echo $t->tipodenuncia_id; ?>" <?php echo $selected; ?>><?php echo $t->nome; ?></option>
		                        <?php } ?>
                        </select>
                        <?php echo form_error('id_tipodenuncia'); ?>  
		          </div>
		        </div>
		    
		    </div>

		    <div class="form-group">
          
		        <div class="col-md-12">
		          <div class="panel panel-body border-top-teal text-center">
		            <h6 class="no-margin text-semibold">Relato:</h6>           
		            <textarea class="form-control rounded-0" name="denuncia_relato" id="denuncia_relato" rows="3"><?php echo $dados[0]->denuncia_relato; ?></textarea>           
		          </div>
		        </div>

	      	</div>  

	      
	      	<div class="form-group">

	      		<div class="col-md-4">
		          <div class="panel panel-body border-top-teal text-center">
		            <h6 class="no-margin text-semibold">Responsável:</h6>           
		            <input  type="text" class="form-control" placeholder="Responsável"  data-mask-selectonfocus="true" name="denuncia_nome" id="denuncia_nome" value="<?php echo $dados[0]->denuncia_nome; ?>">          
		          </div>
		        </div>

		        <div class="col-md-4">
		          <div class="panel panel-body border-top-teal text-center">
		            <h6 class="no-margin text-semibold">Telefone:</h6>           
		            <input  type="tel" class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="denuncia_contato" id="contato" value="<?php echo $dados[0]->denuncia_contato; ?>">
		          </div>
		        </div>

		        <div class="col-md-4">
		          <div class="panel panel-body border-top-teal text-center">
		            <h6 class="no-margin text-semibold">Email:</h6>           
		            <input  type="text" placeholder="seu@email.com" class="form-control" name="denuncia_email" id="email" value="<?php echo $dados[0]->denuncia_email; ?>">
		          </div>
		        </div>

	     	</div>

			


                </fieldset>  
				<div class="text-right">
					<button type="submit" class="btn bg-teal">Editar <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>
