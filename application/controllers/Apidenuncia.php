<?php

class Apidenuncia extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Apidenuncia_model');
        $this->load->library('Util');
    }

    public function index()
    {
        $dadosView['meio']  = 'api/404';
        $this->load->view('tema/iframe', $dadosView);
    }

    public function carregarApi($id_empr = null)
    {
        if (empty($id_empr)) {
            $id = $this->uri->segment(3);
        } else {
            $id = $id_empr;
        }

        $dadosView['empresas']      = $this->Apidenuncia_model->pegarPorId($id, 'empresa_id', 'empresa');
        $dadosView['tipodenuncia']  = $this->Apidenuncia_model->listar('tipodenuncia');
        $dadosView['departamento']  = $this->Apidenuncia_model->todosDepartamentoEmpresa($id);
        $dadosView['meio']          = 'api/apidenuncia';
        $this->load->view('tema/iframe', $dadosView);
    }

    public function adicionarApi()
    {
        $id = $this->input->post('id_empresa');

        //      Capturando os dados e erros.    // 
        $error = array();
        $success = array();
        //      /capturando os dados e erros.    // 

        if (!$id) {
            $this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
            redirect('Apidenuncia', 'refresh');
        } else {
            $dados1 = $this->input->post();

            $dados2 = array(
                'denuncia_data_cadastro' => date('Y-m-d') // Data do 1º dia de cadastro             
            );

            $dados  = array_merge($dados1, $dados2);

            $denuncia_id = $this->Apidenuncia_model->addApi($dados, 'denuncia_api');

            if (isset($_FILES['denuncia_anexo']['name']) && !empty($_FILES['denuncia_anexo']['name'][0])) {
                //      Removendo caracteres especiais do nome do arquivo.       //
                foreach ($_FILES['denuncia_anexo']['name'] as $chave => $valor) {
                    $novoNome = $this->util->removerCaracteresEspeciais($valor);
                    $_FILES['denuncia_anexo']['name'][$chave] = $novoNome;
                }
                //      /removendo caracteres especiais do nome do arquivo.       //

                //      Carregando bibliotecas e configurações dos anexos.      //
                $this->load->library('upload');
                $this->load->library('image_lib');

                $upload_conf = array(
                    'upload_path'   => realpath('./public/assets/anexos'),
                    'allowed_types' => 'txt|jpg|jpeg|gif|png|pdf|PDF|JPG|JPEG|GIF|PNG|xls|xlsx|xlsm|ods|zip|ZIP|rar|RAR|mp4|MP4|mp3|MP3', // formatos permitidos para anexos de os
                    'max_size'      => 16 * 1024 * 1024
                );

                $this->upload->initialize($upload_conf);
                //      /carregando bibliotecas e configurações dos anexos.      //

                //      Alterando $_FILES para novas variáveis.     // 
                foreach ($_FILES['denuncia_anexo'] as $chave => $valor) {
                    $i = 1;
                    foreach ($valor as $v) {
                        $field_name = "file_" . $i;
                        $_FILES[$field_name][$chave] = $v;
                        $i++;
                    }
                }
                unset($_FILES['denuncia_anexo']);
                //      /alterando $_FILES para novas variáveis.    //

                //      Fazendo o uploado dos arquivos.     //
                foreach ($_FILES as $field_name => $file) {
                    if (!$this->upload->do_upload($field_name)) {
                        $error['upload'][] = $this->upload->display_errors();
                    } else {
                        $upload_data = $this->upload->data();
                        $tamanho = $upload_data['file_size'];
                        $tipo = $upload_data['file_ext'];

                        if ($upload_data['is_image'] == 1) {

                            $resize_conf = array(
                                'source_image'  => $upload_data['full_path'],
                                'new_image'     => $upload_data['file_path'] . 'thumbs/thumb_' . $upload_data['file_name'],
                                'width'         => 200,
                                'height'        => 125
                            );

                            $this->image_lib->initialize($resize_conf);

                            if (!$this->image_lib->resize()) {
                                $error['resize'][] = $this->image_lib->display_errors();
                            } else {
                                $success[] = $upload_data;

                                $this->Apidenuncia_model->anexarApi($denuncia_id, $upload_data['file_name'], base_url() . 'public/assets/anexos/', 'thumb_' . $upload_data['file_name'], realpath('./public/assets/anexos/'), $tamanho, $tipo);
                            }
                        } else {
                            $success[] = $upload_data;
                            $this->Apidenuncia_model->anexarApi($denuncia_id, $upload_data['file_name'], base_url() . 'public/assets/anexos/', '', realpath('./public/assets/anexos/'), $tamanho, $tipo);
                        }
                    }
                }
            }
            //      /fazendo o uploado dos arquivos.     //

            if ($denuncia_id && count($error) <= 0) {
                $this->session->set_flashdata('sucesso', 'Denuncia cadastrada com sucesso!');
            } else {
                $this->Apidenuncia_model->delete($denuncia_id);
                $this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
            }

            redirect('apidenuncia/carregarapi/' . $id, 'refresh');
        }
    }

    public function listar()
    {
        $dadosView['dados'] = $this->Apidenuncia_model->listarGo();


        $dadosView['meio']  = 'api/listar';
        $this->load->view('tema/tema', $dadosView);
    }

    public function visualizar()
    {
        $id = $this->uri->segment(3);

        $this->load->model('Denuncia_model');

        // $this->load->model('Empresa_model');
        // $this->load->model('TipoDenuncia_model');
        // $this->load->model('Departamento_model');   

        $dadosView['dados'] = $this->Apidenuncia_model->listarGo($id, 'denuncia_id');
        $dadosView['anexos'] = $this->Denuncia_model->getAnexosLista($id);
        // var_dump($dadosView['anexos']);die();

        // $dadosView['empresas']      = $this->Empresa_model->listar();
        // $dadosView['tipodenuncia']  = $this->TipoDenuncia_model->listar();
        // $dadosView['departamento']  = $this->Departamento_model->listar();                  
        // $dadosView['permissoes'] = $this->Denuncia_model->todasPermissoes();
        $dadosView['meio']       = 'api/visualizar';

        $this->load->view('tema/tema', $dadosView);
    }

    public function sicronizarExe()
    {
        $id = $this->uri->segment(3);
        $dados1 = array(
            'id_empresa' => $this->input->post('id_empresa'),
            'id_departamento' => $this->input->post('id_departamento'),
            'id_tipodenuncia' => $this->input->post('id_tipodenuncia'),
            'denuncia_data_fato' => $this->input->post('denuncia_data_fato'),
            'denuncia_data_cadastro' => $this->input->post('denuncia_data_cadastro'),
            'id_usuario' => $this->input->post('id_usuario'),
            'denuncia_relato' => $this->input->post('denuncia_relato'),
            'denuncia_nome' => $this->input->post('denuncia_nome'),
            'denuncia_contato' => $this->input->post('denuncia_contato'),
            'denuncia_email' => $this->input->post('denuncia_email')
        );
        $anexos = json_decode($this->input->post('anexos'));
        // $dados2 = array(              
        //     'denuncia_data_cadastro' => date('Y-m-d') // Data do 1º dia de cadastro             
        // );
        // $dados  = array_merge($dados1,$dados2);
        $resultado = $this->Apidenuncia_model->addApi($dados1, 'denuncia');
        if ($anexos != null) {
            foreach ($anexos as $chave => $valor) {
                $this->Apidenuncia_model->updateAnexo($valor->anexo_id, $resultado);
            }
        }

        if ($resultado) {
            $this->Apidenuncia_model->update($id);
            $this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');
        } else {
            $this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
        }
        redirect('apidenuncia/listar', 'refresh');
    }
}

/* End of file DenunciaIframe.php */
/* Location: ./application/controllers/Denuncia.php */