<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//class Empresa_model extends CI_Model {
class Empresa_model extends MY_Model {


	public $tabela  = 'empresa';
	public $chave   = 'empresa_id';
	public $visivel = 'empresa_visivel';


	public function pegarPorCnpj($cnpj)
	{
		$this->db->select('empresa_nomeRazao');		
		$this->db->where('empresa_cpfCnpj',$cnpj);		
		return $this->db->get($this->tabela)->result();
	}

	public function selecionarEmpresas($termo)
	{
		$this->db->select('empresa_id AS id, empresa_nomeFantasia AS text'); 		
		$this->db->like('empresa_nomeRazao',$termo);
		$this->db->or_like('empresa_nomeFantasia',$termo);			
		$this->db->where($this->visivel, 1);
		$this->db->limit(10);
		return $this->db->get($this->tabela)->result_array();
	}

	// public function inEmpresa($id=null)
	// {		
	// 	$this->db->select(' * ');		
	// 	$this->db->where_in($this->chave,$id);
	// 	$this->db->where($this->visivel, 1);		
	// 	return $this->db->get($this->tabela)->result();		
	// }


	// public function Listar2()
	// {
	// 	$this->db->select('*');
	// 	$this->db->where($this->visivel,'1');
	// 	return $this->db->get($this->tabela)->result();
	// }	




}

/* End of file Cliente_model.php */
/* Location: ./application/models/Cliente_model.php */