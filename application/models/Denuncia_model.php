<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//class Denuncia_model extends CI_Model {
class Denuncia_model extends MY_Model {

	public $tabela       = 'denuncia';
	public $chave   	 = 'denuncia_id';
	public $visivel      = 'denuncia_visivel';


	public function listarGo($id=null,$campo=null)
	{
		$this->db->select('denuncia.denuncia_id, empresa.empresa_nomeFantasia, tipodenuncia.nome as tp_denuncia, departamento.nome as dp_nome, denuncia_data_cadastro,
		denuncia_data_fato, denuncia_relato, denuncia_nome, denuncia_email, denuncia_contato, denuncia_und_localidade,
		denuncia.denuncia_status ');
		$this->db->join('empresa','empresa_id = id_empresa');
		$this->db->join('tipodenuncia','tipodenuncia_id = id_tipodenuncia');
		$this->db->join('departamento','departamento_id = id_departamento','left');		
		$this->db->where($this->visivel, 1);
		if ($id) {
		$this->db->where('denuncia.'.$campo,$id);	
		}	
		return $this->db->get($this->tabela)->result();	
	}


	public function add($data,$returnId = false){
        $this->db->insert($this->tabela, $data);         
        if ($this->db->affected_rows() == '1')
		{
            if($returnId == true){
                return $this->db->insert_id($this->tabela);
            }
			return TRUE;
		}
		
		return FALSE;       
    }


	public function todosDepartamentoEmpresa($id)
	{
		$this->db->select('*');		
		$this->db->like('id_empresa',$id);		
		return $this->db->get('departamento')->result();
	}	

	public function pegarPorId2($id)
	{
		$this->db->select('usuario_nome, usuario_id, denuncia.* ');
		$this->db->join('usuarios','usuario_id = id_usuario');
		// $this->db->join('anexos','id_denuncia = denuncia_id','LEFT');					
		$this->db->where($this->visivel, 1);
		$this->db->where($this->chave, $id);	
		return $this->db->get($this->tabela)->result();	
	}

	public function anexar($denuncia, $anexo, $url, $thumb, $path, $tamanho, $tipo){
        
        $this->db->set('anexo_anexo',$anexo);
        $this->db->set('anexo_url',$url);
        $this->db->set('anexo_thumb',$thumb);
        $this->db->set('anexo_path',$path);         
        $this->db->set('anexo_tamanho',$tamanho); 
        $this->db->set('anexo_tipo',$tipo);       
        $this->db->set('id_denuncia',$denuncia);

        return $this->db->insert('anexos');
    }
  
    public function getAnexos($id){
        $this->db->where('anexo_id',$id);
        $this->db->limit(1);
        return $this->db->get('anexos')->row();
    }

    public function getAnexosLista($id){
        $this->db->where('id_denuncia',$id);        
        return $this->db->get('anexos')->result();
    }

    public  function deleteAnexo($table,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE;        
    }

}

/* End of file Denuncia_model.php */
/* Location: ./application/models/Denuncia_model.php */