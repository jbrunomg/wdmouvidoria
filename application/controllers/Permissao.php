<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permissao extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Permissao_model');
	}

	public function index()
	{
		$resultado          = $this->Permissao_model->listar();
		$dadosView['dados'] = $resultado;
		$dadosView['meio']  = 'permissao/listar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionar()
	{
		$dadosView['meio']  = 'permissao/adicionar';
		$this->load->view('tema/tema',$dadosView);		
	}

	public function adicionarExe()
	{
		$dados = array(
		  'permissao_nome'     => $this->input->post('descricao'),
		  'permissao_permissoes'  => serialize(array('usuario' => 'sem permissao')),
		  'permissao_situacao' => 1,
		  'permissao_atualizacao'  => date('Y-m-d H:i:s'),
		  'permissao_visivel'      => 1
		);

		//p($dados);

		$resultado = $this->Permissao_model->inserir($dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
		}

		redirect('Permissao', 'refresh');
	}
	public function editar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados'] = $this->Permissao_model->pegarPorId($id);
		$dadosView['meio']  = 'permissao/editar';
		$this->load->view('tema/tema',$dadosView);		
	}
	public function editarExe()
	{
		$id = $this->input->post('id');

		$dados = array(
		  'permissao_nome'     => $this->input->post('descricao'),
		  'permissao_situacao' => 1,
		  'permissao_atualizacao'  => date('Y-m-d H:i:s'),
		  'permissao_visivel'      => 1
		);

		$resultado = $this->Permissao_model->editar($id, $dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
		}

		redirect('Permissao', 'refresh');
	}
	public function excluir()
	{
		$id = $this->input->post('id');

		$dados = array(
						'permissao_visivel'     => 0,						
						'permissao_atualizacao' => date('Y-m-d H:i:s')
					);


		$resultado = $this->Permissao_model->excluir($id,$dados);

		if ($resultado) {			
			echo json_encode(array('status' => true));
		}else{
			echo json_encode(array('status' => false));
		}
	}

	public function configurar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados'] = $this->Permissao_model->pegarPorId($id);
		$dadosView['meio']  = 'permissao/configurar';
		$this->load->view('tema/tema',$dadosView);		
	}
	public function configurarExe()
	{
		$id = $this->input->post('id');

		$permissoes = serialize($this->input->post('permissao'));

		$dados = array(
						'permissao_permissoes'  => $permissoes,					
						'permissao_atualizacao' => date('Y-m-d H:i:s')
					);

		$resultado = $this->Permissao_model->editar($id,$dados);

		if ($resultado) {
			if($id == $this->session->userdata('usuario_permissoes_id')){
				$this->session->unset_userdata('permissao');
				$this->session->set_userdata('permissao', $dados['permissao_permissoes']);
			}
			$this->session->set_flashdata('sucesso', 'Permissões alteradas com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para alterar as permissões ou não tiveram alterações!');
		}	

		redirect('Permissao','refresh');
	}
}

/* End of file Permissao.php */
/* Location: ./application/controllers/Permissao.php */