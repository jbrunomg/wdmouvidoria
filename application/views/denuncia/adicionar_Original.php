<!-- Form horizontal -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Cadastro de Denuncia</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<!-- <li><a data-action="reload"></a></li> -->
            		<!-- <li><a data-action="close"></a></li> -->
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/adicionarExe" method="post" enctype="multipart/form-data">
				
        
      <!-- Bordered tab content -->  

        <div class="tabbable tab-content-bordered">
            <ul class="nav nav-tabs nav-tabs-highlight">
              <li class="active"><a href="#bordered-tab1" data-toggle="tab">Denuncia</a></li>
              <li><a href="#bordered-tab2" data-toggle="tab">Anexo</a></li>               
            </ul>

            <div class="tab-content">
              <div class="tab-pane has-padding active" id="bordered-tab1">
                
                <!-- <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" /> -->
                <div class="form-group">
                  
                  <div class="col-md-4">
                    <div class="panel panel-body border-top-teal text-center">
                      <h6 class="no-margin text-semibold">Empresa</h6>
                        <br/>               
                        <select class="form-control" name="id_empresa" id="id_empresa">
                        <option value="">Selecione</option>
                          <?php foreach ($empresas as $e) { ?>
                              <option value="<?php echo $e->empresa_id; ?>"><?php echo $e->empresa_nomeFantasia; ?></option>
                        <?php } ?>               
                      </select> 
                      <?php echo form_error('id_empresa'); ?>                      
                    </div>
                  </div>  

                  <div class="col-md-4">
                    <div class="panel panel-body border-top-teal text-center">
                      <h6 class="no-margin text-semibold">Data do Fato</h6>
                        <br/>               
                        <div class="col-lg-12">
                          <input type="date" class="form-control" placeholder="Data do Fato" name="denuncia_data_fato">
                        </div>                        
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="panel panel-body border-top-teal text-center">
                      <h6 class="no-margin text-semibold">Usuário</h6>
                        <br/>               
                        <div class="col-lg-12">
                          <input disabled type="text" class="form-control" placeholder="Usuário" value="<?php echo $this->session->userdata('usuario_nome'); ?>">
                          <input type="hidden" class="form-control"  name="id_usuario"  value="<?php echo $this->session->userdata('usuario_id'); ?>">
                        </div>                        
                    </div>
                  </div>
                </div>

                <div class="form-group">          
                  <div class="col-md-6">
                    <div class="panel panel-body border-top-teal text-center">
                      <h6 class="no-margin text-semibold">Departemanto</h6>
                        <br/> 
                        <select name="id_departamento" id="departemento" class="js-example-basic-single js-states form-control" required></select>           
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="panel panel-body border-top-teal text-center">
                      <h6 class="no-margin text-semibold">Tipo de Denuncia</h6>
                        <br/>               
                        <select class="form-control" name="id_tipodenuncia" id="id_tipodenuncia">
                          <option value="">Selecione</option>
                            <?php foreach ($tipodenuncia as $t) { ?>
                                <option value="<?php echo $t->tipodenuncia_id; ?>"><?php echo $t->nome; ?></option>
                          <?php } ?>               
                        </select> 
                      <?php echo form_error('id_tipodenuncia'); ?>           
                    </div>
                  </div>                  
                </div>

                <div class="form-group">          
                  <div class="col-md-12">
                    <div class="panel panel-body border-top-teal text-center">
                      <h6 class="no-margin text-semibold">Relato:</h6>           
                      <textarea class="form-control rounded-0" name="denuncia_relato" id="denuncia_relato" rows="3"></textarea>           
                    </div>
                  </div>
                </div>

                <div class="form-group">
                    <div class="col-md-4">
                      <div class="panel panel-body border-top-teal text-center">
                        <h6 class="no-margin text-semibold">Responsável:</h6>           
                        <input  type="text" class="form-control" placeholder="Responsável"  data-mask-selectonfocus="true" name="denuncia_nome" id="denuncia_nome" value="<?php echo set_value('denuncia_nome'); ?>">          
                        <?php echo form_error('denuncia_nome'); ?>
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="panel panel-body border-top-teal text-center">
                        <h6 class="no-margin text-semibold">Telefone:</h6>           
                        <input  type="tel" class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="denuncia_contato" id="contato" value="<?php echo set_value('denuncia_contato'); ?>">
                        <?php echo form_error('denuncia_contato'); ?>                      
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="panel panel-body border-top-teal text-center">
                        <h6 class="no-margin text-semibold">Email:</h6>           
                        <input  type="text" placeholder="seu@email.com" class="form-control" name="denuncia_email" id="email" value="<?php echo set_value('denuncia_email'); ?>">
                        <?php echo form_error('denuncia_email'); ?>
                      </div>
                    </div>
                </div>
                
              </div>


              <div class="tab-pane has-padding" id="bordered-tab2">
                <div class="form-group">
                  <form id="formAnexos" enctype="multipart/form-data" action="javascript:;" accept-charset="utf-8" method="post">
                    <div class="col-md-4">
                    <label class="display-block">Anexo:</label>
                      <input name="recommendations" type="file" class="file-styled">
                      <span class="help-block">Accepted formats: pdf, doc. Max file size 2Mb</span>
                    </div>
                    <div class="col-md-4">
                      <label>Descrição:</label>
                      <textarea name="experience-description" rows="3" cols="4" placeholder="Descrição.." class="form-control"></textarea>
                    </div> 
                    <div class="col-md-4">
                      <div class="text-center">
                      </br></br>
                        <button type="submit" class="btn bg-teal">Anexar <i class="icon-arrow-right14 position-right"></i></button>
                      </div>
                    </div>
                  </form> 

                </div>
              </div>

 
            </div>
          </div>

        <!-- /bordered tab content -->
        
        </br></br>  
				<div class="text-right">
					<button type="submit" class="btn bg-teal">Cadastrar <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>