<!-- Form horizontal -->
	<div class="panel panel-flat">

		
			<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/adicionarExe" method="post" enctype="multipart/form-data">
			
           
              <div class="panel-heading">
                <h5 class="panel-title">Cadastro</h5>
                <div class="heading-elements">
                  <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                   <!--  <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li> -->
                  </ul>
                </div>
              </div>

              <div class="panel-body">
                <div class="row">
                  <div class="col-md-6">
                    <fieldset>
                      <legend class="text-semibold"><i class="icon-reading position-left"></i>Detalhar Denuncia</legend>

                      <div class="form-group">
                        <label class="col-lg-3 control-label">Empresa:</label>
                        <div class="col-lg-9">
                          <select class="form-control" name="id_empresa" id="id_empresa">
                            <option value="">Selecione</option>
                              <?php foreach ($empresas as $e) { ?>
                                  <option value="<?php echo $e->empresa_id; ?>"><?php echo $e->empresa_nomeFantasia; ?></option>
                              <?php } ?>               
                          </select> 
                          <?php echo form_error('id_empresa'); ?>  
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-lg-3 control-label">Unidade/ Localidade:</label>
                        <div class="col-lg-9">
                          <input  type="text" class="form-control" placeholder="Und/Loc"  name="denuncia_und_localidade" id="denuncia_und_localidade" value="<?php echo set_value('denuncia_und_localidade'); ?>">          
                          <?php echo form_error('denuncia_und_localidade'); ?>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-lg-3 control-label">Data do fato:</label>
                        <div class="col-lg-9">
                          <input type="date" class="form-control" placeholder="Data do Fato" name="denuncia_data_fato">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-lg-3 control-label">Usuário:</label>
                        <div class="col-lg-9">
                          <input disabled type="text" class="form-control" placeholder="Usuário" value="<?php echo $this->session->userdata('usuario_nome'); ?>">
                          <input type="hidden" class="form-control"  name="id_usuario"  value="<?php echo $this->session->userdata('usuario_id'); ?>">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-lg-3 control-label">Departamento:</label>
                        <div class="col-lg-9">
                          <select name="id_departamento" id="departemento" class="js-example-basic-single js-states form-control" required></select>           
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-lg-3 control-label">Tipo de Denuncia:</label>
                        <div class="col-lg-9">
                          <select class="form-control" name="id_tipodenuncia" id="id_tipodenuncia">
                          <option value="">Selecione</option>
                                <?php foreach ($tipodenuncia as $t) { ?>
                                    <option value="<?php echo $t->tipodenuncia_id; ?>"><?php echo $t->nome; ?></option>
                              <?php } ?>               
                            </select> 
                          <?php echo form_error('id_tipodenuncia'); ?> 
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-lg-3 control-label">Status:</label>
                        <div class="col-lg-9">
                          <select class="form-control" name="denuncia_status" id="denuncia_status">
                            <option value="aberto">Em aberto</option>
                            <option value="analise">Em análise</option>
                            <option value="encerrado">Encerrado</option>               
                          </select>                            
                        </div>
                      </div>            

                      <div class="form-group">
                        <label class="col-lg-3 control-label">Relato:</label>
                        <div class="col-lg-9">
                          <textarea class="form-control rounded-0" name="denuncia_relato" id="denuncia_relato" rows="3"></textarea>           
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-lg-3 control-label">Denunciante:</label>
                        <div class="col-lg-9">
                          <input  type="text" class="form-control" placeholder="Responsável"  data-mask-selectonfocus="true" name="denuncia_nome" id="denuncia_nome" value="<?php echo set_value('denuncia_nome'); ?>">          
                          <?php echo form_error('denuncia_nome'); ?>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-lg-3 control-label">Telefone:</label>
                        <div class="col-lg-9">
                          <input  type="tel" class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="denuncia_contato" id="contato" value="<?php echo set_value('denuncia_contato'); ?>">
                          <?php echo form_error('denuncia_contato'); ?> 
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-lg-3 control-label">Email:</label>
                        <div class="col-lg-9">
                          <input  type="text" placeholder="seu@email.com" class="form-control" name="denuncia_email" id="email" value="<?php echo set_value('denuncia_email'); ?>">
                          <?php echo form_error('denuncia_email'); ?>
                        </div>
                      </div>

        
                    </fieldset>
                  </div>

                  <div class="col-md-6">
                    <fieldset>
                      <legend class="text-semibold"><i class="icon-drawer-in position-left"></i> Anexo(s)</legend>
      

                    </fieldset>
                  </div>
                </div>


                <div class="text-right">
                  <button type="submit" class="btn bg-teal">Cadastrar <i class="icon-arrow-right14 position-right"></i></button>
                </div>               
              </div>
              
      
			</form>
		
	</div>