<table class="table">	
	<tbody>		
		<tr>	
			<td style="font-size:12px;">
				<?php 
					echo $emitente->emitente_nome."<br>";
					echo "Fone  : ".$emitente->emitente_telefone."<br>";
					echo "E-mail: ".$emitente->emitente_email."<br>";					
				?>
			</td>		
			<td class="text-right"><img src="<?php echo $emitente->emitente_imagem;?>" width="100px" heigth="100px"></td>
			
		</tr>		
	</tbody>
</table>
<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px"><?php echo $titulo; ?></h3>
<table class="table table-bordered">
	<thead>
		<tr>			
			<th style="padding: 5px;fonte-size:12" class="text-center">Sacado</th>
			<th style="padding: 5px;fonte-size:12" class="text-center">Nº da N.F</th>
			<th style="padding: 5px;fonte-size:12" class="text-center">Empresa</th>
			<th style="padding: 5px;fonte-size:12" class="text-center">Estag. Cobrados</th>
			<th style="padding: 5px;fonte-size:12" class="text-center">Estag. Ativos</th>
			<th style="padding: 5px;fonte-size:12" class="text-center">Valor</th>
			<th style="padding: 5px;fonte-size:12" class="text-center">Vencimento</th>
			<th style="padding: 5px;fonte-size:12" class="text-center">Valor da Taxa</th>
		</tr>
	</thead>
	<tbody>
		<?php		
		$cont = 0;
		$totalServico = 0;
		
		$totalEstCobrado = 0;
		$totalEst = 0; ?>
		
		<?php foreach ($dados as $valor){
		$preco = $valor->valor;
		$totalServico = $totalServico + $preco;

		$totalEstCobrado = $totalEstCobrado + $valor->cobrado;

		$totalEst = $totalEst + $valor->ativo;

		?>
			<?php $cont++; ?>			
			<tr>				
				<td style="padding: 5px;fonte-size:12" class="text-center"><?php echo $valor->sacado; ?></td>
				<td style="padding: 5px;fonte-size:12" class="text-center"><?php echo $valor->nota; ?></td>
				<?php if ($valor->fantasia <> '') { ?>
					<td style="padding: 5px;fonte-size:12" class="text-center"><?php echo $valor->fantasia; ?></td>
				<?php } else { ?>
				<td style="padding: 5px;fonte-size:12" class="text-center"><?php echo $valor->empresa; ?></td>
				<?php } ?>
				<td style="padding: 5px;fonte-size:12" class="text-center"><?php echo $valor->cobrado; ?></td>
				<td style="padding: 5px;fonte-size:12" class="text-center"><?php echo $valor->ativo; ?></td>
				<td style="padding: 5px;fonte-size:12" class="text-center"><?php echo 'R$'. number_format($valor->valor, 2, ",", ".") ?></td>				
				<td style="padding: 5px;fonte-size:12" class="text-center"><?php echo $valor->vencimento.'/'.date('m').'/'.date('Y'); ?></td>
				<td style="padding: 5px;fonte-size:12" class="text-center"><?php echo 'R$'. number_format($valor->taxa, 2, ",", ".") ?></td>
				
			</tr>	
		
		<?php } ?>	
	</tbody>
</table>

<td style="WORD-BREAK:BREAK-ALL;">
<br/>
<h3 class="text-center" style="margin-top:-10px"><?php echo "Visão Geral" ?></h3>

<table class="table table-bordered">
	<thead>
		<tr>			
			<th style="padding: 5px;fonte-size:12" class="text-center">Estag. Cobrados</th>
			<th style="padding: 5px;fonte-size:12" class="text-center">Estag. Ativos</th>
	
		</tr>
	</thead>
	<tbody>		
		<tr>				
			<td style="padding: 5px;fonte-size:12" class="text-center"><?php echo $totalEstCobrado;?></td>
			<td style="padding: 5px;fonte-size:12" class="text-center"><?php echo $totalEst;?></td>		
		</tr>		
	</tbody>
</table>

<table class="table table-bordered">
	<thead>
		<tr>			
			<th style="padding: 5px;fonte-size:12" class="text-center">Totais Empresa:</th>
			<th style="padding: 5px;fonte-size:12" class="text-center">Valor Total:</th>
	
		</tr>
	</thead>
	<tbody>		
		<tr>				
			<td style="padding: 5px;fonte-size:12" class="text-center"><?php echo $cont;?></td>
			<td style="padding: 5px;fonte-size:12" class="text-center">R$ <?php echo number_format( $totalServico,2,',','.');?></td>		
		</tr>		
	</tbody>
</table>

<br/>
<h3 class="text-center" style="margin-top:-10px"><?php echo "Visão Geral II" ?></h3>

<div class="container">

	<div class="col-md-4" >
		<table class="table table-bordered" width="400px">
			<thead>
				<tr>			
					<th style="padding: 5px;fonte-size:8" class="text-center">Empresa(s) Inativa(s):</th>			
				</tr>
			</thead>
			<tbody>		
				<?php foreach ($empreinativa as $e){ ?>							
					<tr>				
						<td style="padding: 5px;fonte-size:8" class="text-center"><?php echo $e->inativas; ?></td>									
					</tr>				
				<?php } ?>		
			</tbody>
		</table>
	</div>

	<div class="col-md-4">
		<table class="table table-bordered" width="400px">
			<thead>
				<tr>			
					<th style="padding: 5px;fonte-size:8" class="text-center">Empresa(s) Nova(s):</th>				
				</tr>
			</thead>
			<tbody>		
				<?php foreach ($emprenova as $e){ ?>							
					<tr>				
						<td style="padding: 5px;fonte-size:8" class="text-center"><?php echo $e->novas; ?></td>									
					</tr>				
				<?php } ?>		
			</tbody>
		</table>
	</div>

	<div class="col-md-4">
		<table class="table table-bordered" width="400px">
			<thead>
				<tr>					
					<th style="padding: 5px;fonte-size:8" class="text-center">Empresa(s) Reativada(s):</th>			
				</tr>
			</thead>
			<tbody>		
				<?php foreach ($emprereativada as $e){ ?>							
					<tr>				
						<td style="padding: 5px;fonte-size:8" class="text-center"><?php echo $e->reativada; ?></td>									
					</tr>				
				<?php } ?>		
			</tbody>
		</table>
	</div>

</div>




<!-- <h4 style="text-align: right; color: grey">Totais Empresa: <?php echo $cont;?></h4> 
<h4 style="text-align: right; color: grey">Estag. Cobrados <?php echo $totalEstCobrado;?></h4>
<h4 style="text-align: right; color: grey">Estag. Ativos <?php echo $totalEst;?></h4>

<h4 style="text-align: right">Valor Total: R$ <?php echo number_format( $totalServico,2,',','.');?></h4> -->
<h4 style="text-align: right">Data do Relatório: <?php echo date('d/m/Y');?></h4>		
