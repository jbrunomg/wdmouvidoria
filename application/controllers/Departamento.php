<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Departamento extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Departamento_model');				
	}

	public function index()
	{
		$resultado = $this->Departamento_model->listar();		

		$dadosView['dados'] = $resultado;		
		$dadosView['meio']  = 'departamento/listar';
		$this->load->view('tema/tema',$dadosView);
	}


	public function adicionar()
	{		
		$dadosView['permissoes'] = $this->Departamento_model->todasPermissoes();
		$dadosView['meio']       = 'departamento/adicionar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionarExe()
	{			

		$dados = array(
			'nome'          => $this->input->post('nome'),	
			'id_empresa'    => implode(',',$this->input->post('empresa')),
			'data_cadastro' => date('Y-m-d H:i:s')
		);
	
		$resultado = $this->Departamento_model->inserir($dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
		}

		redirect('Departamento', 'refresh');
	}

	public function editar()
	{
		$id = $this->uri->segment(3);
		$this->load->model('Empresa_model');

		$dadosView['dados']      = $this->Departamento_model->pegarPorId($id);
		$dadosView['empresasdp'] = explode(',',$dadosView['dados'][0]->id_empresa);		

		$dadosView['empresas']   = $this->Empresa_model->listar();		

		$dadosView['permissoes'] = $this->Departamento_model->todasPermissoes();
		$dadosView['meio']       = 'departamento/editar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function editarExe()
	{	
	
		$id = $this->input->post('departamento_id');

		// $dados = $this->input->post();

		$dados = array(
			'nome'          => $this->input->post('nome'),	
			'id_empresa'    => implode(',',$this->input->post('empresa'))
		);

		$resultado = $this->Departamento_model->editar($id,$dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro alterado com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para alterado o registro!');
		}

		redirect('Departamento', 'refresh');
	}

	public function excluir()
	{
		$id = $this->input->post('id');

		$resultado = $this->Departamento_model->excluir($id);

		if ($resultado) {			
			echo json_encode(array('status' => true));
		}else{
			echo json_encode(array('status' => false));
		}
	}
	
	public function visualizar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']      = $this->Departamento_model->pegarPorId($id);
		$dadosView['permissoes'] = $this->Departamento_model->todasPermissoes();
		$dadosView['meio']       = 'departamento/visualizar';
		$this->load->view('tema/tema',$dadosView);
	}

}

/* End of file Usuario.php */
/* Location: ./application/controllers/Usuario.php */