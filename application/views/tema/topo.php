<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>WDM - Ouvidoria</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/core.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/colors.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/timepicki.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->	

	
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/core/libraries/jquery.min.js"></script>	

     <script type="text/javascript" src="<?php echo base_url()?>public/assets/js/maps/google/basic/basic.js"></script>



	<script type="text/javascript">
	var base_url = '<?php echo base_url(); ?>';
	var token = '<?php echo $this->security->get_csrf_hash(); ?>';
	</script>
	

</head>

<body>
<!-- Main content -->
	<div class="content-wrapper">
		
