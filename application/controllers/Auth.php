<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Auth_model');
	}

	public function index()
	{
		$this->load->view('login');
	}

	public function processarLogin()
	{

		$this->form_validation->set_rules('login', 'Login', 'trim|required|valid_email');       
        $this->form_validation->set_rules('senha', 'Senha', 'trim|required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('login');
        }
        else
        {	        	

        	$dados = array(
        					'usuario_email'   => $this->input->post('login'),
        					'usuario_senha'   => sha1(md5($this->input->post('senha'))),
        					'usuario_visivel' => 1,
        					'usuario_situacao'=> 1,
        					'permissao_visivel'  => 1
        				  );        	

        	$resultado = $this->Auth_model->processarLogin($dados); 
        	
        	if($resultado){

	        	$session = array(
						        'usuario_id'            => $resultado[0]->usuario_id,
						        'usuario_email'         => $resultado[0]->usuario_email,
						        'usuario_nome'          => $resultado[0]->usuario_nome,
						        'permissao_nome'        => $resultado[0]->permissao_nome,					       
						        'usuario_permissoes_id' => $resultado[0]->usuario_permissoes_id,
						        'usuario_imagem'        => $resultado[0]->usuario_imagem,						      
						        'permissao'             => $resultado[0]->permissao_permissoes					       
							);

				$this->session->set_userdata($session);

				redirect('Dashboard','refresh');

        	}else{

        		$this->session->set_flashdata('erro', 'Login/Senha Inválidos');
        		$this->load->view('login');
        	}
        }
		
	}

	public function sair()
	{
		$this->session->sess_destroy();
		$this->load->view('login');
	}
}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */