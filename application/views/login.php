<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>WDM - Ouvidoria</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/core.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/colors.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/validation/validate.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/core/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/login_validation.js"></script>
	<!-- /theme JS files -->
	<script type="text/javascript" src="<?php echo base_url() ?>public/assets/js/plugins/notifications/bootbox.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>public/assets/js/plugins/notifications/sweet_alert.min.js"></script>
	

</head>

<body class="login-cover">

	<!-- Page container -->
	<div class="page-container login-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<!-- Form with validation -->
					<form action="<?php echo base_url() ?>auth/processarLogin" class="form-validate" method="post">
						<div class="panel panel-body login-form">
							<div class="text-center">
								<img src="<?php echo base_url() ?>/public/assets/images/logo.png" width="100px">
								<h5 class="content-group"> <small class="display-block">Digite seus dados de acesso</small></h5>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<input type="email" class="form-control" placeholder="Usuário" name="login" required="required" value="<?php echo set_value('login'); ?>">
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
								</div>
								<?php echo form_error('login'); ?>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<input type="password" class="form-control" placeholder="Senha" name="senha" required="required" value="<?php echo set_value('senha'); ?>">
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
								<?php echo form_error('senha'); ?>
							</div>

							<div class="form-group login-options">
								<div class="row">
									<div class="col-sm-6">
									</div>

									<!-- <div class="col-sm-6 text-right">
										<a href="<?php echo base_url(); ?>welcome/esqueceuSenha">Esqueceu a senha?</a>
									</div> -->
								</div>
							</div>

							<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

							<div class="form-group">
								<button type="submit" class="btn bg-blue btn-block">Logar<i class="icon-arrow-right14 position-right"></i></button>
							</div>
							
						</div>
					</form>
					<!-- /form with validation -->


					<!-- Footer -->
					<div class="footer text-white">
						&copy; 2019 - <?php echo date('Y') ?>. <a href="www.wdmtecnologia.com.br" class="text-white">WDM - Auditoria</a> by <a href="#" class="text-white" target="_blank">Link Para o autor</a>
					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

	<?php if(!(NULL === $this->session->flashdata('erro'))){ ?>
		<script type="text/javascript">
			        swal({
			            title: "Oops...",
			            text: "<?php echo $this->session->flashdata('erro'); ?>",
			            confirmButtonColor: "#EF5350",
			            type: "error"
			        });
		</script>
	<?php }
	$this->session->unset_userdata('erro');
	 ?>

</body>
</html>
