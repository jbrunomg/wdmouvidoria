<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SubDepartamento extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('SubDepartamento_model');

	}

	public function index()
	{
		$resultado = $this->SubDepartamento_model->listarGo();

		$dadosView['dados'] = $resultado;
		$dadosView['meio']  = 'subdepartamento/listar';
		$this->load->view('tema/tema',$dadosView);
	}


	public function adicionar()
	{		
		$this->load->model('Departamento_model');

		$dadosView['dados'] = $this->Departamento_model->listar();
		$dadosView['permissoes'] = $this->SubDepartamento_model->todasPermissoes();
		$dadosView['meio']       = 'subdepartamento/adicionar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionarExe()
	{			

		$dados1 = $this->input->post();

		$dados2 = array(		  
		  'data_cadastro'    => date('Y-m-d H:i:s')
		);

		$dados = array_merge($dados1,$dados2);

	
		$resultado = $this->SubDepartamento_model->inserir($dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
		}

		redirect('SubDepartamento', 'refresh');
	}

	public function editar()
	{
		$this->load->model('Departamento_model');

		$id = $this->uri->segment(3);

		$dadosView['dados']        = $this->SubDepartamento_model->pegarPorId($id);
		$dadosView['departamento'] = $this->Departamento_model->listar();

		//var_dump($dadosView['departamento']);die(); 

		$dadosView['permissoes']   = $this->SubDepartamento_model->todasPermissoes();
		$dadosView['meio']         = 'subdepartamento/editar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function editarExe()
	{	
	
		$id = $this->input->post('sub_departamento_id');

		$dados = $this->input->post();

		$resultado = $this->SubDepartamento_model->editar($id,$dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro alterado com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para alterado o registro!');
		}

		redirect('SubDepartamento', 'refresh');
	}

	public function excluir()
	{
		$id = $this->input->post('id');

		$resultado = $this->SubDepartamento_model->excluir($id);

		if ($resultado) {			
			echo json_encode(array('status' => true));
		}else{
			echo json_encode(array('status' => false));
		}
	}
	
	public function visualizar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']      = $this->SubDepartamento_model->pegarPorId($id);
		$dadosView['permissoes'] = $this->SubDepartamento_model->todasPermissoes();
		$dadosView['meio']       = 'subdepartamento/visualizar';
		$this->load->view('tema/tema',$dadosView);
	}

}

/* End of file Usuario.php */
/* Location: ./application/controllers/Usuario.php */