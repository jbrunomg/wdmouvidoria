<?php // var_dump($dados);die(); 
?>
<!-- Form horizontal -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Edição de Empresa</h5>
		<div class="heading-elements">
			<ul class="icons-list">
				<li><a data-action="collapse"></a></li>
				<!-- <li><a data-action="reload"></a></li> -->
				<!-- <li><a data-action="close"></a></li> -->
			</ul>
		</div>
	</div>

	<div class="panel-body">
		<form class="form-horizontal" action="<?= base_url(); ?><?= $this->uri->segment(1); ?>/sicronizarExe/<?= $dados[0]->denuncia_id; ?>" method="post" enctype="multipart/form-data">
			<fieldset class="content-group">
				<legend class="text-bold">Dados Empresa:</legend>

				<!-- <input  type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" /> -->

				<input disabled type="hidden" name="denuncia_id" value="<?= $dados[0]->denuncia_id; ?>" />

				<div class="form-group">

					<div class="col-md-4">
						<div class="panel panel-body border-top-teal text-center">
							<h6 class="no-margin text-semibold">Empresa</h6>
							<br />
							<div class="col-lg-12">
								<input disabled type="text" class="form-control" placeholder="Empresa" value="<?= $dados[0]->empresa_nomeFantasia; ?>">
								<input type="hidden" class="form-control" name="id_empresa" value="<?= $dados[0]->id_empresa; ?>">
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="panel panel-body border-top-teal text-center">
							<h6 class="no-margin text-semibold">Unidade/ Localidade</h6>
							<br />
							<div class="col-lg-12">
								<input readonly type="text" class="form-control" placeholder="Und/Loc" name="denuncia_und_localidade" value="<?= $dados[0]->denuncia_und_localidade; ?>">
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="panel panel-body border-top-teal text-center">
							<h6 class="no-margin text-semibold">Departemanto</h6>
							<br />
							<div class="col-lg-12">
								<input disabled type="text" class="form-control" placeholder="Departamento" value="<?= $dados[0]->dp_nome; ?>">
								<input type="hidden" class="form-control" name="id_departamento" value="<?= $dados[0]->id_departamento; ?>">
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="panel panel-body border-top-teal text-center">
							<h6 class="no-margin text-semibold">Tipo de Denuncia</h6>
							<br />
							<div class="col-lg-12">
								<input disabled type="text" class="form-control" placeholder="Tipo Denuncia" value="<?= $dados[0]->tp_denuncia; ?>">
								<input type="hidden" class="form-control" name="id_tipodenuncia" value="<?= $dados[0]->id_tipodenuncia; ?>">
							</div>
						</div>
					</div>

				</div>


				<div class="form-group">

					<div class="col-md-4">
						<div class="panel panel-body border-top-teal text-center">
							<h6 class="no-margin text-semibold">Data do Fato</h6>
							<br />
							<div class="col-lg-12">
								<input readonly type="date" class="form-control" placeholder="Data do Fato" name="denuncia_data_fato" value="<?= $dados[0]->denuncia_data_fato; ?>">
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="panel panel-body border-top-teal text-center">
							<h6 class="no-margin text-semibold">Data da Denuncia</h6>
							<br />
							<div class="col-lg-12">
								<input readonly type="date" class="form-control" placeholder="Data do Fato" name="denuncia_data_cadastro" value="<?= $dados[0]->denuncia_data_cadastro; ?>">
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="panel panel-body border-top-teal text-center">
							<h6 class="no-margin text-semibold">Usuário</h6>
							<br />
							<div class="col-lg-12">
								<input disabled type="text" class="form-control" placeholder="Usuário" value="<?= $this->session->userdata('usuario_nome'); ?>">
								<input type="hidden" class="form-control" name="id_usuario" value="<?= $this->session->userdata('usuario_id'); ?>">
							</div>
						</div>
					</div>


				</div>

				<div class="form-group">

					<div class="col-md-12">
						<div class="panel panel-body border-top-teal text-center">
							<h6 class="no-margin text-semibold">Relato:</h6>
							<textarea readonly class="form-control rounded-0" name="denuncia_relato" id="denuncia_relato" rows="3"><?= $dados[0]->denuncia_relato; ?></textarea>
						</div>
					</div>
				</div>
				<?php if (!empty($anexos)) : ?>
					<div class="table-responsive panel panel-body border-top-teal">
						<div class="text-center">
							<h6 class="no-margin text-semibold">Anexos:</h6>
						</div>

						<table class="table ">
							<thead>
								<tr class="border-double">
									<th>#</th>
									<th>Nome</th>
									<th>Tamanho</th>
									<th>Tipo</th>
									<th>Opções</th>
								</tr>
							</thead>
							<tbody>
								<input type="hidden" name="anexos" value="<?= htmlspecialchars(json_encode($anexos)) ?>">
								<?php foreach ($anexos as $a) : ?>
									<tr>
										<?php if ($a->anexo_tipo == '.mp3') : ?>
											<td>
												<audio src="<?= $a->anexo_url . $a->anexo_anexo; ?>" preload="auto" controls>
													<p>Seu nevegador não suporta o elemento audio.</p>
												</audio>
											</td>
										<?php elseif ($a->anexo_tipo == '.mp4') : ?>
											<td>
												<video src="<?= $a->anexo_url . $a->anexo_anexo; ?>" controls width="280" height="200">
													Seu navegador não suporta o elemento <code>video</code>.
												</video>
											</td>
										<?php else : ?>
											<td>
												<div class="media-left media-middle">
													<a href="<?= $a->anexo_url . 'thumbs/' . $a->anexo_thumb; ?>" data-popup="lightbox">
														<img src="<?= $a->anexo_url . 'thumbs/' . $a->anexo_thumb; ?>" alt="" class="img-rounded img-preview">
													</a>
												</div>
											</td>
										<?php endif ?>

										<td><?= $a->anexo_anexo; ?></td>
										<td><?= $a->anexo_tamanho; ?></td>
										<td><?= $a->anexo_tipo; ?></td>

										<td>
											<ul class="icons-list">
												<?php if (checarPermissao('vDenuncia')) : ?>
													<li class="text-teal-600"><a target="_blank" href="<?= $a->anexo_url ?><?= $a->anexo_anexo ?>" data-popup="tooltip" title="Visualizar"><i class="icon-search4"></i></a></li>
												<?php endif ?>
												<?php if (checarPermissao('vDenuncia')) : ?>
													<!--		Reutilizando o controller Denuncia.		-->
													<li class="text-teal-600"><a href="<?= base_url() ?>Denuncia/download/<?= $a->anexo_id; ?>" data-popup="tooltip" title="download"><i class="icon-file-download"></i></a></li>
												<?php endif ?>
												<!--		/reutilizando o controller Denuncia.		-->
											</ul>
										</td>
									</tr>
								<?php endforeach ?>
							</tbody>
						</table>
					</div>
				<?php endif ?>
				<div class="form-group">

					<div class="col-md-4">
						<div class="panel panel-body border-top-teal text-center">
							<h6 class="no-margin text-semibold">Responsável:</h6>
							<input readonly type="text" class="form-control" placeholder="Responsável" data-mask-selectonfocus="true" name="denuncia_nome" id="denuncia_nome" value="<?= $dados[0]->denuncia_nome; ?>">
						</div>
					</div>

					<div class="col-md-4">
						<div class="panel panel-body border-top-teal text-center">
							<h6 class="no-margin text-semibold">Telefone:</h6>
							<input readonly type="tel" class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="denuncia_contato" id="contato" value="<?= $dados[0]->denuncia_contato; ?>">
						</div>
					</div>

					<div class="col-md-4">
						<div class="panel panel-body border-top-teal text-center">
							<h6 class="no-margin text-semibold">Email:</h6>
							<input readonly type="text" placeholder="seu@email.com" class="form-control" name="denuncia_email" id="email" value="<?= $dados[0]->denuncia_email; ?>">
						</div>
					</div>

				</div>


			</fieldset>
			<div class="text-center">
				<button type="submit" class="btn bg-teal">Sicronizar <i class="icon-database-refresh position-right"></i></button>
			</div>
		</form>
	</div>
</div>