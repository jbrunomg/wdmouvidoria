<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pagamentosistema extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Pagamentosistema_model');
    }
    


    public function index()
    {

        $resultado = $this->Pagamentosistema_model->listar();

        $dadosView['dados'] = $resultado;
        $dadosView['meio']  = 'pagamentosistema/pagamento';
        $this->load->view('tema/tema',$dadosView);
    }
 

    public function visualizar()
    {
        $id = $this->uri->segment(3);

        $resultado = $this->Pagamentosistema_model->pegarPorId($id);

        // echo "<pre>";
        // var_dump($resultado);die();
               
        //$this->data['emitente'] = $this->mapos_model->getEmitente();

        $dadosView['results'] = $resultado;
        $dadosView['meio']  = 'pagamentosistema/visualizar';
        $this->load->view('tema/tema',$dadosView);
       
    }
}

