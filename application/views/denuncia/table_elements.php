<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>TESTE MAGNATA</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/core.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/colors.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	

</head>

<body>

	<!-- Main navbar -->
	<div class="navbar navbar-inverse">
		<div class="navbar-header">
			<a class="navbar-brand" href="index.html"><img src="assets/images/logo_light.png" alt=""></a>

			<ul class="nav navbar-nav visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav">
				<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-git-compare"></i>
						<span class="visible-xs-inline-block position-right">Git updates</span>
						<span class="badge bg-warning-400">9</span>
					</a>
					
					<div class="dropdown-menu dropdown-content">
						<div class="dropdown-content-heading">
							Git updates
							<ul class="icons-list">
								<li><a href="#"><i class="icon-sync"></i></a></li>
							</ul>
						</div>

						<ul class="media-list dropdown-content-body width-350">
							<li class="media">
								<div class="media-left">
									<a href="#" class="btn border-primary text-primary btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-pull-request"></i></a>
								</div>

								<div class="media-body">
									Drop the IE <a href="#">specific hacks</a> for temporal inputs
									<div class="media-annotation">4 minutes ago</div>
								</div>
							</li>

							<li class="media">
								<div class="media-left">
									<a href="#" class="btn border-warning text-warning btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-commit"></i></a>
								</div>
								
								<div class="media-body">
									Add full font overrides for popovers and tooltips
									<div class="media-annotation">36 minutes ago</div>
								</div>
							</li>

							<li class="media">
								<div class="media-left">
									<a href="#" class="btn border-info text-info btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-branch"></i></a>
								</div>
								
								<div class="media-body">
									<a href="#">Chris Arney</a> created a new <span class="text-semibold">Design</span> branch
									<div class="media-annotation">2 hours ago</div>
								</div>
							</li>

							<li class="media">
								<div class="media-left">
									<a href="#" class="btn border-success text-success btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-merge"></i></a>
								</div>
								
								<div class="media-body">
									<a href="#">Eugene Kopyov</a> merged <span class="text-semibold">Master</span> and <span class="text-semibold">Dev</span> branches
									<div class="media-annotation">Dec 18, 18:36</div>
								</div>
							</li>

							<li class="media">
								<div class="media-left">
									<a href="#" class="btn border-primary text-primary btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-pull-request"></i></a>
								</div>
								
								<div class="media-body">
									Have Carousel ignore keyboard events
									<div class="media-annotation">Dec 12, 05:46</div>
								</div>
							</li>
						</ul>

						<div class="dropdown-content-footer">
							<a href="#" data-popup="tooltip" title="All activity"><i class="icon-menu display-block"></i></a>
						</div>
					</div>
				</li>
			</ul>

			<p class="navbar-text">
				<span class="label bg-success">Online</span>
			</p>

			<div class="navbar-right">
				<ul class="nav navbar-nav">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-people"></i>
							<span class="visible-xs-inline-block position-right">Users</span>
						</a>
						
						<div class="dropdown-menu dropdown-content">
							<div class="dropdown-content-heading">
								Users online
								<ul class="icons-list">
									<li><a href="#"><i class="icon-gear"></i></a></li>
								</ul>
							</div>

							<ul class="media-list dropdown-content-body width-300">
								<li class="media">
									<div class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></div>
									<div class="media-body">
										<a href="#" class="media-heading text-semibold">Jordana Ansley</a>
										<span class="display-block text-muted text-size-small">Lead web developer</span>
									</div>
									<div class="media-right media-middle"><span class="status-mark border-success"></span></div>
								</li>

								<li class="media">
									<div class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></div>
									<div class="media-body">
										<a href="#" class="media-heading text-semibold">Will Brason</a>
										<span class="display-block text-muted text-size-small">Marketing manager</span>
									</div>
									<div class="media-right media-middle"><span class="status-mark border-danger"></span></div>
								</li>

								<li class="media">
									<div class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></div>
									<div class="media-body">
										<a href="#" class="media-heading text-semibold">Hanna Walden</a>
										<span class="display-block text-muted text-size-small">Project manager</span>
									</div>
									<div class="media-right media-middle"><span class="status-mark border-success"></span></div>
								</li>

								<li class="media">
									<div class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></div>
									<div class="media-body">
										<a href="#" class="media-heading text-semibold">Dori Laperriere</a>
										<span class="display-block text-muted text-size-small">Business developer</span>
									</div>
									<div class="media-right media-middle"><span class="status-mark border-warning-300"></span></div>
								</li>

								<li class="media">
									<div class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></div>
									<div class="media-body">
										<a href="#" class="media-heading text-semibold">Vanessa Aurelius</a>
										<span class="display-block text-muted text-size-small">UX expert</span>
									</div>
									<div class="media-right media-middle"><span class="status-mark border-grey-400"></span></div>
								</li>
							</ul>

							<div class="dropdown-content-footer">
								<a href="#" data-popup="tooltip" title="All users"><i class="icon-menu display-block"></i></a>
							</div>
						</div>
					</li>

					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-bubbles4"></i>
							<span class="visible-xs-inline-block position-right">Messages</span>
							<span class="badge bg-warning-400">2</span>
						</a>
						
						<div class="dropdown-menu dropdown-content width-350">
							<div class="dropdown-content-heading">
								Messages
								<ul class="icons-list">
									<li><a href="#"><i class="icon-compose"></i></a></li>
								</ul>
							</div>

							<ul class="media-list dropdown-content-body">
								<li class="media">
									<div class="media-left">
										<img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt="">
										<span class="badge bg-danger-400 media-badge">5</span>
									</div>

									<div class="media-body">
										<a href="#" class="media-heading">
											<span class="text-semibold">James Alexander</span>
											<span class="media-annotation pull-right">04:58</span>
										</a>

										<span class="text-muted">who knows, maybe that would be the best thing for me...</span>
									</div>
								</li>

								<li class="media">
									<div class="media-left">
										<img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt="">
										<span class="badge bg-danger-400 media-badge">4</span>
									</div>

									<div class="media-body">
										<a href="#" class="media-heading">
											<span class="text-semibold">Margo Baker</span>
											<span class="media-annotation pull-right">12:16</span>
										</a>

										<span class="text-muted">That was something he was unable to do because...</span>
									</div>
								</li>

								<li class="media">
									<div class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></div>
									<div class="media-body">
										<a href="#" class="media-heading">
											<span class="text-semibold">Jeremy Victorino</span>
											<span class="media-annotation pull-right">22:48</span>
										</a>

										<span class="text-muted">But that would be extremely strained and suspicious...</span>
									</div>
								</li>

								<li class="media">
									<div class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></div>
									<div class="media-body">
										<a href="#" class="media-heading">
											<span class="text-semibold">Beatrix Diaz</span>
											<span class="media-annotation pull-right">Tue</span>
										</a>

										<span class="text-muted">What a strenuous career it is that I've chosen...</span>
									</div>
								</li>

								<li class="media">
									<div class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></div>
									<div class="media-body">
										<a href="#" class="media-heading">
											<span class="text-semibold">Richard Vango</span>
											<span class="media-annotation pull-right">Mon</span>
										</a>
										
										<span class="text-muted">Other travelling salesmen live a life of luxury...</span>
									</div>
								</li>
							</ul>

							<div class="dropdown-content-footer">
								<a href="#" data-popup="tooltip" title="All messages"><i class="icon-menu display-block"></i></a>
							</div>
						</div>
					</li>

					<li class="dropdown dropdown-user">
						<a class="dropdown-toggle" data-toggle="dropdown">
							<img src="assets/images/placeholder.jpg" alt="">
							<span>Victoria</span>
							<i class="caret"></i>
						</a>

						<ul class="dropdown-menu dropdown-menu-right">
							<li><a href="#"><i class="icon-user-plus"></i> My profile</a></li>
							<li><a href="#"><i class="icon-coins"></i> My balance</a></li>
							<li><a href="#"><span class="badge bg-blue pull-right">58</span> <i class="icon-comment-discussion"></i> Messages</a></li>
							<li class="divider"></li>
							<li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>
							<li><a href="#"><i class="icon-switch2"></i> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">

					<!-- User menu -->
					<div class="sidebar-user">
						<div class="category-content">
							<div class="media">
								<a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
								<div class="media-body">
									<span class="media-heading text-semibold">Victoria Baker</span>
									<div class="text-size-mini text-muted">
										<i class="icon-pin text-size-small"></i> &nbsp;Santa Ana, CA
									</div>
								</div>

								<div class="media-right media-middle">
									<ul class="icons-list">
										<li>
											<a href="#"><i class="icon-cog3"></i></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- /user menu -->


					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
								

							</ul>
						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Tables</span> - Elements</h4>
						</div>

						<div class="heading-elements">
							<div class="heading-btn-group">
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
							</div>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="table_elements.html">Tables</a></li>
							<li class="active">Elements</li>
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
									Settings
									<span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
									<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
									<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Table components -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Table components</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							Table below contains different examples of components, that can be used in the table: form components, interface components, buttons etc. All of them are adapted for different cases, such as multiple elements in the same table cell. These components support all available sizes and styles. Also added a few examples of sparklines based on <code>sparklines.js</code> library. For more advanced sparklines <code>D3.js</code> library suits best
						</div>

						<div class="table-responsive">
							<table class="table table-bordered table-lg">
								<tbody>
									<tr class="active">
										<th colspan="3">Control buttons and icons</th>
									</tr>
									<tr>
										<td class="col-md-2 col-sm-3">Control links</td>
										<td class="col-sm-3">
											<ul class="icons-list">
												<li><a href="#"><i class="icon-pencil7"></i></a></li>
												<li><a href="#"><i class="icon-trash"></i></a></li>
												<li><a href="#"><i class="icon-cog7"></i></a></li>
											</ul>
										</td>
										<td>Basic table row control buttons. These links appear as a list of links with icons</td>
									</tr>
									<tr>
										<td>Colored links</td>
										<td>
											<ul class="icons-list">
												<li class="text-primary-600"><a href="#"><i class="icon-pencil7"></i></a></li>
												<li class="text-danger-600"><a href="#"><i class="icon-trash"></i></a></li>
												<li class="text-teal-600"><a href="#"><i class="icon-cog7"></i></a></li>
											</ul>
										</td>
										<td>Control links support different colors: default Bootstrap contextual colors and custom text colors from the custom color system. To use these colors add <code>.text-*</code> class to the parent <code>&lt;li></code> element</td>
									</tr>						
									

									<tr class="border-double active">
										<th colspan="3">File uploaders</th>
									</tr>
									<tr>
										<td>Default upload</td>
										<td>
											<input type="file" class="form-control">
										</td>
										<td>Default single file uploader</td>
									</tr>
									<tr>
										<td>Styled uploader</td>
										<td>
											<input type="file" class="file-styled">
										</td>
										<td>Single file uploader, styled with <code>uniform.js</code> plugin</td>
									</tr>
									<tr>
										<td>Multiple uploader</td>
										<td>
											<input type="file" class="bootstrap-uploader">
										</td>
										<td>Multiple file uploader, using <code>file_input.js</code> plugin</td>
									</tr>				

									
								</tbody>
							</table>
						</div>
					</div>
					<!-- /table components -->


		            <!-- Edit modal -->
					<div id="edit_modal" class="modal fade" role="dialog">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title">Edit table</h5>
								</div>

								<div class="modal-body">
									<div class="table-responsive">
										<table class="table">
											<thead>
												<tr>
													<th>First Name</th>
													<th>Last Name</th>
													<th>Username</th>
													<th class="col-xs-1">Action</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td><input type="text" class="form-control" value="Mark"></td>
													<td><input type="text" class="form-control" value="Otto"></td>
													<td><input type="text" class="form-control" value="@mdo"></td>
													<td class="text-center">
														<ul class="icons-list">
															<li><a href="#"><i class="icon-plus22"></i></a></li>
															<li><a href="#"><i class="icon-cross3"></i></a></li>
														</ul>
													</td>
												</tr>
												<tr>
													<td><input type="text" class="form-control" value="Jacob"></td>
													<td><input type="text" class="form-control" value="Thornton"></td>
													<td><input type="text" class="form-control" value="@fat"></td>
													<td class="text-center">
														<ul class="icons-list">
															<li><a href="#"><i class="icon-plus22"></i></a></li>
															<li><a href="#"><i class="icon-cross3"></i></a></li>
														</ul>
													</td>
												</tr>
												<tr>
													<td><input type="text" class="form-control" value="Larry"></td>
													<td><input type="text" class="form-control" value="the Bird"></td>
													<td><input type="text" class="form-control" value="@twitter"></td>
													<td class="text-center">
														<ul class="icons-list">
															<li><a href="#"><i class="icon-plus22"></i></a></li>
															<li><a href="#"><i class="icon-cross3"></i></a></li>
														</ul>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
									<button type="button" class="btn btn-primary" data-dismiss="modal">Save changes</button>
								</div>
							</div>
						</div>
					</div>
					<!-- /edit modal -->


		            <!-- Remove modal -->
					<div id="remove_modal" class="modal fade" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title">Confirm action</h5>
								</div>

								<div class="modal-body">
									You are about to remove this row. Are you sure?
								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-primary" data-dismiss="modal">Yes, remove</button>
									<button type="button" class="btn btn-default" data-dismiss="modal">No, thanks</button>
								</div>
							</div>
						</div>
					</div>
					<!-- /remove modal -->


		            <!-- Options modal -->
					<div id="options_modal" class="modal fade" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title">Row options</h5>
								</div>

								<div class="modal-body">
									<form class="form-horizontal">
										<div class="form-group">
											<label class="text-semibold control-label col-sm-3">Allow select:</label>
											<div class="col-sm-9">
												<select class="select">
													<option value="single" selected="selected">Single</option>
													<option value="multiple">Multiple</option>
													<option value="disabled">Disabled</option>
												</select>
											</div>
										</div>

										<div class="form-group">
											<label class="text-semibold control-label col-sm-3">Allow edit:</label>
											<div class="col-sm-9">
												<select class="select">
													<option value="inline">Edit inline</option>
													<option value="modal" selected="selected">Edit in modal</option>
													<option value="popover">Edit in popover</option>
													<option value="disabled">Disabled</option>
												</select>
											</div>
										</div>

										<div class="form-group">
											<label class="text-semibold control-label col-sm-3">Add status:</label>
											<div class="col-sm-9">
												<select class="select">
													<option value="completed" selected="selected">Completed</option>
													<option value="progress">In progress</option>
													<option value="assigned">Assigned</option>
													<option value="created">Created</option>
												</select>
											</div>
										</div>

										<div class="form-group">
											<label class="text-semibold control-label col-sm-3">Set priority:</label>
											<div class="col-sm-9">
												<select class="select-actions">
													<option value="urgent" data-icon="radio-checked text-danger" selected="selected">Urgent</option>
													<option value="high" data-icon="radio-checked text-primary">High</option>
													<option value="normal" data-icon="radio-checked text-success">Normal</option>
													<option value="low" data-icon="radio-checked text-info">Low</option>
												</select>
											</div>
										</div>

										<div class="form-group">
											<label class="text-semibold control-label col-sm-3" for="enable-controls">Enable controls:</label>
											<div class="col-sm-9">
												<div class="checkbox switchery-sm">
													<input type="checkbox" class="display-controls" id="enable-controls" checked="checked">
												</div>
											</div>
										</div>

										<div class="form-group">
											<label class="text-semibold control-label col-sm-3">Controls:</label>
											<div class="col-sm-9">
												<select class="select-actions select-sm" id="available_controls" multiple="multiple">
													<option value="edit" data-icon="pencil7" selected="selected">Edit</option>
													<option value="remove" data-icon="trash" selected="selected">Remove</option>
													<option value="options" data-icon="cog4" selected="selected">Options</option>
													<option value="add" data-icon="plus22">Add</option>
													<option value="add" data-icon="versions">Copy</option>
													<option value="select" data-icon="check">Select</option>
													<option value="mark" data-icon="file-download">Export</option>
													<option value="mark" data-icon="file-upload">Import</option>
													<option value="mark" data-icon="printer">Print</option>
												</select>
											</div>
										</div>
									</form>
								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
									<button type="button" class="btn btn-primary" data-dismiss="modal">Save settings</button>
								</div>
							</div>
						</div>
					</div>
					<!-- /options modal -->


					<!-- Footer -->
					<div class="footer text-muted">
						&copy; 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<!-- -->
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/core/libraries/jquery_ui/interactions.min.js"></script> 
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/styling/uniform.min.js"></script> 
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/styling/switchery.min.js"></script> 
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/selects/select2.min.js"></script> 
	 <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/selects/selectboxit.min.js"></script> 
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script> 
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/inputs/touchspin.min.js"></script> 


	<!--<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/uploaders/fileinput/plugins/purify.min.js"></script> 
	 <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/uploaders/fileinput/plugins/sortable.min.js"></script>  -->
	
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/uploaders/fileinput/fileinput.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/forms/editable/editable.min.js"></script> 
	<!-- <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/extensions/contextmenu.js"></script> -->
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/plugins/visualization/sparkline.min.js"></script> 
	<!-- <script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/core/app.js"></script> -->
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/pages/table_elements.js"></script>
	<!-- /theme JS files -->

</body>
</html>
