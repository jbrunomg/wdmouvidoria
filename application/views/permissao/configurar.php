<?php $p = unserialize($dados[0]->permissao_permissoes);?>

<!-- Table with togglable columns -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Configurar permissão <?php echo $dados[0]->permissao_nome; ?></h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<li><a data-action="close"></a></li>
        	</ul>
    	</div>
	</div>

	<br>
	<form action="<?php echo base_url(); ?>permissao/configurarExe/" method="post">

	<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

	<input type="hidden" value="<?php echo $dados[0]->permissao_id; ?>" name="id">
	<table class="table table-togglable table-hover">
		<thead>
			<tr>
				<th data-toggle="true">Atividade</th>
				<th data-hide="phone,tablet">Visualizar</th>
				<th data-hide="phone,tablet">Adicionar</th>
				<th data-hide="phone,tablet">Editar</th>
				<th data-hide="phone">Excluir</th>	
				<th data-hide="phone">Outros</th>									
			</tr>
		</thead>
		<tbody>	



	<tr>
		<td>Empresas</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vEmpresa" class="control-success" <?php echo (in_array('vEmpresa',$p))? 'checked="checked"': '' ?>>
					Visualizar Empresa
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aEmpresa" class="control-success" <?php echo (in_array('aEmpresa',$p))? 'checked="checked"': '' ?>>
					Adicionar Empresa
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eEmpresa" class="control-success" <?php echo (in_array('eEmpresa',$p))? 'checked="checked"': '' ?>>
					Editar Empresa
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dEmpresa" class="control-success" <?php echo (in_array('dEmpresa',$p))? 'checked="checked"': '' ?>>
					Excluir Empresa
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="cEmpresa" class="control-success" <?php echo (in_array('cEmpresa',$p))? 'checked="checked"': '' ?>>
					Configurar Mala Direta
				</label>
			</div>
		</td>	
		<td></td>													
	</tr>

	<tr>
		<td>Departamentos</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vDepartamento" class="control-success" <?php echo (in_array('vDepartamento',$p))? 'checked="checked"': '' ?>>
					Visualizar Departamento
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aDepartamento" class="control-success" <?php echo (in_array('aDepartamento',$p))? 'checked="checked"': '' ?>>
					Adicionar Departamento
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eDepartamento" class="control-success" <?php echo (in_array('eDepartamento',$p))? 'checked="checked"': '' ?>>
					Editar Departamento
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dDepartamento" class="control-success" <?php echo (in_array('dDepartamento',$p))? 'checked="checked"': '' ?>>
					Excluir Departamento
				</label>
			</div>
		</td>	
		<td></td>													
	</tr>


	<tr>
		<td>Sub-Departamentos</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vSubDepartamento" class="control-success" <?php echo (in_array('vSubDepartamento',$p))? 'checked="checked"': '' ?>>
					Visualizar SubDepartamento
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aSubDepartamento" class="control-success" <?php echo (in_array('aSubDepartamento',$p))? 'checked="checked"': '' ?>>
					Adicionar SubDepartamento
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eSubDepartamento" class="control-success" <?php echo (in_array('eSubDepartamento',$p))? 'checked="checked"': '' ?>>
					Editar SubDepartamento
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dSubDepartamento" class="control-success" <?php echo (in_array('dSubDepartamento',$p))? 'checked="checked"': '' ?>>
					Excluir SubDepartamento
				</label>
			</div>
		</td>	
		<td></td>													
	</tr>


	<tr>
		<td>Categorias</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vCategoria" class="control-success" <?php echo (in_array('vCategoria',$p))? 'checked="checked"': '' ?>>
					Visualizar Categoria
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aCategoria" class="control-success" <?php echo (in_array('aCategoria',$p))? 'checked="checked"': '' ?>>
					Adicionar Categoria
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eCategoria" class="control-success" <?php echo (in_array('eCategoria',$p))? 'checked="checked"': '' ?>>
					Editar Categoria
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dCategoria" class="control-success" <?php echo (in_array('dCategoria',$p))? 'checked="checked"': '' ?>>
					Excluir Categoria
				</label>
			</div>
		</td>	
		<td></td>													
	</tr>

	<tr>
		<td>Denúncia</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vDenuncia" class="control-success" <?php echo (in_array('vDenuncia',$p))? 'checked="checked"': '' ?>>
					Visualizar Denúncia
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aDenuncia" class="control-success" <?php echo (in_array('aDenuncia',$p))? 'checked="checked"': '' ?>>
					Adicionar Denúncia
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eDenuncia" class="control-success" <?php echo (in_array('eDenuncia',$p))? 'checked="checked"': '' ?>>
					Editar Denúncia
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dDenuncia" class="control-success" <?php echo (in_array('dDenuncia',$p))? 'checked="checked"': '' ?>>
					Excluir Denúncia
				</label>
			</div>
		</td>	
		<td></td>													
	</tr>

	<tr>
		<td>Tipo da Denúncia</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vTipoDenuncia" class="control-success" <?php echo (in_array('vTipoDenuncia',$p))? 'checked="checked"': '' ?>>
					Visualizar Tipo da Denúncia
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aTipoDenuncia" class="control-success" <?php echo (in_array('aTipoDenuncia',$p))? 'checked="checked"': '' ?>>
					Adicionar Tipo da Denúncia
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eTipoDenuncia" class="control-success" <?php echo (in_array('eTipoDenuncia',$p))? 'checked="checked"': '' ?>>
					Editar Tipo da Denúncia
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dTipoDenuncia" class="control-success" <?php echo (in_array('dTipoDenuncia',$p))? 'checked="checked"': '' ?>>
					Excluir Tipo da Denúncia
				</label>
			</div>
		</td>	
		<td></td>													
	</tr>


	<tr>
		<td>Usuários</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vUsuarios" class="control-success" <?php echo (in_array('vUsuarios',$p))? 'checked="checked"': '' ?>>
					Visualizar Usuarios
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aUsuarios" class="control-success" <?php echo (in_array('aUsuarios',$p))? 'checked="checked"': '' ?>>
					Adicionar Usuarios
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eUsuarios" class="control-success" <?php echo (in_array('eUsuarios',$p))? 'checked="checked"': '' ?>>
					Editar Usuarios
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dUsuarios" class="control-success" <?php echo (in_array('dUsuarios',$p))? 'checked="checked"': '' ?>>
					Excluir Usuarios
				</label>
			</div>
		</td>	
		<td></td>													
	</tr>


	<tr>
		<td>Arquivo</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vArquivo" class="control-success" <?php echo (in_array('vArquivo',$p))? 'checked="checked"': '' ?>>
					Visualizar Arquivo
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aArquivo" class="control-success" <?php echo (in_array('aArquivo',$p))? 'checked="checked"': '' ?>>
					Adicionar Arquivo
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eArquivo" class="control-success" <?php echo (in_array('eArquivo',$p))? 'checked="checked"': '' ?>>
					Editar Arquivo
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dArquivo" class="control-success" <?php echo (in_array('dArquivo',$p))? 'checked="checked"': '' ?>>
					Excluir Arquivo
				</label>
			</div>
		</td>	
		<td></td>													
	</tr>

	<tr>
		<td>Newsletter</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vNewsletter" class="control-success" <?php echo (in_array('vNewsletter',$p))? 'checked="checked"': '' ?>>
					Visualizar Newsletter
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aNewsletter" class="control-success" <?php echo (in_array('aNewsletter',$p))? 'checked="checked"': '' ?>>
					Adicionar Newsletter
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eNewsletter" class="control-success" <?php echo (in_array('eNewsletter',$p))? 'checked="checked"': '' ?>>
					Editar Newsletter
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dNewsletter" class="control-success" <?php echo (in_array('dNewsletter',$p))? 'checked="checked"': '' ?>>
					Excluir Newsletter
				</label>
			</div>
		</td>	
		<td></td>													
	</tr>   


	<tr>
		<td>Permissões</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vPermissoes" class="control-success" <?php echo (in_array('vPermissoes',$p))? 'checked="checked"': '' ?>>
					Visualizar Permissoes
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aPermissoes" class="control-success" <?php echo (in_array('aPermissoes',$p))? 'checked="checked"': '' ?>>
					Adicionar Permissoes
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="ePermissoes" class="control-success" <?php echo (in_array('ePermissoes',$p))? 'checked="checked"': '' ?>>
					Editar Permissoes
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dPermissoes" class="control-success" <?php echo (in_array('dPermissoes',$p))? 'checked="checked"': '' ?>>
					Excluir Permissoes
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="cPermissoes" class="control-success" <?php echo (in_array('cPermissoes',$p))? 'checked="checked"': '' ?>>
					Configurar Permissoes
				</label>
			</div>
		</td>		
		<td></td>													
	</tr>
		
	<tr>
		<td colspan="6">
			<div class="text-right">
				<button type="submit" class="btn bg-teal">Configurar<i class="icon-arrow-right14 position-right"></i></button>
			</div>
		</td>
	</tr>
		
		</tbody>			
	</table>		
	</form>
</div>
<!-- /table with togglable columns -->