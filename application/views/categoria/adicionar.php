<!-- Form horizontal -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Cadastro de Categoria</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<!-- <li><a data-action="reload"></a></li> -->
            		<!-- <li><a data-action="close"></a></li> -->
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/adicionarExe" method="post" enctype="multipart/form-data">
				<fieldset class="content-group">
					<legend class="text-bold">Dados do categoria:</legend>

					<!-- <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" /> -->

			        <div class="form-group">
			          <label class="control-label col-lg-2">Nome do Categoria:</label>
			          <div class="col-lg-5">
			            <input  type="text" class="form-control" placeholder="Nome Categoria" name="nome" id="nome" value="<?php echo set_value('nome'); ?>">
			          <?php echo form_error('nome'); ?>
			          </div>                    
			        </div>                		

				</fieldset>
				<div class="text-right">
					<button type="submit" class="btn bg-teal">Cadastrar <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>
