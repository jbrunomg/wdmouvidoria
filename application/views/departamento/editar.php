<!-- Form horizontal -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Edição de Departamento</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<!-- <li><a data-action="close"></a></li> -->
        	</ul>
    	</div>
	</div>

	<div class="panel-body">
		<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/editarExe" method="post" enctype="multipart/form-data">
			<fieldset class="content-group">
				<legend class="text-bold">Dados Departamento:</legend>

				<!-- <input  type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" /> -->

				<input  type="hidden" name="departamento_id" value="<?php echo $dados[0]->departamento_id; ?>" />

				<div class="form-group">
					<label class="control-label col-lg-2">Nome da Departamento:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" placeholder="Nome Departamento" name="nome" id="nome" value="<?php echo $dados[0]->nome; ?>">
					<?php echo form_error('nome'); ?>
					</div>										
				</div>

				    <legend class="text-bold">Empresas com Departamento:</legend>               		
			        
			        <div class="col-md-12">
			            <div class="panel panel-body border-top-teal text-center">
			              <h6 class="no-margin text-semibold">Empresa</h6>
			                <br/>
                            <select name="empresa[]" id="empresa" class="form-control select2" multiple="multiple" required> 			    
                            <?php // foreach ($empresas as $e)
                            	foreach($empresas as $key => $e) { $select = "";                    
                                foreach($empresasdp as $dp){                              	
                                    if($e->empresa_id == $dp){
                                        $select = "selected";
                                    }
                                  }
                                  echo "<option value='".$e->empresa_id."'".$select.">".$e->empresa_nomeFantasia."</option>";
                              }?>                        
                            </select>

					    </div>
			        </div>

                </fieldset>  
				<div class="text-right">
					<button type="submit" class="btn bg-teal">Editar <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>
