<!-- Form horizontal -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Cadastro de Departameto</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<!-- <li><a data-action="reload"></a></li> -->
            		<!-- <li><a data-action="close"></a></li> -->
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/adicionarExe" method="post" enctype="multipart/form-data">
				<fieldset class="content-group">
					<legend class="text-bold">Dados do departamento:</legend>

					<!-- <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" /> -->

			        <div class="form-group">
			          <label class="control-label col-lg-2">Nome do Departamento:</label>
			          <div class="col-lg-5">
			            <input  type="text" class="form-control" placeholder="Nome Departamento" name="nome" id="nome" value="<?php echo set_value('nome'); ?>" required>
			          <?php echo form_error('nome'); ?>
			          </div>                    
			        </div> 

			        <legend class="text-bold">Empresas com Departamento:</legend>               		
			        
			        <div class="col-md-12">
			            <div class="panel panel-body border-top-teal text-center">
			              <h6 class="no-margin text-semibold">Empresa</h6>
			                <br/> 
			                <select name="empresa[]" id="empresa" class="js-example-basic-single js-states form-control" required></select>           
			            </div>
			        </div>

				</fieldset>
				<div class="text-right">
					<button type="submit" class="btn bg-teal">Cadastrar <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>
