<?php $totalProdutos = 0; ?>

    <!-- Bordered striped table -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Pagamento do Sistema</h5>
          <!--   <div class="buttons">                
                <a id="imprimir" title="Imprimir" class="btn btn-mini btn-inverse" href=""><i class="icon-print icon-white"></i> Imprimir</a>
                
            </div> -->

            

            <div class="heading-elements">
                <a class="btn btn-success btn-lg" href="<?php echo base_url(); ?>relatorios/imprimirpagamento/<?php echo $results[0]->pagamento_sistema_id; ?>">Imprimir <i class="icon-printer position-left"></i></a>
                <!-- <button type="button" class="btn btn-primary"><i class="icon-printer position-left"></i> Imprimir</button>                 -->

          <!--       <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul> -->
            </div>
        </div>

        <div class="panel-body">
            <!-- Pagamentos -->
        </div>

        <div class="table-responsive">


            <div style="margin-top: 0; padding-top: 0">             

                <table class="table table-bordered table-condensed" id="tblProdutos">
                    <thead>
                        <tr><th colspan="4"><h1>Recibo de Pagamento #<?php echo $results[0]->pagamento_sistema_id ?></h1></th></tr>
                        <tr>
                            <th style="font-size: 15px">Descrição</th>
                            <th style="font-size: 15px">Data de vencimento</th>
                            <th style="font-size: 15px">Data de pagamanto</th>                                         
                            <th style="font-size: 15px">Sub-total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php                                                                
                        
                            echo '<tr>';
                            echo '<td>'.$results[0]->pagamento_sistema_descricao.'</td>';
                            echo '<td>'.date('d/m/Y', strtotime($results[0]->pagamento_sistema_data_vencimento)).'</td>';
                            echo '<td>'.date('d/m/Y', strtotime($results[0]->pagamento_sistema_data_pagamento)).'</td>';
                            echo '<td>'.$results[0]->pagamento_sistema_valor.'</td>';                                                                                     
                            echo '</tr>';
                        ?>
                        <tr>
                            <td colspan="3" style="text-align: right"><strong>Total:</strong></td>
                            <td><strong> <?php echo $results[0]->pagamento_sistema_valor;?></strong></td>
                        </tr>       

                    </tbody>
                </table>     

                <table class="table table-bordered table-condensed" id="tblProdutos">                  

                    <tbody>
                        <tr>
                            <td colspan="5" style="text-align: left"><strong>Observações:</strong><br><?php echo $results[0]->pagamento_sistema_observacoes; ?></td>
                        </tr>
                    </tbody>
                </table>
                <hr size="1" style="border:1px dashed #C0C0C0;">
                <table class="table table-bordered table-condensed" id="canhoto">
                    <tbody>
                        <tr>
                            <td colspan="3" style="text-align: center"><strong>RECEBEMOS DA Carvalhal Auditoria Interna e Compliance O VALOR REFERENTE AO PAGAMENTO DE NÚMERO AO LADO</strong></td>
                            <td colspan="2" rowspan="2" style="text-align: center"><strong>PAGAMENTO: </strong><br>#<?php echo $results[0]->pagamento_sistema_id?><br><strong>VALOR: </strong><br><?php echo  $results[0]->pagamento_sistema_valor;?></td>
                        </tr>
                        <tr>
                            <td colspan="1" style="text-align: center;"><br><strong>Data <?php echo date('d/m/Y', strtotime($results[0]->pagamento_sistema_data_pagamento))?></strong></td>
                            <td colspan="2" style="text-align: left"><br><strong>Assinatura: <img src="<?php echo base_url(); ?>public/assets/images/signature.png"> </strong></td>
                        </tr>
                    </tbody>
                </table>
            </div>

<!--             <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Descrição</th>
                        <th>Data de Vencimento</th>
                        <th>Data de Pagamento</th>
                        <th>Valor</th>
                        <th>Status</th>
                        <th></th>                        
                    </tr>
                </thead>
                <tbody>

                    <?php foreach ($dados as $r) {
                        $status = ($r->pagamento_sistema_status == 1)?'PAGO':'PENDENTE';
                        $pagamento = ($r->pagamento_sistema_data_pagamento)?date('d/m/Y', strtotime($r->pagamento_sistema_data_pagamento)):'';
                        echo '<tr>';
                        echo '<td>'.$r->pagamento_sistema_id.'</td>';
                        echo '<td>'.$r->pagamento_sistema_descricao.'</td>';
                        echo '<td>'.date('d/m/Y', strtotime($r->pagamento_sistema_data_vencimento)).'</td>';
                        echo '<td>'.$pagamento.'</td>';
                        echo '<td>'.$r->pagamento_sistema_valor.'</td>'; 
                        echo '<td>'.$status.'</td>';                            
                        echo '<td>';
                        if($pagamento <> ''){
                             echo '<a style="margin: 1%" href="'.base_url().'pagamentosistema/visualizar/'.$r->pagamento_sistema_id.'" class="btn tip-top" title="Visualizar Pagamento"><i class="icon-eye"></i></a>  '; 
                        }
                       
                        echo '</td>';
                        echo '</tr>';
                    }?>
                    <tr>
                        
                    </tr>

                </tbody>
            </table> -->

        </div>
    </div>
    <!-- /bordered striped table -->



<script type="text/javascript">
    $(document).ready(function(){
        $("#imprimir").click(function(){         
            PrintElem('#printOs');
        })

        function PrintElem(elem)
        {
            Popup($(elem).html());
        }

        function Popup(data) 
        {
            var mywindow = window.open('', 'mydiv', 'height=400,width=600');
            mywindow.document.open();
            mywindow.document.onreadystatechange=function(){
             if(this.readyState==='complete'){
              this.onreadystatechange=function(){};
              mywindow.focus();
              mywindow.print();
              mywindow.close();
             }
            }
            mywindow.document.write('<html><head><title>WDM-Controller</title>');
            mywindow.document.write("<link rel='stylesheet' href='<?php echo base_url();?>assets/css/bootstrap.min.css' />");
            mywindow.document.write("<link rel='stylesheet' href='<?php echo base_url();?>assets/css/bootstrap-responsive.min.css' />");
            mywindow.document.write("<link rel='stylesheet' href='<?php echo base_url();?>assets/css/matrix-style.css' />");
            mywindow.document.write("<link rel='stylesheet' href='<?php echo base_url();?>assets/css/matrix-media.css' />");

            mywindow.document.write('</head><body >');
            mywindow.document.write(data);
                    
            mywindow.document.write('</body></html>'); 

            mywindow.document.close(); // necessary for IE >= 10


            return true;
        }


    });
</script>